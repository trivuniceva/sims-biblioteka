from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_prikazClana(object):

    def __init__(self, display_user_obj, what_to_do, do_this, go_back_where):
        self.display_user_obj = display_user_obj
        self.what_to_do = what_to_do
        self.do_this = do_this
        self.go_back_where = go_back_where

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()

        if self.go_back_where == 'administrator':
            self.ui = Ui_AdministratorMainPage()
        else:
            self.ui = Ui_bibliotekarMain()

        self.ui.setupUi(self.window1)
        self.window1.show()

    def you_did_it(self, message):
        from src.View.login.obavestenje import Ui_Obavestenje

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_Obavestenje()
        self.ui.setupUi(self.window1, None, message)
        self.window1.show()

    def setupUi(self, prikazClana):
        prikazClana.setObjectName("prikazClana")
        prikazClana.setFixedSize(960, 537)
        prikazClana.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(prikazClana)
        self.centralwidget.setObjectName("centralwidget")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(51, 190, 61, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color: #86ACA5;")
        self.label_3.setObjectName("label_3")
        self.usernameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.usernameBox.setEnabled(True)
        self.usernameBox.setGeometry(QtCore.QRect(150, 120, 181, 22))
        self.usernameBox.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n")
        self.usernameBox.setObjectName("usernameBox")

        self.usernameBox.setText(self.display_user_obj.get_username())

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(11, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png)")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(prikazClana.hide)

        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(381, 190, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color: #86ACA5;")
        self.label_9.setObjectName("label_9")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(51, 120, 91, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setStyleSheet("color: #86ACA5;")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(381, 120, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color: #86ACA5;")
        self.label_2.setObjectName("label_2")
        self.nameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.nameBox.setEnabled(True)
        self.nameBox.setGeometry(QtCore.QRect(151, 190, 180, 22))
        self.nameBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n")
        self.nameBox.setObjectName("nameBox")

        self.nameBox.setText(self.display_user_obj.get_name())

        self.lastnameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.lastnameBox.setEnabled(True)
        self.lastnameBox.setGeometry(QtCore.QRect(500, 190, 181, 22))
        self.lastnameBox.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n")
        self.lastnameBox.setObjectName("lastnameBox")

        self.lastnameBox.setText(self.display_user_obj.get_lastname())

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(721, 0, 241, 541))
        self.widget.setStyleSheet("background-color: #86ACA5;")
        self.widget.setObjectName("widget")
        self.changeButton = QtWidgets.QPushButton(self.widget)
        self.changeButton.setGeometry(QtCore.QRect(20, 430, 200, 89))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.changeButton.setFont(font)
        self.changeButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.changeButton.setObjectName("changeButton")

        self.changeButton.clicked.connect(self.change_user)

        self.passwordBox = QtWidgets.QLineEdit(self.centralwidget)
        self.passwordBox.setEnabled(True)
        self.passwordBox.setGeometry(QtCore.QRect(500, 120, 181, 22))
        self.passwordBox.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n")
        self.passwordBox.setObjectName("passwordBox")

        self.passwordBox.setText(self.display_user_obj.get_password())

        self.telephoneBox = QtWidgets.QLineEdit(self.centralwidget)
        self.telephoneBox.setEnabled(True)
        self.telephoneBox.setGeometry(QtCore.QRect(150, 330, 180, 22))
        self.telephoneBox.setStyleSheet("border: none;\n"
                                        "border-bottom: 2px solid #E7E3D5;\n"
                                        "color: #527770;\n")
        self.telephoneBox.setObjectName("telephoneBox")

        self.telephoneBox.setText(self.display_user_obj.get_telephone_number())

        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(380, 330, 111, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color: #86ACA5;")
        self.label_10.setObjectName("label_10")
        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setEnabled(True)
        self.jmbgBox.setGeometry(QtCore.QRect(149, 260, 181, 22))
        self.jmbgBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.jmbgBox.setObjectName("jmbgBox")

        self.jmbgBox.setText(self.display_user_obj.get_jmbg())

        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setEnabled(True)
        self.emailBox.setGeometry(QtCore.QRect(499, 260, 181, 22))
        self.emailBox.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.emailBox.setObjectName("emailBox")

        self.emailBox.setText(self.display_user_obj.get_email())

        self.addressBox = QtWidgets.QLineEdit(self.centralwidget)
        self.addressBox.setEnabled(True)
        self.addressBox.setGeometry(QtCore.QRect(499, 330, 181, 22))
        self.addressBox.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.addressBox.setObjectName("addressBox")

        self.addressBox.setText(self.display_user_obj.get_address())

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(380, 260, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color: #86ACA5;")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(50, 330, 61, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setStyleSheet("color: #86ACA5;")
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(50, 260, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("color: #86ACA5;")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(50, 400, 101, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setStyleSheet("color: #86ACA5;")
        self.label_7.setObjectName("label_7")
        self.dateBox = QtWidgets.QLineEdit(self.centralwidget)
        self.dateBox.setEnabled(True)
        self.dateBox.setGeometry(QtCore.QRect(150, 400, 180, 22))
        self.dateBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.dateBox.setObjectName("dateBox")

        self.dateBox.setText(self.display_user_obj.get_birth_date())

        self.membershipTypeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.membershipTypeBox.setEnabled(True)
        self.membershipTypeBox.setGeometry(QtCore.QRect(499, 400, 181, 22))
        self.membershipTypeBox.setStyleSheet("border: none;\n"
                                             "border-bottom: 2px solid #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "\n"
                                             "")
        self.membershipTypeBox.setObjectName("membershipTypeBox")

        self.membershipTypeBox.setText(self.display_user_obj.get_membership_type())

        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setGeometry(QtCore.QRect(380, 400, 101, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_11.setFont(font)
        self.label_11.setStyleSheet("color: #86ACA5;")
        self.label_11.setObjectName("label_11")
        prikazClana.setCentralWidget(self.centralwidget)

        self.retranslateUi(prikazClana)
        QtCore.QMetaObject.connectSlotsByName(prikazClana)

    def retranslateUi(self, prikazClana):
        _translate = QtCore.QCoreApplication.translate
        prikazClana.setWindowTitle(_translate("prikazClana", "MainWindow"))
        self.label_3.setText(_translate("prikazClana", "ime:"))
        self.label_9.setText(_translate("prikazClana", "prezime:"))
        self.label.setText(_translate("prikazClana", "korisnicko ime:"))
        self.label_2.setText(_translate("prikazClana", "sifra:"))
        self.changeButton.setText(_translate("prikazClana", self.what_to_do))
        self.label_10.setText(_translate("prikazClana", "adresa stanovanja:"))
        self.label_4.setText(_translate("prikazClana", "email:"))
        self.label_5.setText(_translate("prikazClana", "telefon:"))
        self.label_6.setText(_translate("prikazClana", "jmbg:"))
        self.label_7.setText(_translate("prikazClana", "datum rodjenja:"))
        self.label_11.setText(_translate("prikazClana", "tip clanarine:"))

    def change_user(self):
        from src.Controller.users_controller.member_CRUD import member_CRUD
        from src.Controller.users_controller.users_check import check_if_username_used

        username = self.display_user_obj.get_username()

        memb = member_CRUD(self.display_user_obj.get_id_membership(), self.display_user_obj.telephone_number,
                           self.display_user_obj.get_address(), self.display_user_obj.get_membership_type(),
                           self.display_user_obj.get_birth_date(), username, self.display_user_obj.get_password(),
                           self.display_user_obj.get_name(), self.display_user_obj.get_lastname(),
                           self.display_user_obj.get_jmbg(),
                           self.display_user_obj.get_email(), 'member')

        if self.do_this == 'delete':
            memb.delete_member(username)
        else:
            # check if user change username or something else
            if self.usernameBox.text() == username:
                print(self.display_user_obj.get_name())
                print(self.nameBox.text())
                memb.update_member(username, self.passwordBox.text(), self.nameBox.text(), self.lastnameBox.text(),
                                   self.jmbgBox.text(), self.emailBox.text(), self.telephoneBox.text(),
                                   self.addressBox.text(), self.dateBox.text(), self.membershipTypeBox.text())

                self.you_did_it('izmenili ste korisnika')

            else:
                if check_if_username_used(username):
                    self.you_did_it('username je zauzet')
