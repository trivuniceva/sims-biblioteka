from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_kreirajKorisnika(object):

    def go_back(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, kreirajKorisnika):
        kreirajKorisnika.setObjectName("kreirajKorisnika")
        kreirajKorisnika.setFixedSize(960, 537)
        kreirajKorisnika.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(kreirajKorisnika)
        self.centralwidget.setObjectName("centralwidget")
        self.imeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.imeBox.setGeometry(QtCore.QRect(40, 90, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.imeBox.setFont(font)
        self.imeBox.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.imeBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.imeBox.setObjectName("imeBox")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 531))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.kreirajOption = QtWidgets.QPushButton(self.widget_2)
        self.kreirajOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.kreirajOption.setAutoFillBackground(False)
        self.kreirajOption.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")
        self.kreirajOption.setObjectName("kreirajOption")

        self.kreirajOption.clicked.connect(self.kreirajCallFunc)
        self.kreirajOption.clicked.connect(kreirajKorisnika.hide)

        self.izmeniOption = QtWidgets.QPushButton(self.widget_2)
        self.izmeniOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.izmeniOption.setAutoFillBackground(False)
        self.izmeniOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.izmeniOption.setObjectName("izmeniOption")

        self.izmeniOption.clicked.connect(self.izmeniCallFunc)
        self.izmeniOption.clicked.connect(kreirajKorisnika.hide)

        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")

        self.obrisiOption.clicked.connect(self.obrisiCallFunc)
        self.obrisiOption.clicked.connect(kreirajKorisnika.hide)

        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setGeometry(QtCore.QRect(40, 170, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.jmbgBox.setFont(font)
        self.jmbgBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.jmbgBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.jmbgBox.setObjectName("jmbgBox")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png)")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(kreirajKorisnika.hide)

        self.prezimeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.prezimeBox.setGeometry(QtCore.QRect(410, 90, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.prezimeBox.setFont(font)
        self.prezimeBox.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.prezimeBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.prezimeBox.setObjectName("prezimeBox")
        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setGeometry(QtCore.QRect(410, 170, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.emailBox.setFont(font)
        self.emailBox.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.emailBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.emailBox.setObjectName("emailBox")
        self.telefonBox = QtWidgets.QLineEdit(self.centralwidget)
        self.telefonBox.setGeometry(QtCore.QRect(40, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.telefonBox.setFont(font)
        self.telefonBox.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.telefonBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.telefonBox.setObjectName("telefonBox")
        self.adresaStanovanjaBox = QtWidgets.QLineEdit(self.centralwidget)
        self.adresaStanovanjaBox.setGeometry(QtCore.QRect(410, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.adresaStanovanjaBox.setFont(font)
        self.adresaStanovanjaBox.setStyleSheet("border: none;\n"
                                               "border-bottom: 2px solid #E7E3D5;\n"
                                               "color: #527770;\n"
                                               "\n"
                                               "")
        self.adresaStanovanjaBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.adresaStanovanjaBox.setObjectName("adresaStanovanjaBox")
        self.datumRodjBox = QtWidgets.QLineEdit(self.centralwidget)
        self.datumRodjBox.setGeometry(QtCore.QRect(40, 330, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.datumRodjBox.setFont(font)
        self.datumRodjBox.setStyleSheet("border: none;\n"
                                        "border-bottom: 2px solid #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "\n"
                                        "")
        self.datumRodjBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.datumRodjBox.setObjectName("datumRodjBox")
        self.kreirajButton = QtWidgets.QPushButton(self.centralwidget)
        self.kreirajButton.setGeometry(QtCore.QRect(280, 420, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.kreirajButton.setFont(font)
        self.kreirajButton.setAutoFillBackground(False)
        self.kreirajButton.setStyleSheet("background-color: #86ACA5;\n"
                                         "color: #FFFFFF;\n"
                                         "border-radius: 15px;")
        self.kreirajButton.setObjectName("kreirajButton")

        self.kreirajButton.clicked.connect(self.kreirajCallFunc)
        # self.kreirajButton.clicked.connect(kreirajKorisnika.hide)

        self.membershipType = QtWidgets.QComboBox(self.centralwidget)
        self.membershipType.setGeometry(QtCore.QRect(540, 330, 161, 51))
        self.membershipType.setStyleSheet("color: #577F77;\n"
                                          "background-color: #ffffff;\n"
                                          "selection-color: #E7E3D5;\n"
                                          "selection-background-color: #577F77;\n"
                                          "border-bottom: 2px solid #E7E3D5;\n"
                                          "border-top: #ffffff;\n"
                                          "")
        self.membershipType.setObjectName("membershipType")
        self.membershipType.addItem("")
        self.membershipType.addItem("")
        self.membershipType.addItem("")
        self.membershipType.addItem("")
        self.membershipType.addItem("")

        self.membershipTypeText = QtWidgets.QLineEdit(self.centralwidget)
        self.membershipTypeText.setGeometry(QtCore.QRect(410, 330, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.membershipTypeText.setFont(font)
        self.membershipTypeText.setStyleSheet("border: none;\n"
                                              "border-bottom: 2px solid #E7E3D5;\n"
                                              "color: #527770;\n"
                                              "\n"
                                              "")
        self.membershipTypeText.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.membershipTypeText.setObjectName("membershipTypeText")
        self.membershipTypeText.setReadOnly(True)

        kreirajKorisnika.setCentralWidget(self.centralwidget)

        self.retranslateUi(kreirajKorisnika)
        QtCore.QMetaObject.connectSlotsByName(kreirajKorisnika)

    def retranslateUi(self, kreirajKorisnika):
        _translate = QtCore.QCoreApplication.translate
        kreirajKorisnika.setWindowTitle(_translate("kreirajKorisnika", "MainWindow"))
        self.imeBox.setPlaceholderText(_translate("kreirajKorisnika", "ime:"))
        self.kreirajOption.setText(_translate("kreirajKorisnika", "kreiraj"))
        self.izmeniOption.setText(_translate("kreirajKorisnika", "izmeni"))
        self.obrisiOption.setText(_translate("kreirajKorisnika", "obrisi"))
        self.jmbgBox.setPlaceholderText(_translate("kreirajKorisnika", "jmbg:"))
        self.prezimeBox.setPlaceholderText(_translate("kreirajKorisnika", "prezime:"))
        self.emailBox.setPlaceholderText(_translate("kreirajKorisnika", "email:"))
        self.telefonBox.setPlaceholderText(_translate("kreirajKorisnika", "telefon:"))
        self.adresaStanovanjaBox.setPlaceholderText(_translate("kreirajKorisnika", "adresa stanovanja:"))
        self.datumRodjBox.setPlaceholderText(_translate("kreirajKorisnika", "datum rodjenja:"))
        self.kreirajButton.setText(_translate("kreirajKorisnika", "kreiraj"))
        self.membershipType.setItemText(0, _translate("kreirajKorisnika", "pocasni"))
        self.membershipType.setItemText(1, _translate("kreirajKorisnika", "penzioneri"))
        self.membershipType.setItemText(2, _translate("kreirajKorisnika", "odrasli"))
        self.membershipType.setItemText(3, _translate("kreirajKorisnika", "studenti"))
        self.membershipType.setItemText(4, _translate("kreirajKorisnika", "deca"))
        self.membershipTypeText.setPlaceholderText(_translate("kreirajKorisnika", "tip clanarine:"))

    def kreirajCallFunc(self):
        from src.Controller.users_controller.member_CRUD import member_CRUD
        from src.Controller.sending_emails import autogenerate_username, autogenerate_password, \
            autogenerate_id_membership, send_username_password_member

        username = autogenerate_username()
        password = autogenerate_password()
        id_membership = autogenerate_id_membership()

        name = self.imeBox.text()
        lastname = self.prezimeBox.text()
        jmbg = self.jmbgBox.text()
        email = self.emailBox.text()
        telephone_number = self.telefonBox.text()
        address = self.adresaStanovanjaBox.text()
        membership_type = self.membershipType.currentText()
        birth_date = self.datumRodjBox.text()

        print(username)
        memb = member_CRUD(id_membership, telephone_number, address, membership_type, birth_date, username, password,
                           name, lastname, jmbg, email, 'member')
        memb.create_member()

        send_username_password_member(username, password, id_membership, email)
        self.you_did_it()

    def you_did_it(self):
        from src.View.login.obavestenje import Ui_Obavestenje

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_Obavestenje()
        self.ui.setupUi(self.window1, None, 'Kreirali ste korisnika')
        self.window1.show()

    def izmeniCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.UpdateMemberPage import Ui_izmeniKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_izmeniKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def obrisiCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.DeleteMemberPage import Ui_brisiKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_brisiKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()
