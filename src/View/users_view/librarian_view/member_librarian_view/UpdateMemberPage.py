from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_izmeniKorisnika(object):

    def go_back(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, izmeniKorisnika):
        izmeniKorisnika.setObjectName("izmeniKorisnika")
        izmeniKorisnika.setFixedSize(960, 537)
        izmeniKorisnika.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(izmeniKorisnika)
        self.centralwidget.setObjectName("centralwidget")
        self.prikaziRezultateButton = QtWidgets.QPushButton(self.centralwidget)
        self.prikaziRezultateButton.setGeometry(QtCore.QRect(298, 470, 161, 51))
        self.prikaziRezultateButton.setAutoFillBackground(False)
        self.prikaziRezultateButton.setStyleSheet("background-color: #86ACA5;\n"
                                                  "color: #FFFFFF;\n"
                                                  "border-radius: 15px;")
        self.prikaziRezultateButton.setObjectName("prikaziRezultateButton")

        self.prikaziRezultateButton.clicked.connect(self.showSearchResults)
        self.prikaziRezultateButton.clicked.connect(izmeniKorisnika.hide)

        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setGeometry(QtCore.QRect(20, 330, 331, 31))
        self.emailBox.setStyleSheet("background-color: #ffffff;\n"
                                    "borde-radius: 2px;\n"
                                    "color: #527770;\n"
                                    "padding-left:8px;")
        self.emailBox.setText("")
        self.emailBox.setObjectName("emailBox")
        self.usernameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.usernameBox.setGeometry(QtCore.QRect(20, 230, 331, 31))
        self.usernameBox.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.usernameBox.setText("")
        self.usernameBox.setObjectName("usernameBox")
        self.birthDateBox = QtWidgets.QLineEdit(self.centralwidget)
        self.birthDateBox.setGeometry(QtCore.QRect(370, 420, 331, 31))
        self.birthDateBox.setStyleSheet("background-color: #ffffff;\n"
                                        "borde-radius: 2px;\n"
                                        "color: #527770;\n"
                                        "padding-left:8px;")
        self.birthDateBox.setText("")
        self.birthDateBox.setObjectName("birthDateBox")
        self.imeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.imeBox.setGeometry(QtCore.QRect(20, 280, 331, 31))
        self.imeBox.setStyleSheet("background-color: #ffffff;\n"
                                  "borde-radius: 2px;\n"
                                  "color: #527770;\n"
                                  "padding-left:8px;")
        self.imeBox.setText("")
        self.imeBox.setObjectName("imeBox")
        self.passwordBox = QtWidgets.QLineEdit(self.centralwidget)
        self.passwordBox.setGeometry(QtCore.QRect(370, 230, 331, 31))
        self.passwordBox.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.passwordBox.setText("")
        self.passwordBox.setObjectName("passwordBox")
        self.addressBox = QtWidgets.QLineEdit(self.centralwidget)
        self.addressBox.setGeometry(QtCore.QRect(370, 380, 331, 31))
        self.addressBox.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.addressBox.setText("")
        self.addressBox.setObjectName("addressBox")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png)")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(izmeniKorisnika.hide)

        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setGeometry(QtCore.QRect(370, 330, 331, 31))
        self.jmbgBox.setStyleSheet("background-color: #ffffff;\n"
                                   "borde-radius: 2px;\n"
                                   "color: #527770;\n"
                                   "padding-left:8px;")
        self.jmbgBox.setText("")
        self.jmbgBox.setObjectName("jmbgBox")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.kreirajOption = QtWidgets.QPushButton(self.widget_2)
        self.kreirajOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.kreirajOption.setAutoFillBackground(False)
        self.kreirajOption.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")
        self.kreirajOption.setObjectName("kreirajOption")

        self.kreirajOption.clicked.connect(self.kreirajCallFunc)
        self.kreirajOption.clicked.connect(izmeniKorisnika.hide)

        self.izmeniOption = QtWidgets.QPushButton(self.widget_2)
        self.izmeniOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.izmeniOption.setAutoFillBackground(False)
        self.izmeniOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.izmeniOption.setObjectName("izmeniOption")
        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")

        self.obrisiOption.clicked.connect(self.obrisiCallFunc)
        self.obrisiOption.clicked.connect(izmeniKorisnika.hide)

        self.telephoneBox = QtWidgets.QLineEdit(self.centralwidget)
        self.telephoneBox.setGeometry(QtCore.QRect(20, 380, 331, 31))
        self.telephoneBox.setStyleSheet("background-color: #ffffff;\n"
                                        "borde-radius: 2px;\n"
                                        "color: #527770;\n"
                                        "padding-left:8px;")
        self.telephoneBox.setText("")
        self.telephoneBox.setObjectName("telephonBox")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(40, 40, 631, 151))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(20, 20, 441, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:#ffffff;")
        self.label_2.setObjectName("label_2")

        self.prezimeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.prezimeBox.setGeometry(QtCore.QRect(370, 280, 331, 31))
        self.prezimeBox.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.prezimeBox.setText("")
        self.prezimeBox.setObjectName("prezimeBox")
        self.membershipTypeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.membershipTypeBox.setGeometry(QtCore.QRect(20, 420, 331, 31))
        self.membershipTypeBox.setStyleSheet("background-color: #ffffff;\n"
                                             "borde-radius: 2px;\n"
                                             "color: #527770;\n"
                                             "padding-left:8px;")
        self.membershipTypeBox.setText("")
        self.membershipTypeBox.setObjectName("membershipTypeBox")
        izmeniKorisnika.setCentralWidget(self.centralwidget)

        self.retranslateUi(izmeniKorisnika)
        QtCore.QMetaObject.connectSlotsByName(izmeniKorisnika)

    def retranslateUi(self, izmeniKorisnika):
        _translate = QtCore.QCoreApplication.translate
        izmeniKorisnika.setWindowTitle(_translate("izmeniKorisnika", "MainWindow"))
        self.prikaziRezultateButton.setText(_translate("izmeniKorisnika", "prikazi rezultate"))
        self.emailBox.setPlaceholderText(_translate("izmeniKorisnika", "email:"))
        self.usernameBox.setPlaceholderText(_translate("izmeniKorisnika", "korisnicko ime (direktna pretraga):"))
        self.birthDateBox.setPlaceholderText(_translate("izmeniKorisnika", "datum rodjenja:"))
        self.imeBox.setPlaceholderText(_translate("izmeniKorisnika", "ime: "))
        self.passwordBox.setPlaceholderText(_translate("izmeniKorisnika", "sifra:"))
        self.addressBox.setPlaceholderText(_translate("izmeniKorisnika", "adresa stanovanja:"))
        self.jmbgBox.setPlaceholderText(_translate("izmeniKorisnika", "jmbg:"))
        self.kreirajOption.setText(_translate("izmeniKorisnika", "kreiraj"))
        self.izmeniOption.setText(_translate("izmeniKorisnika", "izmeni"))
        self.obrisiOption.setText(_translate("izmeniKorisnika", "obrisi"))
        self.telephoneBox.setPlaceholderText(_translate("izmeniKorisnika", "telefon:"))
        self.label_2.setText(_translate("izmeniKorisnika", "izmeni korisnika"))
        self.prezimeBox.setPlaceholderText(_translate("izmeniKorisnika", "prezime:"))
        self.membershipTypeBox.setPlaceholderText(_translate("izmeniKorisnika", "tip clanarine:"))

    def kreirajCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.CreateMemberPage import Ui_kreirajKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_kreirajKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def obrisiCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.DeleteMemberPage import Ui_brisiKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_brisiKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def showSearchResults(self):
        from src.Controller.users_controller.users_check import check_if_username_used, direct_search, \
            multicriteria_search

        if check_if_username_used(self.usernameBox.text()):
            display_user_obj = direct_search(self.usernameBox.text())
            self.show_member_update(display_user_obj)

        else:
            results = multicriteria_search()

            print('1 display: -------------------->')
            if self.give_conditions(results):
                results = self.give_conditions(results)
            else:
                self.show_results_for_update(list(results))

    def show_member_update(self, display_user_obj):
        self.window1 = QtWidgets.QMainWindow()
        from src.View.users_view.librarian_view.member_librarian_view.showMemberPage import Ui_prikazClana

        self.ui = Ui_prikazClana(display_user_obj, 'izmeni', 'update', 'korisnik')
        self.ui.setupUi(self.window1)
        self.window1.show()

    def show_results_for_update(self, results):
        from src.View.users_view.admin_view.librarian_admin_view.showResultsLibrarianPage import Ui_prikazRezultata

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_prikazRezultata(results, 'izmeni', 'update', 'korisnik')
        self.ui.setupUi(self.window1)
        self.window1.show()

    def give_conditions(self, results):
        from src.Controller.users_controller.users_check import password_search, name_search, lastname_search, \
            email_search, jmbg_search, telephone_search, address_search, membership_search, birth_search

        result = results

        if self.passwordBox.text() != '':
            result = password_search(results, self.passwordBox.text())

        if self.imeBox.text() != '':
            result = name_search(results, self.imeBox.text())

        if self.prezimeBox.text() != '':
            result = lastname_search(results, self.prezimeBox.text())

        if self.emailBox.text() != '':
            result = email_search(results, self.emailBox.text())

        if self.jmbgBox.text() != '':
            result = jmbg_search(results, self.jmbgBox.text())

        if self.telephoneBox.text() != '':
            result = telephone_search(results, self.jmbgBox.text())

        if self.addressBox.text() != '':
            result = address_search(results, self.jmbgBox.text())

        if self.membershipTypeBox.text() != '':
            result = membership_search(results, self.jmbgBox.text())

        if self.birthDateBox.text() != '':
            result = birth_search(results, self.jmbgBox.text())

        else:
            return False

        return result
