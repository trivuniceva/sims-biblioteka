from PyQt5 import QtCore, QtGui, QtWidgets
from threading import Timer


class Ui_bibliotekarMain(object):
    def __init__(self, logged_user=None):
        self.logged_user = logged_user

    def setupUi(self, bibliotekarMain):
        bibliotekarMain.setObjectName("bibliotekarMain")
        bibliotekarMain.setFixedSize(960, 537)
        bibliotekarMain.setStyleSheet("background-color: #ffffff;")

        self.centralwidget = QtWidgets.QWidget(bibliotekarMain)
        self.centralwidget.setObjectName("centralwidget")
        self.notificationButton = QtWidgets.QPushButton(self.centralwidget)
        self.notificationButton.setGeometry(QtCore.QRect(40, 310, 121, 89))
        self.notificationButton.setAutoFillBackground(False)
        self.notificationButton.setStyleSheet("background-color: #E7E3D5;\n"
                                              "color: #527770;\n"
                                              "border-radius: 20px;")
        self.notificationButton.setObjectName("notificationButton")

        self.notificationButton.clicked.connect(self.notifikacijeCallFunc)

        self.vracanjeKnjigaButton = QtWidgets.QPushButton(self.centralwidget)
        self.vracanjeKnjigaButton.setGeometry(QtCore.QRect(430, 310, 121, 89))
        self.vracanjeKnjigaButton.setAutoFillBackground(False)
        self.vracanjeKnjigaButton.setStyleSheet("background-color: #E7E3D5;\n"
                                                "color: #527770;\n"
                                                "border-radius: 20px;")
        self.vracanjeKnjigaButton.setObjectName("vracanjeKnjigaButton")
        self.vracanjeKnjigaButton.clicked.connect(self.vracanje_knjiga)
        self.vracanjeKnjigaButton.clicked.connect(bibliotekarMain.hide)

        self.clanoviButton = QtWidgets.QPushButton(self.centralwidget)
        self.clanoviButton.setGeometry(QtCore.QRect(280, 420, 161, 51))
        self.clanoviButton.setAutoFillBackground(False)
        self.clanoviButton.setStyleSheet("background-color: #86ACA5;\n"
                                         "color: #FFFFFF;\n"
                                         "border-radius: 15px;")
        self.clanoviButton.setObjectName("clanoviButton")

        self.clanoviButton.clicked.connect(self.clanoviCallFunc)
        self.clanoviButton.clicked.connect(bibliotekarMain.hide)

        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.odjavaButton = QtWidgets.QPushButton(self.widget_2)
        self.odjavaButton.setGeometry(QtCore.QRect(50, 460, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.odjavaButton.setFont(font)
        self.odjavaButton.setAutoFillBackground(False)
        self.odjavaButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.odjavaButton.setObjectName("izvestajiButton_2")

        self.odjavaButton.clicked.connect(self.odjavaCallFunc)
        self.odjavaButton.clicked.connect(bibliotekarMain.hide)

        self.izdavanjeKnjigaButton = QtWidgets.QPushButton(self.centralwidget)
        self.izdavanjeKnjigaButton.setGeometry(QtCore.QRect(300, 310, 121, 89))
        self.izdavanjeKnjigaButton.setAutoFillBackground(False)
        self.izdavanjeKnjigaButton.setStyleSheet("background-color: #E7E3D5;\n"
                                                 "color: #527770;\n"
                                                 "border-radius: 20px;")

        self.izdavanjeKnjigaButton.clicked.connect(self.izdavanje_knjiga)
        self.izdavanjeKnjigaButton.clicked.connect(bibliotekarMain.hide)

        self.izdavanjeKnjigaButton.setObjectName("izdavanjeKnjigaButton")
        self.pregledZauzecaButton = QtWidgets.QPushButton(self.centralwidget)
        self.pregledZauzecaButton.setGeometry(QtCore.QRect(170, 310, 121, 89))
        self.pregledZauzecaButton.setAutoFillBackground(False)
        self.pregledZauzecaButton.setStyleSheet("background-color: #E7E3D5;\n"
                                                "color: #527770;\n"
                                                "border-radius: 20px;")
        self.pregledZauzecaButton.setObjectName("pregledZauzecaButton")

        self.pregledZauzecaButton.clicked.connect(self.pregled_zaduzeca)
        self.pregledZauzecaButton.clicked.connect(bibliotekarMain.hide)

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 211))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(20, 20, 241, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")
        self.rezervacijeButton = QtWidgets.QPushButton(self.centralwidget)
        self.rezervacijeButton.setGeometry(QtCore.QRect(560, 310, 121, 89))
        self.rezervacijeButton.setAutoFillBackground(False)
        self.rezervacijeButton.setStyleSheet("background-color: #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "border-radius: 20px;")
        self.rezervacijeButton.setObjectName("rezervacijeButton")

        self.rezervacijeButton.clicked.connect(self.pregled_rezervacija)
        self.rezervacijeButton.clicked.connect(bibliotekarMain.hide)

        bibliotekarMain.setCentralWidget(self.centralwidget)

        self.retranslateUi(bibliotekarMain)
        QtCore.QMetaObject.connectSlotsByName(bibliotekarMain)

    def pregled_zaduzeca(self):
        from src.View.book_view.debitSearch import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def izdavanje_knjiga(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def vracanje_knjiga(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def pregled_rezervacija(self):
        from src.View.book_view.reservationSearch import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def clanoviCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.CreateMemberPage import Ui_kreirajKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_kreirajKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def notifikacijeCallFunc(self):
        from src.View.workInProress import Ui_workInProgress

        self.window11 = QtWidgets.QMainWindow()
        self.ui = Ui_workInProgress()
        self.ui.setupUi(self.window11)
        self.window11.show()

        t = Timer(1, self.close_window)
        t.start()

    def close_window(self):
        self.window11.close()

    def odjavaCallFunc(self):
        from src.View.login.LogInWindow import Ui_login

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_login()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def retranslateUi(self, bibliotekarMain):
        _translate = QtCore.QCoreApplication.translate
        bibliotekarMain.setWindowTitle(_translate("bibliotekarMain", "MainWindow"))
        self.notificationButton.setText(_translate("bibliotekarMain", "posalji notifikacije"))
        self.vracanjeKnjigaButton.setText(_translate("bibliotekarMain", "vracanje knjiga"))
        self.clanoviButton.setText(_translate("bibliotekarMain", "clanovi"))
        self.odjavaButton.setText(_translate("bibliotekarMain", "odjava"))
        self.izdavanjeKnjigaButton.setText(_translate("bibliotekarMain", "izdavanje knjiga"))
        self.pregledZauzecaButton.setText(_translate("bibliotekarMain", "pregled zauzeca"))
        self.label.setText(_translate("bibliotekarMain", "Dobrodosli"))
        self.rezervacijeButton.setText(_translate("bibliotekarMain", "pregled rezervacija"))
