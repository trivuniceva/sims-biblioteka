from PyQt5 import QtCore, QtGui, QtWidgets
from threading import Timer


class Ui_AdministratorMainPage(object):

    def odjavaCallFunc(self):
        from src.View.login.LogInWindow import Ui_login

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_login()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, AdministratorMainPage):
        AdministratorMainPage.setObjectName("AdministratorMainPage")
        AdministratorMainPage.setFixedSize(960, 537)
        font = QtGui.QFont()
        font.setStrikeOut(False)
        AdministratorMainPage.setFont(font)
        AdministratorMainPage.setStyleSheet("background-color: #ffffff")
        self.centralwidget = QtWidgets.QWidget(AdministratorMainPage)
        self.centralwidget.setObjectName("centralwidget")

        self.izvestajiButton = QtWidgets.QPushButton(self.centralwidget)
        self.izvestajiButton.setGeometry(QtCore.QRect(280, 420, 161, 51))
        self.izvestajiButton.setAutoFillBackground(False)
        self.izvestajiButton.setStyleSheet("background-color: #86ACA5;\n"
                                           "color: #FFFFFF;\n"
                                           "border-radius: 15px;")
        self.izvestajiButton.setObjectName("izvestajiButton")

        self.izvestajiButton.clicked.connect(self.izvestajiCallFunc)
        # self.izvestajiButton.clicked.connect(AdministratorMainPage.hide)

        self.librarianButton = QtWidgets.QPushButton(self.centralwidget)
        self.librarianButton.setGeometry(QtCore.QRect(160, 310, 131, 89))
        self.librarianButton.setAutoFillBackground(False)
        self.librarianButton.setStyleSheet("background-color: #E7E3D5;\n"
                                           "color: #527770;\n"
                                           "border-radius: 20px;")
        self.librarianButton.setObjectName("librarianButton")

        self.librarianButton.clicked.connect(self.librarianCallFunc)
        self.librarianButton.clicked.connect(AdministratorMainPage.hide)

        self.booksButton = QtWidgets.QPushButton(self.centralwidget)
        self.booksButton.setGeometry(QtCore.QRect(310, 310, 131, 89))
        self.booksButton.setAutoFillBackground(False)
        self.booksButton.setStyleSheet("background-color: #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "border-radius: 20px;")
        self.booksButton.setObjectName("booksButton")

        self.booksButton.clicked.connect(AdministratorMainPage.hide)
        self.booksButton.clicked.connect(self.booksCallFunc)

        self.membershipButton = QtWidgets.QPushButton(self.centralwidget)
        self.membershipButton.setGeometry(QtCore.QRect(460, 310, 131, 89))
        self.membershipButton.setAutoFillBackground(False)
        self.membershipButton.setStyleSheet("background-color: #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "border-radius: 20px;")
        self.membershipButton.setObjectName("membershipButton")

        self.membershipButton.clicked.connect(self.membershipCallFunc)
        self.membershipButton.clicked.connect(AdministratorMainPage.hide)

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 211))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(20, 20, 241, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.odjavaButton = QtWidgets.QPushButton(self.widget_2)
        self.odjavaButton.setGeometry(QtCore.QRect(50, 460, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.odjavaButton.setFont(font)
        self.odjavaButton.setAutoFillBackground(False)
        self.odjavaButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.odjavaButton.setObjectName("izvestajiButton_2")

        self.odjavaButton.clicked.connect(AdministratorMainPage.hide)
        self.odjavaButton.clicked.connect(self.odjavaCallFunc)

        AdministratorMainPage.setCentralWidget(self.centralwidget)

        self.retranslateUi(AdministratorMainPage)
        QtCore.QMetaObject.connectSlotsByName(AdministratorMainPage)

    def retranslateUi(self, AdministratorMainPage):
        _translate = QtCore.QCoreApplication.translate
        AdministratorMainPage.setWindowTitle(_translate("AdministratorMainPage", "MainWindow"))
        self.izvestajiButton.setText(_translate("AdministratorMainPage", "izvestaji"))
        self.librarianButton.setText(_translate("AdministratorMainPage", "bibliotekar"))
        self.booksButton.setText(_translate("AdministratorMainPage", "knjige"))
        self.membershipButton.setText(_translate("AdministratorMainPage", "cenovnik"))
        self.label.setText(_translate("AdministratorMainPage", "Dobrodosli"))
        self.odjavaButton.setText(_translate("AdministratorMainPage", "odjava"))

    def librarianCallFunc(self):
        from src.View.users_view.admin_view.librarian_admin_view.CreateLibrarianPage import Ui_kreirajBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_kreirajBibliotekara()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def booksCallFunc(self):
        from src.View.book_view.addBook import UiMainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = UiMainWindow()
        self.ui.setupUi(self.window2)
        self.window2.show()

    def membershipCallFunc(self):
        from src.View.membership_view.kreirajCenovnikPage import Ui_KreirajCenovnik

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_KreirajCenovnik()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def izvestajiCallFunc(self):
        from src.View.workInProress import Ui_workInProgress

        self.window11 = QtWidgets.QMainWindow()
        self.ui = Ui_workInProgress()
        self.ui.setupUi(self.window11)
        self.window11.show()

        t = Timer(1, self.close_window)
        t.start()

    def close_window(self):
        self.window11.close()
