from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_kreirajBibliotekara(object):

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, kreirajBibliotekara):
        kreirajBibliotekara.setObjectName("kreirajBibliotekara")
        kreirajBibliotekara.setFixedSize(960, 537)
        kreirajBibliotekara.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(kreirajBibliotekara)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 531))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.kreirajOption = QtWidgets.QPushButton(self.widget_2)
        self.kreirajOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.kreirajOption.setAutoFillBackground(False)
        self.kreirajOption.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")
        self.kreirajOption.setObjectName("kreirajOption")

        self.izmeniOption = QtWidgets.QPushButton(self.widget_2)
        self.izmeniOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.izmeniOption.setAutoFillBackground(False)
        self.izmeniOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.izmeniOption.setObjectName("izmeniOption")

        self.izmeniOption.clicked.connect(self.izmeniCallFunc)
        self.izmeniOption.clicked.connect(kreirajBibliotekara.hide)

        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")

        self.obrisiOption.clicked.connect(self.deleteCallFunc)
        self.obrisiOption.clicked.connect(kreirajBibliotekara.hide)

        self.kreirajButton = QtWidgets.QPushButton(self.centralwidget)
        self.kreirajButton.setGeometry(QtCore.QRect(280, 330, 161, 51))

        font = QtGui.QFont()
        font.setPointSize(14)
        self.kreirajButton.setFont(font)
        self.kreirajButton.setAutoFillBackground(False)
        self.kreirajButton.setStyleSheet("background-color: #86ACA5;\n"
                                         "color: #FFFFFF;\n"
                                         "border-radius: 15px;")
        self.kreirajButton.setObjectName("kreirajButton")

        self.kreirajButton.clicked.connect(self.kreirajCallFunc)

        self.imeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.imeBox.setGeometry(QtCore.QRect(30, 120, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.imeBox.setFont(font)
        self.imeBox.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.imeBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.imeBox.setObjectName("imeBox")
        self.prezimeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.prezimeBox.setGeometry(QtCore.QRect(400, 120, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.prezimeBox.setFont(font)
        self.prezimeBox.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.prezimeBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.prezimeBox.setObjectName("prezimeBox")
        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setGeometry(QtCore.QRect(400, 210, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.emailBox.setFont(font)
        self.emailBox.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.emailBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.emailBox.setObjectName("emailBox")
        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setGeometry(QtCore.QRect(30, 210, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.jmbgBox.setFont(font)
        self.jmbgBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.jmbgBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.jmbgBox.setObjectName("jmbgBox")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(kreirajBibliotekara.hide)

        self.widget_2.raise_()
        self.kreirajButton.raise_()
        self.prezimeBox.raise_()
        self.emailBox.raise_()
        self.jmbgBox.raise_()
        self.imeBox.raise_()
        self.goBackButton.raise_()
        kreirajBibliotekara.setCentralWidget(self.centralwidget)
        self.statusBar = QtWidgets.QStatusBar(kreirajBibliotekara)
        self.statusBar.setObjectName("statusBar")
        kreirajBibliotekara.setStatusBar(self.statusBar)

        self.retranslateUi(kreirajBibliotekara)
        QtCore.QMetaObject.connectSlotsByName(kreirajBibliotekara)

    def retranslateUi(self, kreirajBibliotekara):
        _translate = QtCore.QCoreApplication.translate
        kreirajBibliotekara.setWindowTitle(_translate("kreirajBibliotekara", "MainWindow"))
        self.kreirajOption.setText(_translate("kreirajBibliotekara", "kreiraj"))
        self.izmeniOption.setText(_translate("kreirajBibliotekara", "izmeni"))
        self.obrisiOption.setText(_translate("kreirajBibliotekara", "obrisi"))
        self.kreirajButton.setText(_translate("kreirajBibliotekara", "kreiraj"))
        self.imeBox.setPlaceholderText(_translate("kreirajBibliotekara", "ime:"))
        self.prezimeBox.setPlaceholderText(_translate("kreirajBibliotekara", "prezime:"))
        self.emailBox.setPlaceholderText(_translate("kreirajBibliotekara", "email:"))
        self.jmbgBox.setPlaceholderText(_translate("kreirajBibliotekara", "jmbg:"))

    def kreirajCallFunc(self):
        from src.Controller.sending_emails import autogenerate_username, autogenerate_password, send_username_password
        from src.Controller.users_controller.librarian_CRUD import librarian_CRUD

        username = autogenerate_username()
        password = autogenerate_password()

        name = self.imeBox.text()
        lastname = self.prezimeBox.text()
        email = self.emailBox.text()
        jmbg = self.jmbgBox.text()

        if name != '' and lastname != '' and email != '' and jmbg != '':
            libr = librarian_CRUD(username, password, name, lastname, jmbg, email, 'librarian')
            libr.create_librarian()

            send_username_password(username, password, self.emailBox.text())
            self.you_did_it()

    def izmeniCallFunc(self):
        from src.View.users_view.admin_view.librarian_admin_view.UpdateLibrarianPage import Ui_izmeniBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_izmeniBibliotekara()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def deleteCallFunc(self):
        from src.View.users_view.admin_view.librarian_admin_view.DeleteLibrarianPage import Ui_obrisiBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_obrisiBibliotekara()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def you_did_it(self):
        from src.View.login.obavestenje import Ui_Obavestenje

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_Obavestenje()
        self.ui.setupUi(self.window1, None, 'Kreirali ste bibliotekara')
        self.window1.show()