from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QListWidgetItem, QScrollBar


class Ui_prikazRezultata(object):

    def __init__(self, results, what_to_do, do_this, go_back_where):
        self.results = results
        self.what_to_do = what_to_do
        self.do_this = do_this
        self.go_back_where = go_back_where

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()

        if self.go_back_where == 'administrator':
            self.ui = Ui_AdministratorMainPage()
        else:
            self.ui = Ui_bibliotekarMain()

        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, prikazRezultata):
        prikazRezultata.setObjectName("prikazRezultata")
        prikazRezultata.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        prikazRezultata.setPalette(palette)
        prikazRezultata.setStyleSheet("background-color:#ffffff;")
        self.centralwidget = QtWidgets.QWidget(prikazRezultata)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(prikazRezultata.hide)

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 101))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(40, 20, 461, 61))
        font = QtGui.QFont()
        font.setPointSize(26)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")

        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(95, 181, 531, 251))
        self.listWidget.setObjectName("listWidget")

        self.scroll_bar = QScrollBar()

        self.scroll_bar.setStyleSheet('''
                            QScrollBar:vertical {
                                border: none;
                                background: #ddd7c4;
                                width: 20px;
                                margin: 15px 0 15px 0;
                                border-radius: 0px;
                             }
                            
                            /*  HANDLE BAR VERTICAL */
                            QScrollBar::handle:vertical {	
                                background-color: #86ACA5;
                                min-height: 76px;
                                border-radius: 7px;
                            }
                            QScrollBar::handle:vertical:hover{	
                                background-color: #577F77;
                            }
                            QScrollBar::handle:vertical:pressed {	
                                background-color: #577F77;
                            }
                            
                            /* BTN TOP - SCROLLBAR */
                            QScrollBar::sub-line:vertical {
                                border: none;
                                background-color: #86ACA5;
                                height: 15px;
                                border-top-left-radius: 7px;
                                border-top-right-radius: 7px;
                                subcontrol-position: top;
                                subcontrol-origin: margin;
                            }
                            QScrollBar::sub-line:vertical:hover {	
                                background-color: #577F77;
                            }
                            QScrollBar::sub-line:vertical:pressed {	
                                background-color: #577F77;
                            }
                            
                            /* BTN BOTTOM - SCROLLBAR */
                            QScrollBar::add-line:vertical {
                                border: none;
                                background-color: #86ACA5;
                                height: 15px;
                                border-bottom-left-radius: 7px;
                                border-bottom-right-radius: 7px;
                                subcontrol-position: bottom;
                                subcontrol-origin: margin;
                            }
                            QScrollBar::add-line:vertical:hover {	
                                background-color: #577F77;
                            }
                            QScrollBar::add-line:vertical:pressed {	
                                background-color: #577F77;
                            }
                            
                            /* RESET ARROW */
                            QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {
                                background: none;
                            }
                            QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                                background: none;
                            }
        ''')

        self.listWidget.setVerticalScrollBar(self.scroll_bar)

        for i in range(len(self.results)):
            name = self.results[i].get_name()
            lastname = self.results[i].get_lastname()
            self.item = QListWidgetItem(name + ' ' + lastname)
            self.listWidget.addItem(self.item)

        self.listWidget.itemClicked.connect(prikazRezultata.hide)
        self.listWidget.itemClicked.connect(self.show_librarian)

        self.listWidget.setStyleSheet('background-color:#e7e3d5;'
                                      'color:#86ACA5;'
                                      'border-radius:8px;'
                                      'font-size:32px;'
                                      'scroll_bar-color:#86ACA5;'
                                      )

        self.prikaziButton = QtWidgets.QPushButton(self.centralwidget)
        self.prikaziButton.setGeometry(QtCore.QRect(270, 460, 161, 51))
        self.prikaziButton.setAutoFillBackground(False)
        self.prikaziButton.setStyleSheet("background-color: #86ACA5;\n"
                                         "color: #FFFFFF;\n"
                                         "border-radius: 15px;")
        self.prikaziButton.setObjectName("prikaziButton")
        prikazRezultata.setCentralWidget(self.centralwidget)

        self.retranslateUi(prikazRezultata)
        QtCore.QMetaObject.connectSlotsByName(prikazRezultata)

    def retranslateUi(self, prikazRezultata):
        _translate = QtCore.QCoreApplication.translate
        prikazRezultata.setWindowTitle(_translate("prikazRezultata", "MainWindow"))
        self.label.setText(_translate("prikazRezultata", "REZULTATI PRETRAGE"))
        self.prikaziButton.setText(_translate("prikazRezultata", self.what_to_do))

    def show_librarian(self):
        from src.View.users_view.admin_view.librarian_admin_view.showLibrarianPage import Ui_prikazBibliotekara

        index = self.listWidget.currentRow()
        display_user_obj = self.results[index]

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_prikazBibliotekara(display_user_obj, self.what_to_do, self.do_this, self.go_back_where)
        self.ui.setupUi(self.window1)
        self.window1.show()
