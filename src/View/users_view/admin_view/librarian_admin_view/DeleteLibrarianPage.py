from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_obrisiBibliotekara(object):

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, obrisiBibliotekara):
        obrisiBibliotekara.setObjectName("obrisiBibliotekara")
        obrisiBibliotekara.setFixedSize(960, 537)
        font = QtGui.QFont()
        font.setPointSize(15)
        obrisiBibliotekara.setFont(font)
        obrisiBibliotekara.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(obrisiBibliotekara)
        self.centralwidget.setObjectName("centralwidget")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(obrisiBibliotekara.hide)

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 171))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(20, 20, 441, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:#ffffff;")
        self.label_2.setObjectName("label_2")
        self.imeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.imeBox.setGeometry(QtCore.QRect(20, 330, 331, 31))
        self.imeBox.setStyleSheet("background-color: #ffffff;\n"
                                  "borde-radius: 2px;\n"
                                  "color: #527770;\n"
                                  "padding-left:8px;")
        self.imeBox.setText("")
        self.imeBox.setObjectName("imeBox")
        self.usernameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.usernameBox.setGeometry(QtCore.QRect(20, 280, 331, 31))
        self.usernameBox.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.usernameBox.setText("")
        self.usernameBox.setObjectName("usernameBox")
        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setGeometry(QtCore.QRect(20, 380, 331, 31))
        self.emailBox.setStyleSheet("background-color: #ffffff;\n"
                                    "borde-radius: 2px;\n"
                                    "color: #527770;\n"
                                    "padding-left:8px;")
        self.emailBox.setText("")
        self.emailBox.setObjectName("emailBox")
        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setGeometry(QtCore.QRect(380, 380, 331, 31))
        self.jmbgBox.setStyleSheet("background-color: #ffffff;\n"
                                   "borde-radius: 2px;\n"
                                   "color: #527770;\n"
                                   "padding-left:8px;")
        self.jmbgBox.setText("")
        self.jmbgBox.setObjectName("jmbgBox")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.kreirajOption = QtWidgets.QPushButton(self.widget_2)
        self.kreirajOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.kreirajOption.setAutoFillBackground(False)
        self.kreirajOption.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")
        self.kreirajOption.setObjectName("kreirajOption")

        self.kreirajOption.clicked.connect(self.kreirajCallFunc)
        self.kreirajOption.clicked.connect(obrisiBibliotekara.hide)

        self.izmeniOption = QtWidgets.QPushButton(self.widget_2)
        self.izmeniOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.izmeniOption.setAutoFillBackground(False)
        self.izmeniOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.izmeniOption.setObjectName("izmeniOption")

        self.izmeniOption.clicked.connect(self.izmeniCallFunc)
        self.izmeniOption.clicked.connect(obrisiBibliotekara.hide)

        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")
        self.prikaziRezultateButton = QtWidgets.QPushButton(self.centralwidget)
        self.prikaziRezultateButton.setGeometry(QtCore.QRect(280, 440, 161, 51))
        self.prikaziRezultateButton.setAutoFillBackground(False)
        self.prikaziRezultateButton.setStyleSheet("background-color: #86ACA5;\n"
                                                  "color: #FFFFFF;\n"
                                                  "border-radius: 15px;")
        self.prikaziRezultateButton.setObjectName("prikaziRezultateButton")

        self.prikaziRezultateButton.clicked.connect(self.search_results)
        self.prikaziRezultateButton.clicked.connect(obrisiBibliotekara.hide)

        self.prezimeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.prezimeBox.setGeometry(QtCore.QRect(380, 330, 331, 31))
        self.prezimeBox.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.prezimeBox.setText("")
        self.prezimeBox.setObjectName("prezimeBox")

        self.passwordBox = QtWidgets.QLineEdit(self.centralwidget)
        self.passwordBox.setGeometry(QtCore.QRect(380, 280, 331, 31))
        self.passwordBox.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.passwordBox.setText("")
        self.passwordBox.setObjectName("passwordBox")

        obrisiBibliotekara.setCentralWidget(self.centralwidget)

        self.retranslateUi(obrisiBibliotekara)
        QtCore.QMetaObject.connectSlotsByName(obrisiBibliotekara)

    def retranslateUi(self, obrisiBibliotekara):
        _translate = QtCore.QCoreApplication.translate
        obrisiBibliotekara.setWindowTitle(_translate("obrisiBibliotekara", "MainWindow"))
        self.label_2.setText(_translate("obrisiBibliotekara", "obrisi bibliotekara"))
        self.imeBox.setPlaceholderText(_translate("obrisiBibliotekara", "ime: "))
        self.usernameBox.setPlaceholderText(_translate("obrisiBibliotekara", "korisnicko ime (direktna pretraga):"))
        self.emailBox.setPlaceholderText(_translate("obrisiBibliotekara", "email:"))
        self.jmbgBox.setPlaceholderText(_translate("obrisiBibliotekara", "jmbg:"))
        self.kreirajOption.setText(_translate("obrisiBibliotekara", "kreiraj"))
        self.izmeniOption.setText(_translate("obrisiBibliotekara", "izmeni"))
        self.obrisiOption.setText(_translate("obrisiBibliotekara", "obrisi"))
        self.prikaziRezultateButton.setText(_translate("obrisiBibliotekara", "prikazi rezultate"))
        self.prezimeBox.setPlaceholderText(_translate("obrisiBibliotekara", "prezime:"))
        self.passwordBox.setPlaceholderText(_translate("izmeniBibliotekara", "sifra:"))

    def kreirajCallFunc(self):
        from src.View.users_view.admin_view.librarian_admin_view.CreateLibrarianPage import Ui_kreirajBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_kreirajBibliotekara()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def izmeniCallFunc(self):
        from src.View.users_view.admin_view.librarian_admin_view.UpdateLibrarianPage import Ui_izmeniBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_izmeniBibliotekara()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def search_results(self):
        from src.Controller.users_controller.users_check import check_if_username_used, direct_search, multicriteria_search

        if check_if_username_used(self.usernameBox.text()):
            display_user_obj = direct_search(self.usernameBox.text())
            self.show_librarian(display_user_obj)

        else:
            results = multicriteria_search()

            print('1 display: -------------------->')
            print(results)

            if self.give_conditions(results):
                results = self.give_conditions(results)
            else:
                self.show_results(list(results))

    def give_conditions(self, results):
        from src.Controller.users_controller.users_check import password_search, name_search, lastname_search, \
            email_search, jmbg_search

        result = results

        if self.passwordBox.text() != '':
            result = password_search(results, self.passwordBox.text())

        if self.imeBox.text() != '':
            result = name_search(results, self.imeBox.text())

        if self.prezimeBox.text() != '':
            result = lastname_search(results, self.prezimeBox.text())

        if self.emailBox.text() != '':
            result = email_search(results, self.emailBox.text())

        if self.jmbgBox.text() != '':
            result = jmbg_search(results, self.jmbgBox.text())

        else:
            return False

        return result

    def show_librarian(self, display_user_obj):
        from src.View.users_view.admin_view.librarian_admin_view.showLibrarianPage import Ui_prikazBibliotekara

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_prikazBibliotekara(display_user_obj, 'obrisi', 'delete', 'administrator')
        self.ui.setupUi(self.window1)
        self.window1.show()

    def show_results(self, results):
        from src.View.users_view.admin_view.librarian_admin_view.showResultsLibrarianPage import Ui_prikazRezultata

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_prikazRezultata(results, 'obrisi', 'delete', 'administrator')
        self.ui.setupUi(self.window1)
        self.window1.show()
