from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_prikazBibliotekara(object):

    def __init__(self, display_user_obj, what_to_do, do_this, go_back_where):
        self.display_user_obj = display_user_obj
        self.what_to_do = what_to_do
        self.do_this = do_this
        self.go_back_where = go_back_where

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()

        if self.go_back_where == 'administrator':
            self.ui = Ui_AdministratorMainPage()
        else:
            self.ui = Ui_bibliotekarMain()

        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, prikazBibliotekara):
        prikazBibliotekara.setObjectName("prikazBibliotekara")
        prikazBibliotekara.setFixedSize(960, 537)
        prikazBibliotekara.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(prikazBibliotekara)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(50, 230, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setStyleSheet("color: #86ACA5;")
        self.label.setObjectName("label")
        self.nameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.nameBox.setEnabled(True)
        self.nameBox.setGeometry(QtCore.QRect(139, 230, 181, 22))
        self.nameBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.nameBox.setObjectName("nameBox")

        self.nameBox.setText(self.display_user_obj.get_name())

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(50, 300, 61, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color: #86ACA5;")
        self.label_3.setObjectName("label_3")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget.setStyleSheet("background-color: #86ACA5;")
        self.widget.setObjectName("widget")
        self.deleteButton = QtWidgets.QPushButton(self.widget)
        self.deleteButton.setGeometry(QtCore.QRect(20, 430, 200, 89))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.deleteButton.setFont(font)
        self.deleteButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.deleteButton.setObjectName("deleteButton")

        self.deleteButton.clicked.connect(self.change_user)

        self.usernameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.usernameBox.setEnabled(True)
        self.usernameBox.setGeometry(QtCore.QRect(290, 160, 201, 22))
        self.usernameBox.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.usernameBox.setObjectName("usernameBox")

        self.usernameBox.setText(self.display_user_obj.get_username())

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(207, 160, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setStyleSheet("color: #86ACA5;")
        self.label_4.setObjectName("label_4")
        self.jmbgBox = QtWidgets.QLineEdit(self.centralwidget)
        self.jmbgBox.setEnabled(True)
        self.jmbgBox.setGeometry(QtCore.QRect(140, 300, 180, 22))
        self.jmbgBox.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        self.jmbgBox.setObjectName("jmbgBox")

        self.jmbgBox.setText(self.display_user_obj.get_jmbg())

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(prikazBibliotekara.hide)

        self.emailBox = QtWidgets.QLineEdit(self.centralwidget)
        self.emailBox.setEnabled(True)
        self.emailBox.setGeometry(QtCore.QRect(490, 300, 180, 22))
        self.emailBox.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.emailBox.setObjectName("emailBox")

        self.emailBox.setText(self.display_user_obj.get_email())

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(400, 230, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color: #86ACA5;")
        self.label_2.setObjectName("label_2")
        self.lastnameBox = QtWidgets.QLineEdit(self.centralwidget)
        self.lastnameBox.setEnabled(True)
        self.lastnameBox.setGeometry(QtCore.QRect(489, 230, 181, 22))
        self.lastnameBox.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.lastnameBox.setObjectName("lastnameBox")

        self.lastnameBox.setText(self.display_user_obj.get_lastname())

        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(400, 300, 51, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color: #86ACA5;")
        self.label_9.setObjectName("label_9")
        prikazBibliotekara.setCentralWidget(self.centralwidget)

        self.retranslateUi(prikazBibliotekara)
        QtCore.QMetaObject.connectSlotsByName(prikazBibliotekara)

    def retranslateUi(self, prikazBibliotekara):
        _translate = QtCore.QCoreApplication.translate
        prikazBibliotekara.setWindowTitle(_translate("prikazBibliotekara", "MainWindow"))
        self.label.setText(_translate("prikazBibliotekara", "Ime:"))
        self.label_3.setText(_translate("prikazBibliotekara", "Jmbg:"))
        self.deleteButton.setText(_translate("prikazBibliotekara", self.what_to_do))
        self.label_4.setText(_translate("prikazBibliotekara", "Username:"))
        self.label_2.setText(_translate("prikazBibliotekara", "Prezime:"))
        self.label_9.setText(_translate("prikazBibliotekara", "Email:"))

    def change_user(self):
        from src.Controller.users_controller.librarian_CRUD import librarian_CRUD
        from src.Controller.users_controller.users_check import check_if_username_used

        username = self.display_user_obj.get_username()

        libr = librarian_CRUD(username, self.display_user_obj.get_password(), self.display_user_obj.get_name(),
                              self.display_user_obj.get_lastname(), self.display_user_obj.get_jmbg(),
                              self.display_user_obj.get_email(), 'librarian')

        if self.do_this == 'delete':
            libr.delete_librarian(username)
        else:
            # check if user change username or something else
            if self.usernameBox.text() == username:
                libr.update_librarian(self.usernameBox.text(), self.nameBox.text(), self.lastnameBox.text(),
                                      self.jmbgBox.text(), self.emailBox.text())
            else:
                if check_if_username_used(username):
                    print('pop up window that username is used')
