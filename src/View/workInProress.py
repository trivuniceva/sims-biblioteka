from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
import time


class Ui_workInProgress(object):

    def setupUi(self, workInProgress):
        workInProgress.setObjectName("workInProgress")
        workInProgress.setFixedSize(960, 537)
        self.pictureLabel = QtWidgets.QLabel(workInProgress)
        self.pictureLabel.setGeometry(QtCore.QRect(50, 40, 851, 451))
        self.pictureLabel.setText("")
        self.pictureLabel.setObjectName("pictureLabel")

        pixmap = QPixmap('C:\\Users\\Nikolina\\Desktop\\sims_projekat\\src\\View\\work-in-progress-2ohayi.jpg')
        self.pictureLabel.setPixmap(pixmap)

        self.pictureLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.retranslateUi(workInProgress)
        QtCore.QMetaObject.connectSlotsByName(workInProgress)

    def retranslateUi(self, workInProgress):
        _translate = QtCore.QCoreApplication.translate
        workInProgress.setWindowTitle(_translate("workInProgress", "Form"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    workInProgress = QtWidgets.QWidget()
    ui = Ui_workInProgress()
    ui.setupUi(workInProgress)
    workInProgress.show()
    sys.exit(app.exec_())
