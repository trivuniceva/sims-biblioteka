from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_UserViewWindow(object):
    def setupUi(self, UserViewWindow, parent, loggedUser):
        self.parent = parent
        self.logged_user = loggedUser

        UserViewWindow.setObjectName("UserViewWindow")
        UserViewWindow.setFixedSize(960, 537)
        size_policy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(UserViewWindow.sizePolicy().hasHeightForWidth())
        UserViewWindow.setSizePolicy(size_policy)
        UserViewWindow.setStyleSheet("background-color:#ffffff")
        self.widget = QtWidgets.QWidget(UserViewWindow)
        self.widget.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget.setStyleSheet("background-color: #86ACA5;")
        self.widget.setObjectName("widget")
        self.izmenaBtn = QtWidgets.QPushButton(self.widget)
        self.izmenaBtn.setGeometry(QtCore.QRect(20, 430, 200, 89))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.izmenaBtn.setFont(font)
        self.izmenaBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "border-radius: 20px;")
        self.izmenaBtn.setObjectName("izmenaBtn")
        self.izmenaBtn.clicked.connect(self.izmenaBtnClicked)
        self.imeTB = QtWidgets.QLineEdit(UserViewWindow)
        self.imeTB.setEnabled(False)
        self.imeTB.setGeometry(QtCore.QRect(110, 170, 210, 22))
        self.imeTB.setStyleSheet("border: none;\n"
                                 "border-bottom: 2px solid #E7E3D5;\n"
                                 "color: #527770;\n"
                                 "\n"
                                 "")
        self.imeTB.setObjectName("imeTB")
        self.imeTB.setText(loggedUser.get_name())

        self.prezimeTB = QtWidgets.QLineEdit(UserViewWindow)
        self.prezimeTB.setEnabled(False)
        self.prezimeTB.setGeometry(QtCore.QRect(425, 170, 150, 22))
        self.prezimeTB.setStyleSheet("border: none;\n"
                                     "border-bottom: 2px solid #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "\n"
                                     "")
        self.prezimeTB.setObjectName("prezimeTB")
        self.prezimeTB.setText(loggedUser.get_lastname())

        self.usernameTB = QtWidgets.QLineEdit(UserViewWindow)
        self.usernameTB.setEnabled(False)
        self.usernameTB.setGeometry(QtCore.QRect(140, 240, 180, 22))
        self.usernameTB.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.usernameTB.setObjectName("usernameTB")
        self.usernameTB.setText(loggedUser.get_username())

        self.telefonTB = QtWidgets.QLineEdit(UserViewWindow)
        self.telefonTB.setEnabled(False)
        self.telefonTB.setGeometry(QtCore.QRect(140, 310, 180, 22))
        self.telefonTB.setStyleSheet("border: none;\n"
                                     "border-bottom: 2px solid #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "\n"
                                     "")
        self.telefonTB.setObjectName("telefonTB")
        self.telefonTB.setText(loggedUser.get_telephone_number())

        self.brClKarteTB = QtWidgets.QLineEdit(UserViewWindow)
        self.brClKarteTB.setEnabled(False)
        self.brClKarteTB.setGeometry(QtCore.QRect(370, 100, 113, 22))
        self.brClKarteTB.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.brClKarteTB.setObjectName("brClKarteTB")
        self.brClKarteTB.setText(loggedUser.get_id_membership())

        self.adresaTB = QtWidgets.QLineEdit(UserViewWindow)
        self.adresaTB.setEnabled(False)
        self.adresaTB.setGeometry(QtCore.QRect(140, 380, 180, 22))
        self.adresaTB.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.adresaTB.setObjectName("adresaTB")
        self.adresaTB.setText(loggedUser.get_address())

        self.label = QtWidgets.QLabel(UserViewWindow)
        self.label.setGeometry(QtCore.QRect(50, 170, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(UserViewWindow)
        self.label_2.setGeometry(QtCore.QRect(350, 170, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(UserViewWindow)
        self.label_3.setGeometry(QtCore.QRect(50, 240, 91, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(UserViewWindow)
        self.label_4.setGeometry(QtCore.QRect(210, 100, 141, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(UserViewWindow)
        self.label_5.setGeometry(QtCore.QRect(50, 310, 81, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(UserViewWindow)
        self.label_6.setGeometry(QtCore.QRect(50, 380, 71, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(UserViewWindow)
        self.label_7.setGeometry(QtCore.QRect(350, 240, 121, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")

        '''
        self.dateEdit = QtWidgets.QDateEdit(UserViewWindow)
        self.dateEdit.setEnabled(False)
        self.dateEdit.setGeometry(QtCore.QRect(490, 240, 110, 22))
        self.dateEdit.setObjectName("dateEdit")
        '''

        self.dateEdit = QtWidgets.QLineEdit(UserViewWindow)
        self.dateEdit.setEnabled(False)
        self.dateEdit.setGeometry(QtCore.QRect(490, 240, 110, 22))
        self.dateEdit.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.dateEdit.setObjectName("adresaTB")
        self.dateEdit.setText(loggedUser.get_birth_date())

        self.izmenaLozinkeBtn = QtWidgets.QPushButton(UserViewWindow)
        self.izmenaLozinkeBtn.setEnabled(True)
        self.izmenaLozinkeBtn.setGeometry(QtCore.QRect(370, 310, 200, 89))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.izmenaLozinkeBtn.setFont(font)
        self.izmenaLozinkeBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "border-radius: 20px;")
        self.izmenaLozinkeBtn.setObjectName("izmenaLozinkeBtn")
        self.izmenaLozinkeBtn.clicked.connect(self.ChangePasswordWindowOpen)

        self.nazadBtn = QtWidgets.QPushButton(UserViewWindow)
        self.nazadBtn.setGeometry(QtCore.QRect(10, 10, 24, 24))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        self.nazadBtn.setFont(font)
        self.nazadBtn.setAutoFillBackground(False)
        self.nazadBtn.setStyleSheet("background-color: none;\n"
                                    "\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "border-radius: 20px;")
        self.nazadBtn.setObjectName("nazadBtn")

        self.retranslateUi(UserViewWindow)
        QtCore.QMetaObject.connectSlotsByName(UserViewWindow)

    def retranslateUi(self, UserViewWindow):
        _translate = QtCore.QCoreApplication.translate
        UserViewWindow.setWindowTitle(_translate("UserViewWindow", "User View Window"))
        self.izmenaBtn.setText(_translate("UserViewWindow", "Izmeni\n"
                                                            "podatke"))
        self.label.setText(_translate("UserViewWindow", "Ime:"))
        self.label_2.setText(_translate("UserViewWindow", "Prezime:"))
        self.label_3.setText(_translate("UserViewWindow", "Username:"))
        self.label_4.setText(_translate("UserViewWindow", "br. Članske karte:"))
        self.label_5.setText(_translate("UserViewWindow", "Telefon:"))
        self.label_6.setText(_translate("UserViewWindow", "Adresa:"))
        self.label_7.setText(_translate("UserViewWindow", "Datum rodjenja:"))
        self.izmenaLozinkeBtn.setText(_translate("UserViewWindow", "Izmeni\n"
                                                                   "lozinku"))
        self.nazadBtn.setText(_translate("UserViewWindow", "<"))
        self.nazadBtn.clicked.connect(self.parent.nazad)

    def izmenaBtnClicked(self):
        if self.izmenaBtn.text() == "Izmeni\npodatke":
            self.enableChange()
        else:
            self.saveChanges()

    def enableChange(self):
        self.imeTB.setEnabled(True)
        self.prezimeTB.setEnabled(True)
        self.usernameTB.setEnabled(True)
        self.dateEdit.setEnabled(True)
        self.telefonTB.setEnabled(True)
        self.adresaTB.setEnabled(True)
        self.izmenaBtn.setText("Sacuvaj\nIzmene")

    def saveChanges(self):
        from src.View.login.obavestenje import Ui_Obavestenje
        from src.Controller.users_controller.member_CRUD import member_CRUD

        self.imeTB.setEnabled(False)
        self.prezimeTB.setEnabled(False)
        self.usernameTB.setEnabled(False)
        self.dateEdit.setEnabled(False)
        self.telefonTB.setEnabled(False)
        self.adresaTB.setEnabled(False)
        self.izmenaBtn.setText("Izmeni\npodatke")

        mb = member_CRUD(self.logged_user.get_id_membership(), self.logged_user.get_telephone_number(),
                         self.logged_user.get_address(), self.logged_user.get_membership_type(),
                         self.logged_user.get_birth_date(), self.logged_user.get_username(),
                         self.logged_user.get_password(), self.logged_user.get_name(), self.logged_user.get_lastname(),
                         self.logged_user.get_jmbg(), self.logged_user.get_email(), 'member')

        mb.update_member(self.usernameTB.text(),
                         self.logged_user.get_password(), self.imeTB.text(), self.prezimeTB.text(),
                         self.logged_user.get_jmbg(), self.logged_user.get_email(),
                         self.telefonTB.text(), self.adresaTB.text(),
                         self.dateEdit.text(), self.logged_user.get_membership_type())

        self.Obavestenje = QtWidgets.QWidget()
        obavestenje_ui = Ui_Obavestenje()
        obavestenje_ui.setupUi(self.Obavestenje, self, "Podaci izmenjeni")
        self.Obavestenje.show()

    def nazad(self):
        self.Obavestenje.hide()
        self.parent.nazad()

    def ChangePasswordWindowOpen(self):
        from src.View.login.IzmenaLozinkeWindow import Ui_IzmenaLozinkeWindow

        self.IzmenaLozinkeWindow = QtWidgets.QWidget()
        izmena_lozinke_ui = Ui_IzmenaLozinkeWindow()
        izmena_lozinke_ui.setupUi(self.IzmenaLozinkeWindow, self.logged_user)
        self.IzmenaLozinkeWindow.show()
