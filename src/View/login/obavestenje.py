from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Obavestenje(object):
    def setupUi(self, Obavestenje, parent, message):
        self.parent = parent
        self.message = message
        Obavestenje.setObjectName("Obavestenje")
        Obavestenje.setFixedSize(500, 330)
        Obavestenje.setStyleSheet("background-color:#ffffff")
        self.widget = QtWidgets.QWidget(Obavestenje)
        self.widget.setGeometry(QtCore.QRect(75, 75, 350, 150))
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(0, 55, 350, 40))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#fff;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(Obavestenje)
        self.pushButton.setGeometry(QtCore.QRect(75, 240, 350, 60))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("background-color: #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "border-radius: 20px;")
        self.pushButton.setObjectName("pushButton")

        if self.parent is None:
            self.pushButton.clicked.connect(Obavestenje.hide)
        else:
            self.pushButton.clicked.connect(self.parent.nazad)

        self.retranslateUi(Obavestenje)
        QtCore.QMetaObject.connectSlotsByName(Obavestenje)

    def retranslateUi(self, Obavestenje):
        _translate = QtCore.QCoreApplication.translate
        Obavestenje.setWindowTitle(_translate("Obavestenje", "Obavestenje"))
        self.label.setText(_translate("Obavestenje", self.message))
        self.pushButton.setText(_translate("Obavestenje", "OK"))

