from PyQt5 import QtCore, QtGui, QtWidgets
from threading import Timer


class Ui_UserMain(object):
    def setupUi(self, UserMain, parent, loggedUser):  # izmenjeno
        self.loggedUser = loggedUser  # izmenjeno
        self.parent = parent  # izmenjeno
        self.UserMain = UserMain  # izmenjeno
        self.UserMain.setObjectName("UserMain")  # izmenjeno
        self.UserMain.setFixedSize(960, 537)  # izmenjeno
        self.UserMain.setStyleSheet("backgroun-color:#ffffff;")  # izmenjeno
        self.widget = QtWidgets.QWidget(UserMain)
        self.widget.setGeometry(QtCore.QRect(720, 0, 240, 537))
        self.widget.setStyleSheet("background-color: #86ACA5;")
        self.widget.setObjectName("widget")
        self.odjavaBtn = QtWidgets.QPushButton(self.widget)
        self.odjavaBtn.setGeometry(QtCore.QRect(60, 470, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.odjavaBtn.setFont(font)
        self.odjavaBtn.setAutoFillBackground(False)
        self.odjavaBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "border-radius: 15px;")
        self.odjavaBtn.setObjectName("odjavaBtn")

        self.odjavaBtn.clicked.connect(parent.odjava)  # izmenjeno

        self.widget_2 = QtWidgets.QWidget(UserMain)
        self.widget_2.setGeometry(QtCore.QRect(50, 60, 621, 211))
        self.widget_2.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget_2.setObjectName("widget_2")
        self.label = QtWidgets.QLabel(self.widget_2)
        self.label.setGeometry(QtCore.QRect(20, 20, 241, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")
        self.pregledProfilaBtn = QtWidgets.QPushButton(UserMain)
        self.pregledProfilaBtn.setGeometry(QtCore.QRect(70, 310, 131, 89))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pregledProfilaBtn.setFont(font)
        self.pregledProfilaBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "border-radius: 20px;")
        self.pregledProfilaBtn.setObjectName("pregledProfilaBtn")
        self.pregledProfilaBtn.clicked.connect(self.openUserView)  # izmenjeno
        self.pretraziKnjiguBtn = QtWidgets.QPushButton(UserMain)
        self.pretraziKnjiguBtn.setGeometry(QtCore.QRect(220, 310, 131, 89))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pretraziKnjiguBtn.setFont(font)
        self.pretraziKnjiguBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "border-radius: 20px;")
        self.pretraziKnjiguBtn.setObjectName("pretraziKnjiguBtn")

        self.pretraziKnjiguBtn.clicked.connect(self.user_search_book)

        self.produziClanarinuBtn = QtWidgets.QPushButton(UserMain)
        self.produziClanarinuBtn.setGeometry(QtCore.QRect(370, 310, 131, 89))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.produziClanarinuBtn.setFont(font)
        self.produziClanarinuBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                               "color: #527770;\n"
                                               "border-radius: 20px;")
        self.produziClanarinuBtn.setObjectName("produziClanarinuBtn")

        self.produziClanarinuBtn.clicked.connect(self.produziClanarinuCallFunc)

        self.zaduzenjaBtn = QtWidgets.QPushButton(UserMain)
        self.zaduzenjaBtn.setGeometry(QtCore.QRect(520, 310, 150, 89))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.zaduzenjaBtn.setFont(font)
        self.zaduzenjaBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.zaduzenjaBtn.setObjectName("zaduzenjaBtn")

        self.zaduzenjaBtn.clicked.connect(self.zaduzenjaCallFunc)

        self.retranslateUi(UserMain)
        QtCore.QMetaObject.connectSlotsByName(UserMain)

    def retranslateUi(self, UserMain):
        _translate = QtCore.QCoreApplication.translate
        UserMain.setWindowTitle(_translate("UserMain", "User Main Window"))
        self.odjavaBtn.setText(_translate("UserMain", "Odjava"))
        self.label.setText(_translate("UserMain", "Dobrodosli"))
        self.pregledProfilaBtn.setText(_translate("UserMain", "Pregled\n"
                                                              "profila"))
        self.pretraziKnjiguBtn.setText(_translate("UserMain", "Pretraga\n"
                                                              "knjiga"))
        self.produziClanarinuBtn.setText(_translate("UserMain", "Produzi\n"
                                                                "clanarinu"))
        self.zaduzenjaBtn.setText(_translate("UserMain", "Zaduženja\n"
                                                         "----------------\n"
                                                         "Rezervacije"))

    def openUserView(self):
        from src.View.login.UserViewWindow import Ui_UserViewWindow

        self.UserMain.hide()
        self.window = QtWidgets.QWidget()
        self.ui = Ui_UserViewWindow()
        self.ui.setupUi(self.window, self, self.loggedUser)
        self.window.show()

    def nazad(self):
        self.window.hide()
        self.UserMain.show()

    def user_search_book(self):
        from src.View.book_view.searchBooks import Ui_MainWindow
        self.UserMain.hide()
        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.loggedUser,self.parent)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def zaduzenjaCallFunc(self):
        from src.View.workInProress import Ui_workInProgress

        self.window11 = QtWidgets.QMainWindow()
        self.ui = Ui_workInProgress()
        self.ui.setupUi(self.window11)
        self.window11.show()

        t = Timer(1, self.close_window)
        t.start()

    def close_window(self):
        self.window11.close()

    def produziClanarinuCallFunc(self):
        from src.View.workInProress import Ui_workInProgress

        self.window121 = QtWidgets.QMainWindow()
        self.ui = Ui_workInProgress()
        self.ui.setupUi(self.window121)
        self.window121.show()

        t = Timer(1, self.close_12)
        t.start()

    def close_12(self):
        self.window121.close()
