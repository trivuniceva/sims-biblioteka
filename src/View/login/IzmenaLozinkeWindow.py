from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_IzmenaLozinkeWindow(object):
    def setupUi(self, IzmenaLozinkeWindow, logged_user):
        self.logged_user = logged_user

        self.IzmenaLozinkeWindow = IzmenaLozinkeWindow
        IzmenaLozinkeWindow.setObjectName("IzmenaLozinkeWindow")
        IzmenaLozinkeWindow.setWindowModality(QtCore.Qt.NonModal)
        IzmenaLozinkeWindow.resize(400, 400)
        IzmenaLozinkeWindow.setStyleSheet("background-color:#f5f5f5;")
        self.label = QtWidgets.QLabel(IzmenaLozinkeWindow)
        self.label.setGeometry(QtCore.QRect(100, 50, 200, 20))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#527770;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(IzmenaLozinkeWindow)
        self.label_2.setGeometry(QtCore.QRect(100, 130, 200, 20))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:#527770;")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(IzmenaLozinkeWindow)
        self.label_3.setGeometry(QtCore.QRect(100, 210, 200, 20))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet("color:#527770;")
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.lineEdit = QtWidgets.QLineEdit(IzmenaLozinkeWindow)
        self.lineEdit.setGeometry(QtCore.QRect(125, 90, 150, 22))
        self.lineEdit.setStyleSheet("border: none;\n"
                                    "border-bottom: 2px solid #E7E3D5;\n"
                                    "color: #527770;\n"
                                    "\n"
                                    "")
        self.lineEdit.setObjectName("lineEdit")
        self.lineEdit_2 = QtWidgets.QLineEdit(IzmenaLozinkeWindow)
        self.lineEdit_2.setGeometry(QtCore.QRect(125, 170, 150, 22))
        self.lineEdit_2.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.lineEdit_3 = QtWidgets.QLineEdit(IzmenaLozinkeWindow)
        self.lineEdit_3.setGeometry(QtCore.QRect(120, 250, 150, 22))
        self.lineEdit_3.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.lineEdit_3.setObjectName("lineEdit_3")

        self.pushButton = QtWidgets.QPushButton(IzmenaLozinkeWindow)
        self.pushButton.setGeometry(QtCore.QRect(25, 300, 150, 80))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pushButton.setFont(font)
        self.pushButton.setStyleSheet("background-color: #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "border-radius: 20px;")
        self.pushButton.setObjectName("pushButton")

        self.pushButton.clicked.connect(self.SacuvajIzmene)
        self.pushButton.clicked.connect(IzmenaLozinkeWindow.hide)

        self.pushButton_2 = QtWidgets.QPushButton(IzmenaLozinkeWindow)
        self.pushButton_2.setGeometry(QtCore.QRect(225, 300, 150, 80))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.pushButton_2.setFont(font)
        self.pushButton_2.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(IzmenaLozinkeWindow.hide)

        self.retranslateUi(IzmenaLozinkeWindow)
        QtCore.QMetaObject.connectSlotsByName(IzmenaLozinkeWindow)

    def retranslateUi(self, IzmenaLozinkeWindow):
        _translate = QtCore.QCoreApplication.translate
        IzmenaLozinkeWindow.setWindowTitle(_translate("IzmenaLozinkeWindow", "Izmena lozinke"))
        self.label.setText(_translate("IzmenaLozinkeWindow", "Unesite staru lozinku"))
        self.label_2.setText(_translate("IzmenaLozinkeWindow", "Unseite novu lozinku:"))
        self.label_3.setText(_translate("IzmenaLozinkeWindow", "Ponovite novu lozinku"))
        self.pushButton.setText(_translate("IzmenaLozinkeWindow", "Sačuvaj\n"
                                                                  "izmene"))
        self.pushButton_2.setText(_translate("IzmenaLozinkeWindow", "Odustani"))

    def SacuvajIzmene(self):
        print('<3')

        from src.Controller.users_controller.member_CRUD import member_CRUD

        mb = member_CRUD(self.logged_user.get_id_membership(), self.logged_user.get_telephone_number(),
                         self.logged_user.get_address(), self.logged_user.get_membership_type(),
                         self.logged_user.get_birth_date(), self.logged_user.get_username(),
                         self.logged_user.get_password(), self.logged_user.get_name(), self.logged_user.get_lastname(),
                         self.logged_user.get_jmbg(), self.logged_user.get_email(), self.logged_user.role())

        new_password2 = self.lineEdit_2.text()
        new_password3 = self.lineEdit_3.text()

        if new_password2 == new_password3:
            mb.update_member(self.logged_user.get_username(),
                             new_password2, self.logged_user.get_name(), self.logged_user.get_lastname(),
                             self.logged_user.get_jmbg(), self.logged_user.get_email(),
                             self.logged_user.get_telephone_number(), self.logged_user.get_address(),
                             self.logged_user.get_birth_date(), self.logged_user.get_membership_type())
