from PyQt5.QtGui import QPixmap

from src.View.login.UserMainWindow import Ui_UserMain
import src.Controller.login.LogIn as log
from PyQt5 import QtCore, QtGui, QtWidgets
import sys


class Ui_login(object):

    def setupUi(self, login):
        self.login = login
        self.login.setObjectName("login")
        self.login.setStyleSheet("background-color:#ffffff;")
        self.login.setFixedSize(960, 537)
        self.centralwidget = QtWidgets.QWidget(login)
        self.centralwidget.setObjectName("centralwidget")
        self.emailQLineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.emailQLineEdit.setGeometry(QtCore.QRect(90, 160, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.emailQLineEdit.setFont(font)
        self.emailQLineEdit.setStyleSheet("border: none;\n"
                                          "border-bottom: 2px solid #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "\n"
                                          "")
        self.emailQLineEdit.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.emailQLineEdit.setObjectName("emailQLineEdit")

        self.passwordQLineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.passwordQLineEdit.setGeometry(QtCore.QRect(90, 230, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.passwordQLineEdit.setFont(font)
        self.passwordQLineEdit.setStyleSheet("border: none;\n"
                                             "border-bottom: 2px solid #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "\n"
                                             "")
        self.passwordQLineEdit.setCursorMoveStyle(QtCore.Qt.LogicalMoveStyle)
        self.passwordQLineEdit.setObjectName("passwordQLineEdit")
        self.passwordQLineEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.loginButton = QtWidgets.QPushButton(self.centralwidget)
        self.loginButton.setGeometry(QtCore.QRect(125, 310, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.loginButton.setFont(font)
        self.loginButton.setAutoFillBackground(False)
        self.loginButton.setStyleSheet("background-color: #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "border-radius: 15px;")
        self.loginButton.setObjectName("loginButton")
        self.loginButton.clicked.connect(self.Login)

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(420, 0, 541, 541))
        self.label.setStyleSheet("background-color: #86ACA5;\n")
        self.label.setText("")
        self.label.setObjectName("label")

        # pixmap = QPixmap('../paper.png')
        pixmap1 = QPixmap('../toppng.com-open-book-2186x1031.png')
        # self.label.setPixmap(pixmap)
        self.label.setPixmap(pixmap1)

        # self.label.setAlignment(QtCore.Qt.AlignCenter)

        self.login.setCentralWidget(self.centralwidget)

        self.retranslateUi(login)
        QtCore.QMetaObject.connectSlotsByName(login)

    def retranslateUi(self, login):
        _translate = QtCore.QCoreApplication.translate
        self.login.setWindowTitle(_translate("login", "MainWindow"))
        self.emailQLineEdit.setPlaceholderText(_translate("login", "username:"))
        self.passwordQLineEdit.setPlaceholderText(_translate("login", "password:"))
        self.loginButton.setText(_translate("login", "login"))

    def Login(self):

        username = self.emailQLineEdit.text()
        password = self.passwordQLineEdit.text()

        logged_user = log.login(username, password)
        #        log.login(username,password)

        print('logged_user: ', logged_user)
        if logged_user is not None:
            self.emailQLineEdit.clear()
            self.passwordQLineEdit.clear()
            self.login.hide()
            self.window = QtWidgets.QWidget()
            if logged_user.get_role() == "member":
                self.ui = Ui_UserMain()

                self.ui.setupUi(self.window, self, logged_user)
                self.window.show()
            else:
                self.check_users_role(logged_user)

        else:
            print("Greska")

        # login.setVisible(True)

    def odjava(self):
        self.window.hide()
        self.login.show()

    def check_users_role(self, logged_user):
        if logged_user.get_role() == "administrator":
            from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

            self.window1 = QtWidgets.QMainWindow()
            self.ui = Ui_AdministratorMainPage()
            self.ui.setupUi(self.window1)
            self.window1.show()

        elif logged_user.get_role() == "librarian":
            from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

            self.window2 = QtWidgets.QMainWindow()
            self.ui = Ui_bibliotekarMain(logged_user)
            self.ui.setupUi(self.window2)
            self.window2.show()

    def membersCallFunc(self):
        from src.View.users_view.librarian_view.member_librarian_view.CreateMemberPage import Ui_kreirajKorisnika

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_kreirajKorisnika()
        self.ui.setupUi(self.window1)
        self.window1.show()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    login = QtWidgets.QMainWindow()
    ui = Ui_login()
    ui.setupUi(login)
    login.show()
    sys.exit(app.exec_())
