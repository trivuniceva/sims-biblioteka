from PyQt5 import QtCore, QtGui, QtWidgets


class DebitView(object):
    def __init__(self, logged_user=None):
        self.logged_user = logged_user

    def goToBookReturn(self):
        from src.View.book_view.returnDebit import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToBookSearch(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToReservationSearch(self):
        from src.View.book_view.debitSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToDebitSearch(self):
        from src.View.book_view.reservationSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def make_debit(self, MainWindow, isbn):
        MainWindow.resize(960, 537)
        MainWindow.setStyleSheet("background-color: rgb(255, 255, 255);")
        MainWindow.setWindowTitle("Biblioteka")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.makeDebitBtn = QtWidgets.QPushButton(self.centralwidget)
        self.makeDebitBtn.setGeometry(QtCore.QRect(280, 347, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.makeDebitBtn.setFont(font)
        self.makeDebitBtn.setAutoFillBackground(False)
        self.makeDebitBtn.setStyleSheet("background-color: #86ACA5;color: #FFFFFF;border-radius: 15px;")
        self.makeDebitBtn.setText("iznajmi knjigu")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(MainWindow.hide)
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 551))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")

        self.returnDebitBtn = QtWidgets.QPushButton(self.widget_2)
        self.returnDebitBtn.setGeometry(QtCore.QRect(20, 40, 191, 51))
        self.returnDebitBtn.setFont(font)
        self.returnDebitBtn.setAutoFillBackground(False)
        self.returnDebitBtn.setStyleSheet("background-color: #E7E3D5;color: #527770;border-radius: 15px;")
        self.returnDebitBtn.setText("vracanje knjige")
        self.returnDebitBtn.clicked.connect(self.goToBookReturn)
        self.returnDebitBtn.clicked.connect(MainWindow.close)


        self.searchBookBtn = QtWidgets.QPushButton(self.widget_2)
        self.searchBookBtn.setGeometry(QtCore.QRect(20, 110, 191, 51))
        self.searchBookBtn.setFont(font)
        self.searchBookBtn.setAutoFillBackground(False)
        self.searchBookBtn.setStyleSheet("background-color: #E7E3D5;color: #527770;border-radius: 15px;")
        self.searchBookBtn.setText("pretraga knjiga")
        self.searchBookBtn.clicked.connect(self.goToBookSearch)
        self.searchBookBtn.clicked.connect(MainWindow.close)
        self.allDebitsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allDebitsBtn.setGeometry(QtCore.QRect(20, 180, 191, 51))
        self.allDebitsBtn.setFont(font)
        self.allDebitsBtn.setAutoFillBackground(False)
        self.allDebitsBtn.setStyleSheet("background-color: #E7E3D5;color: #527770;border-radius: 15px;")
        self.allDebitsBtn.setText("pregled zaduzenja")
        self.allDebitsBtn.clicked.connect(self.goToDebitSearch)
        self.allDebitsBtn.clicked.connect(MainWindow.close)
        self.allReservationsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allReservationsBtn.setGeometry(QtCore.QRect(20, 250, 191, 51))
        self.allReservationsBtn.setFont(font)
        self.allReservationsBtn.setAutoFillBackground(False)
        self.allReservationsBtn.setStyleSheet("background-color: #E7E3D5;color: #527770;border-radius: 15px;")
        self.allReservationsBtn.setText("pregled rezervacija")
        self.allReservationsBtn.clicked.connect(self.goToReservationSearch)
        self.allReservationsBtn.clicked.connect(MainWindow.close)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(80, 130, 200, 61))
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:  #86ACA5; padding:2px;")
        self.label_2.setText("Novo zaduzenje")

        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbnTxt = QtWidgets.QLineEdit(self.centralwidget)
        self.isbnTxt.setGeometry(QtCore.QRect(80, 200, 281, 51))
        self.isbnTxt.setFont(font)
        self.isbnTxt.setStyleSheet("border: none;border-bottom: 2px solid #E7E3D5;color: #527770;")
        self.isbnTxt.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.isbnTxt.setPlaceholderText("isbn:")
        self.isbnTxt.setText(isbn)
        self.idMemberTxt = QtWidgets.QLineEdit(self.centralwidget)
        self.idMemberTxt.setGeometry(QtCore.QRect(380, 200, 281, 51))
        self.idMemberTxt.setFont(font)
        self.idMemberTxt.setStyleSheet("border: none;border-bottom: 2px solid #E7E3D5;color: #527770;")
        self.idMemberTxt.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.idMemberTxt.setPlaceholderText("id clana:")

        self.makeDebitBtn.clicked.connect(self.make_debit_clicked)

        MainWindow.setCentralWidget(self.centralwidget)

    def make_debit_clicked(self):
        from src.Controller.book_controller.debitService import DebitControl
        dc = DebitControl()
        msg_box = QtWidgets.QMessageBox()
        ret = msg_box.warning(msg_box, "Upozorenje!", "Da li ste sigurni da zelite da izdate knjigu?", QtWidgets.
                              QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if ret == msg_box.Ok:
            if dc.make_debit(self.idMemberTxt.text(), self.isbnTxt.text(), self.logged_user.get_username()):
                self.isbnTxt.setText("")
                self.idMemberTxt.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Knjiga je uspesno izdata.")
            else:
                self.isbnTxt.setText("")
                self.idMemberTxt.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Doslo je do greske. Knjiga ne moze biti izdata.")
