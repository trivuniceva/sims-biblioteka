from PyQt5 import QtCore, QtGui, QtWidgets


class UiMainWindow(object):

    def show_message(self):
        from src.View.book_view.SuccessAddedBook import Ui_SuccessfullyAddedBookMessage

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_SuccessfullyAddedBookMessage()
        self.ui.setupUi(self.window2)

        self.window2.show()

    def add_created_book(self, wind):
        from src.Controller.book_controller.BookController import add_book

        if self.titleInput.text() != "" and self.authorInput.text() != "" and self.numOfPagesInput.text() != "" and \
                self.yearInput.text() != "" and self.numOfItemsInput.text() != "":
            add_book(self.titleInput.text(), self.authorInput.text(), self.category.currentText(),
                     self.genre.currentText(), self.numOfPagesInput.text(), self.yearInput.text(),
                     self.numOfItemsInput.text(), self.nacinIzdavanja.currentText())

            self.show_message()

    def go_to_update_book_wind(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow("update")
        self.ui.setupUi(self.window2)
        self.window2.show()

    def go_to_delete_book_wind(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow("delete")
        self.ui.setupUi(self.window2)
        self.window2.show()

    def go_back_to_meni(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        MainWindow.setToolTip("")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.titleInput = QtWidgets.QLineEdit(self.centralwidget)
        self.titleInput.setGeometry(QtCore.QRect(50, 70, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.titleInput.setFont(font)
        self.titleInput.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.titleInput.setInputMethodHints(QtCore.Qt.ImhUppercaseOnly)
        self.titleInput.setText("")
        self.titleInput.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.titleInput.setObjectName("titleInput")
        self.authorInput = QtWidgets.QLineEdit(self.centralwidget)
        self.authorInput.setGeometry(QtCore.QRect(50, 140, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.authorInput.setFont(font)
        self.authorInput.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.authorInput.setInputMethodHints(QtCore.Qt.ImhUppercaseOnly)
        self.authorInput.setText("")
        self.authorInput.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.authorInput.setObjectName("authorInput")
        self.numOfPagesInput = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfPagesInput.setGeometry(QtCore.QRect(380, 140, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.numOfPagesInput.setFont(font)
        self.numOfPagesInput.setStyleSheet("border: none;\n"
                                           "border-bottom: 2px solid #E7E3D5;\n"
                                           "color: #527770;\n"
                                           "\n"
                                           "")
        self.numOfPagesInput.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfPagesInput.setText("")
        self.numOfPagesInput.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfPagesInput.setObjectName("numOfPagesInput")
        self.yearInput = QtWidgets.QLineEdit(self.centralwidget)
        self.yearInput.setGeometry(QtCore.QRect(380, 70, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.yearInput.setFont(font)
        self.yearInput.setStyleSheet("border: none;\n"
                                     "border-bottom: 2px solid #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "\n"
                                     "")
        self.yearInput.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.yearInput.setText("")
        self.yearInput.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.yearInput.setObjectName("yearInput")
        self.numOfItemsInput = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfItemsInput.setGeometry(QtCore.QRect(380, 210, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.numOfItemsInput.setFont(font)
        self.numOfItemsInput.setStyleSheet("border: none;\n"
                                           "border-bottom: 2px solid #E7E3D5;\n"
                                           "color: #527770;\n"
                                           "\n"
                                           "")
        self.numOfItemsInput.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfItemsInput.setText("")
        self.numOfItemsInput.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfItemsInput.setObjectName("numOfItemsInput")
        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.createOption = QtWidgets.QPushButton(self.sideMeni)
        self.createOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.createOption.setAutoFillBackground(False)
        self.createOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.createOption.setObjectName("createOption")
        self.updateOption = QtWidgets.QPushButton(self.sideMeni)
        self.updateOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.updateOption.setAutoFillBackground(False)
        self.updateOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.updateOption.setObjectName("updateOption")

        # status pretrage je za izmenu
        self.updateOption.clicked.connect(self.go_to_update_book_wind)
        self.updateOption.clicked.connect(MainWindow.close)

        self.deleteOption = QtWidgets.QPushButton(self.sideMeni)
        self.deleteOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.deleteOption.setAutoFillBackground(False)
        self.deleteOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.deleteOption.setObjectName("deleteOption")

        # status pretrage je za brisanje

        self.deleteOption.clicked.connect(self.go_to_delete_book_wind)
        self.deleteOption.clicked.connect(MainWindow.close)

        self.create = QtWidgets.QPushButton(self.centralwidget)
        self.create.setGeometry(QtCore.QRect(270, 390, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.create.setFont(font)
        self.create.setAutoFillBackground(False)
        self.create.setStyleSheet("background-color: #86ACA5;\n"
                                  "color: #FFFFFF;\n"
                                  "border-radius: 15px;")
        self.create.setObjectName("create")

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Midlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Dark, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Mid, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.AlternateBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ToolTipBase, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.goBackButton.setPalette(palette)
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back_to_meni)
        self.goBackButton.clicked.connect(MainWindow.close)

        self.nacinIzdavanja = QtWidgets.QComboBox(self.centralwidget)
        self.nacinIzdavanja.setGeometry(QtCore.QRect(380, 280, 281, 51))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 120, 215))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.nacinIzdavanja.setPalette(palette)
        self.nacinIzdavanja.setAutoFillBackground(True)
        self.nacinIzdavanja.setEditable(True)
        self.nacinIzdavanja.setObjectName("nacinIzdavanja")
        self.nacinIzdavanja.addItem("")
        self.nacinIzdavanja.addItem("")
        self.nacinIzdavanja.addItem("")
        self.category = QtWidgets.QComboBox(self.centralwidget)
        self.category.setGeometry(QtCore.QRect(50, 210, 281, 51))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 120, 215))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.category.setPalette(palette)
        self.category.setAutoFillBackground(True)
        self.category.setEditable(True)
        self.category.setObjectName("comboBox_2")
        self.category.addItem("")
        self.category.addItem("")
        self.category.addItem("")
        self.category.addItem("")
        self.category.addItem("")
        self.genre = QtWidgets.QComboBox(self.centralwidget)
        self.genre.setGeometry(QtCore.QRect(50, 280, 281, 51))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 120, 215))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Highlight, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.genre.setPalette(palette)
        self.genre.setAutoFillBackground(True)
        self.genre.setEditable(True)
        self.genre.setObjectName("nacinIzdavanja_2")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        self.genre.addItem("")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.create.clicked.connect(self.add_created_book)
        self.create.clicked.connect(MainWindow.hide)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.titleInput.setPlaceholderText(_translate("MainWindow", "naslov:"))
        self.authorInput.setPlaceholderText(_translate("MainWindow", "autor:"))
        self.numOfPagesInput.setPlaceholderText(_translate("MainWindow", "broj stranica:"))
        self.yearInput.setPlaceholderText(_translate("MainWindow", "godina izdavanja:"))
        self.numOfItemsInput.setPlaceholderText(_translate("MainWindow", "broj dostupnih komada:"))
        self.createOption.setText(_translate("MainWindow", "kreiraj"))
        self.updateOption.setText(_translate("MainWindow", "izmeni"))
        self.deleteOption.setText(_translate("MainWindow", "obrisi"))
        self.create.setText(_translate("MainWindow", "kreiraj"))
        self.goBackButton.setText(_translate("MainWindow", "<"))
        self.nacinIzdavanja.setCurrentText(_translate("MainWindow", "broj dana trajanja zaduzenja:"))
        self.nacinIzdavanja.setPlaceholderText(_translate("MainWindow", "broj dana trajanja zaduzenja:"))
        self.nacinIzdavanja.setItemText(0, _translate("MainWindow", "28"))
        self.nacinIzdavanja.setItemText(1, _translate("MainWindow", "14"))
        self.nacinIzdavanja.setItemText(2, _translate("MainWindow", "0"))
        self.category.setCurrentText(_translate("MainWindow", "kategorija:"))
        self.category.setPlaceholderText(_translate("MainWindow", "kategorija:"))
        self.category.setItemText(0, _translate("MainWindow", "teen"))
        self.category.setItemText(1, _translate("MainWindow", "za decu"))
        self.category.setItemText(2, _translate("MainWindow", "za odrasle"))
        self.category.setItemText(3, _translate("MainWindow", "specijalne knjige"))
        self.category.setItemText(4, _translate("MainWindow", "skolske knjige"))
        self.genre.setCurrentText(_translate("MainWindow", "zanr:"))
        self.genre.setPlaceholderText(_translate("MainWindow", "broj dana trajanja zaduzenja:"))
        self.genre.setItemText(0, _translate("MainWindow", "autobiografija"))
        self.genre.setItemText(1, _translate("MainWindow", "akcija"))
        self.genre.setItemText(2, _translate("MainWindow", "basna"))
        self.genre.setItemText(3, _translate("MainWindow", "biografija"))
        self.genre.setItemText(4, _translate("MainWindow", "bajka"))
        self.genre.setItemText(5, _translate("MainWindow", "drama"))
        self.genre.setItemText(6, _translate("MainWindow", "erotika"))
        self.genre.setItemText(7, _translate("MainWindow", "fantastika"))
        self.genre.setItemText(8, _translate("MainWindow", "horor"))
        self.genre.setItemText(9, _translate("MainWindow", "istorija"))
        self.genre.setItemText(10, _translate("MainWindow", "klasika"))
        self.genre.setItemText(11, _translate("MainWindow", "kriminalistika"))
        self.genre.setItemText(12, _translate("MainWindow", "kuvar"))
        self.genre.setItemText(13, _translate("MainWindow", "komedija"))
        self.genre.setItemText(14, _translate("MainWindow", "letopis"))
        self.genre.setItemText(15, _translate("MainWindow", "ljubavna"))
        self.genre.setItemText(16, _translate("MainWindow", "mitologija"))
        self.genre.setItemText(17, _translate("MainWindow", "moderna psihologija"))
        self.genre.setItemText(18, _translate("MainWindow", "novela"))
        self.genre.setItemText(19, _translate("MainWindow", "poezija"))
        self.genre.setItemText(20, _translate("MainWindow", "psihologija"))
        self.genre.setItemText(21, _translate("MainWindow", "putopis"))
        self.genre.setItemText(22, _translate("MainWindow", "publistika"))
        self.genre.setItemText(23, _translate("MainWindow", "roman"))
        self.genre.setItemText(24, _translate("MainWindow", "sport i zdravlje"))
        self.genre.setItemText(25, _translate("MainWindow", "strip"))
        self.genre.setItemText(26, _translate("MainWindow", "triler"))
        self.genre.setItemText(27, _translate("MainWindow", "umetnost"))
        self.genre.setItemText(28, _translate("MainWindow", "vestern"))
        self.genre.setItemText(29, _translate("MainWindow", "ostalo"))
