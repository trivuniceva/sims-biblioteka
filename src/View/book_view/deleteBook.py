from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindowDeleteBook(object):

    def __init__(self, book):
        self.book = book

    def show_message(self):
        from src.View.book_view.SuccessDeletedBook import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.window2)

        self.window2.show()

    def goBackToBookMeni(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def goToUpdateBook(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow("update")
        self.ui.setupUi(self.window1)
        self.window1.show()

    def goToAddBookWind(self):
        from src.View.book_view.addBook import UiMainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = UiMainWindow()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def delete_displayed_book(self):
        from src.Controller.book_controller.BookController import delete_book

        delete_book(self.book.get_isbn())

        self.show_message()

    def setupUi(self, MainWindowDeleteBook):
        MainWindowDeleteBook.setObjectName("MainWindowDeleteBook")
        MainWindowDeleteBook.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindowDeleteBook.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindowDeleteBook)
        self.centralwidget.setObjectName("centralwidget")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBackToBookMeni)
        self.goBackButton.clicked.connect(MainWindowDeleteBook.close)

        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.createOption = QtWidgets.QPushButton(self.widget_2)
        self.createOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.createOption.setAutoFillBackground(False)
        self.createOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.createOption.setObjectName("createOption")

        self.createOption.clicked.connect(self.goToAddBookWind)
        self.createOption.clicked.connect(MainWindowDeleteBook.hide)

        self.updateOption = QtWidgets.QPushButton(self.widget_2)
        self.updateOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.updateOption.setAutoFillBackground(False)
        self.updateOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.updateOption.setObjectName("updateOption")

        self.updateOption.clicked.connect(self.goToUpdateBook)
        self.updateOption.clicked.connect(MainWindowDeleteBook.hide)

        self.deleteOption = QtWidgets.QPushButton(self.widget_2)
        self.deleteOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.deleteOption.setAutoFillBackground(False)
        self.deleteOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.deleteOption.setObjectName("deleteOption")
        self.authorUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.authorUpdate.setGeometry(QtCore.QRect(90, 200, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.authorUpdate.setFont(font)
        self.authorUpdate.setStyleSheet("border: none;\n"
                                        "border-bottom: 2px solid #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "\n"
                                        "")
        self.authorUpdate.setInputMethodHints(QtCore.Qt.ImhUppercaseOnly)
        self.authorUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.authorUpdate.setObjectName("authorUpdate")
        self.authorUpdate.setText(self.book.get_author())
        self.categoryUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.categoryUpdate.setGeometry(QtCore.QRect(90, 270, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.categoryUpdate.setFont(font)
        self.categoryUpdate.setStyleSheet("border: none;\n"
                                          "border-bottom: 2px solid #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "\n"
                                          "")
        self.categoryUpdate.setInputMethodHints(QtCore.Qt.ImhLowercaseOnly)
        self.categoryUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.categoryUpdate.setObjectName("categoryUpdate")
        self.categoryUpdate.setText(self.book.get_category())
        self.yearUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.yearUpdate.setGeometry(QtCore.QRect(380, 200, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.yearUpdate.setFont(font)
        self.yearUpdate.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.yearUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.yearUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.yearUpdate.setObjectName("yearUpdate")
        self.yearUpdate.setText(self.book.get_year_of_publication())
        self.isbndisplay = QtWidgets.QLabel(self.centralwidget)
        self.isbndisplay.setGeometry(QtCore.QRect(260, 60, 211, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbndisplay.setFont(font)
        self.isbndisplay.setStyleSheet("color:  #527777; padding:2px;")
        self.isbndisplay.setAlignment(QtCore.Qt.AlignCenter)
        self.isbndisplay.setObjectName("isbndisplay")
        self.isbndisplay.setText("ISBN :" + self.book.get_isbn())
        self.titleUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.titleUpdate.setGeometry(QtCore.QRect(90, 130, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.titleUpdate.setFont(font)
        self.titleUpdate.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.titleUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.titleUpdate.setObjectName("titleUpdate")
        self.titleUpdate.setText(self.book.get_title())
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(380, 340, 241, 41))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.comboBox.setPalette(palette)
        self.comboBox.setAutoFillBackground(True)
        self.comboBox.setEditable(True)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.deleteButton = QtWidgets.QPushButton(self.centralwidget)
        self.deleteButton.setGeometry(QtCore.QRect(450, 580, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.deleteButton.setFont(font)
        self.deleteButton.setAutoFillBackground(False)
        self.deleteButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.deleteButton.setObjectName("deleteButton")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 40, 581, 371))
        self.label.setStyleSheet("background-color: #86ACA5;\n"
                                 "border-radius:20px;")
        self.label.setText("")
        self.label.setObjectName("label")
        self.genreUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.genreUpdate.setGeometry(QtCore.QRect(90, 340, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.genreUpdate.setFont(font)
        self.genreUpdate.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.genreUpdate.setInputMethodHints(QtCore.Qt.ImhLowercaseOnly)
        self.genreUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.genreUpdate.setObjectName("genreUpdate")
        self.numOfItemsUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfItemsUpdate.setGeometry(QtCore.QRect(380, 270, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.numOfItemsUpdate.setFont(font)
        self.numOfItemsUpdate.setStyleSheet("border: none;\n"
                                            "border-bottom: 2px solid #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "\n"
                                            "")
        self.numOfItemsUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfItemsUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfItemsUpdate.setObjectName("numOfItemsUpdate")
        self.numOfPagesUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfPagesUpdate.setGeometry(QtCore.QRect(380, 130, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.numOfPagesUpdate.setFont(font)
        self.numOfPagesUpdate.setStyleSheet("border: none;\n"
                                            "border-bottom: 2px solid #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "\n"
                                            "")
        self.numOfPagesUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfPagesUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfPagesUpdate.setObjectName("numOfPagesUpdate")
        self.deleteButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.deleteButton_2.setGeometry(QtCore.QRect(270, 440, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.deleteButton_2.setFont(font)
        self.deleteButton_2.setAutoFillBackground(False)
        self.deleteButton_2.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.deleteButton_2.setObjectName("deleteButton_2")

        # dodati upozorenje

        self.deleteButton_2.clicked.connect(self.delete_displayed_book)
        self.deleteButton_2.clicked.connect(MainWindowDeleteBook.hide)

        self.label.raise_()
        self.goBackButton.raise_()
        self.widget_2.raise_()
        self.authorUpdate.raise_()
        self.categoryUpdate.raise_()
        self.yearUpdate.raise_()
        self.isbndisplay.raise_()
        self.titleUpdate.raise_()
        self.comboBox.raise_()
        self.deleteButton.raise_()
        self.genreUpdate.raise_()
        self.numOfItemsUpdate.raise_()
        self.numOfPagesUpdate.raise_()
        self.deleteButton_2.raise_()
        MainWindowDeleteBook.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindowDeleteBook)
        QtCore.QMetaObject.connectSlotsByName(MainWindowDeleteBook)

    def retranslateUi(self, MainWindowDeleteBook):
        _translate = QtCore.QCoreApplication.translate
        MainWindowDeleteBook.setWindowTitle(_translate("MainWindowDeleteBook", "MainWindow"))
        self.createOption.setText(_translate("MainWindowDeleteBook", "kreiraj"))
        self.updateOption.setText(_translate("MainWindowDeleteBook", "izmeni"))
        self.deleteOption.setText(_translate("MainWindowDeleteBook", "obrisi"))
        self.authorUpdate.setText(_translate("MainWindowDeleteBook", self.book.get_author()))
        self.authorUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "autor:"))
        self.categoryUpdate.setText(_translate("MainWindowDeleteBook", self.book.get_category()))
        self.categoryUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "kategorija:"))
        self.categoryUpdate.setReadOnly(True)
        self.yearUpdate.setText(_translate("MainWindowDeleteBook", 'godina izdavanja: ' +
                                           self.book.get_year_of_publication()))
        self.yearUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "godina izdavanja:"))
        self.yearUpdate.setReadOnly(True)
        self.titleUpdate.setText(_translate("MainWindowDeleteBook", self.book.get_title()))
        self.titleUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "naslov:"))
        self.titleUpdate.setReadOnly(True)
        self.comboBox.setCurrentText(_translate("MainWindowDeleteBook", "broj dana trajanja zaduzenja:"))
        self.comboBox.setPlaceholderText(_translate("MainWindowDeleteBook", "broj dana trajanja zaduzenja:"))
        self.comboBox.setItemText(0, _translate("MainWindowDeleteBook", "28"))
        self.comboBox.setItemText(1, _translate("MainWindowDeleteBook", "14"))
        self.comboBox.setItemText(2, _translate("MainWindowDeleteBook", "0"))
        self.comboBox.setCurrentText(self.book.get_book_status())
        self.comboBox.setEditable(False)

        self.deleteButton.setText(_translate("MainWindowDeleteBook", "izmeni"))
        self.genreUpdate.setText(_translate("MainWindowDeleteBook", self.book.get_genre()))
        self.genreUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "zanr:"))
        self.genreUpdate.setReadOnly(True)
        self.numOfItemsUpdate.setText(_translate("MainWindowDeleteBook", 'broj dostupnih komada: ' +
                                                 self.book.get_num_of_available_books()))
        self.numOfItemsUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "broj dostupnih komada:"))
        self.numOfItemsUpdate.setReadOnly(True)
        self.numOfPagesUpdate.setText(_translate("MainWindowDeleteBook", 'broj stranica: ' +
                                                 self.book.get_num_of_pages()))
        self.numOfPagesUpdate.setPlaceholderText(_translate("MainWindowDeleteBook", "broj stranica:"))
        self.numOfPagesUpdate.setReadOnly(True)
        self.deleteButton_2.setText(_translate("MainWindowDeleteBook", "obrisi"))
