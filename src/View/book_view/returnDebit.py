from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):

    def __init__(self, logged_user=None, book=None):
        self.book = book
        self.logged_user = logged_user

    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def return_debit(self):
        from src.Controller.book_controller.debitService import DebitControl
        dc = DebitControl()
        msg_box = QtWidgets.QMessageBox()
        ret = msg_box.warning(msg_box, "Upozorenje!", "Da li ste sigurni da zelite da vratite knjigu?", QtWidgets.
                              QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if ret == msg_box.Ok:
            if dc.make_debit(self.idMemberTxt.text(), self.isbnTxt.text(), self.logged_user.get_username()):
                self.isbnTxt.setText("")
                self.idMemberTxt.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Knjiga je uspesno vracena.")
            else:
                self.isbnTxt.setText("")
                self.idMemberTxt.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Doslo je do greske. Knjiga ne moze biti vracena.")

    def goToAddDebit(self):
        from src.View.book_view.makeDebit import DebitView

        self.window2 = QtWidgets.QMainWindow()
        self.ui = DebitView(self.logged_user)
        self.ui.make_debit(self.window2, None)
        self.window2.show()

    def goToBookSearch(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToReservationSearch(self):
        from src.View.book_view.reservationSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToDebitSearch(self):
        from src.View.book_view.debitSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(960, 537)
        MainWindow.setStyleSheet("background-color: rgb(255, 255, 255);")

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.returnDebit = QtWidgets.QPushButton(self.centralwidget)
        self.returnDebit.setGeometry(QtCore.QRect(280, 347, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.returnDebit.setFont(font)
        self.returnDebit.setAutoFillBackground(False)
        self.returnDebit.setStyleSheet("background-color: #86ACA5;\n"
                                        "color: #FFFFFF;\n"
                                        "border-radius: 15px;")
        self.returnDebit.setObjectName("returnDebit")
        self.returnDebit.clicked.connect(self.return_debit)
        self.returnDebit.clicked.connect(MainWindow.hide)
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(MainWindow.hide)
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 551))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        font = QtGui.QFont()
        font.setPointSize(14)

        self.makeDebit = QtWidgets.QPushButton(self.widget_2)
        self.makeDebit.setGeometry(QtCore.QRect(20, 40, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.makeDebit.setFont(font)
        self.makeDebit.setAutoFillBackground(False)
        self.makeDebit.setStyleSheet("background-color: #E7E3D5;\n"
                                           "color: #527770;\n"
                                           "border-radius: 15px;")
        self.makeDebit.setObjectName("makeDebit")
        self.makeDebit.clicked.connect(self.goToAddDebit)
        self.makeDebit.clicked.connect(MainWindow.hide)
        self.updateDebitBtn = QtWidgets.QPushButton(self.widget_2)
        self.updateDebitBtn.setGeometry(QtCore.QRect(20, 110, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.updateDebitBtn.setFont(font)
        self.updateDebitBtn.setAutoFillBackground(False)
        self.updateDebitBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.updateDebitBtn.setObjectName("updateDebitBtn")
        self.updateDebitBtn.clicked.connect(self.goToBookSearch)
        self.updateDebitBtn.clicked.connect(MainWindow.hide)
        self.allDebitsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allDebitsBtn.setGeometry(QtCore.QRect(20, 180, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allDebitsBtn.setFont(font)
        self.allDebitsBtn.setAutoFillBackground(False)
        self.allDebitsBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.allDebitsBtn.setObjectName("allDebitsBtn")
        self.allDebitsBtn.clicked.connect(self.goToDebitSearch)
        self.allDebitsBtn.clicked.connect(MainWindow.hide)
        self.allReservationsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allReservationsBtn.setGeometry(QtCore.QRect(20, 250, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allReservationsBtn.setFont(font)
        self.allReservationsBtn.setAutoFillBackground(False)
        self.allReservationsBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                              "color: #527770;\n"
                                              "border-radius: 15px;")
        self.allReservationsBtn.setObjectName("allReservationsBtn")
        self.allReservationsBtn.clicked.connect(self.goToReservationSearch)
        self.allReservationsBtn.clicked.connect(MainWindow.hide)
        self.isbnTxt = QtWidgets.QLineEdit(self.centralwidget)
        self.isbnTxt.setGeometry(QtCore.QRect(80, 200, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbnTxt.setFont(font)
        self.isbnTxt.setStyleSheet("border: none;\n"
                                   "border-bottom: 2px solid #E7E3D5;\n"
                                   "color: #527770;\n"
                                   "\n"
                                   "")
        if self.book:
            self.isbnTxt.setText(self.book)
        self.isbnTxt.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.isbnTxt.setObjectName("isbnTxt")
        self.idMemberTxt = QtWidgets.QLineEdit(self.centralwidget)
        self.idMemberTxt.setGeometry(QtCore.QRect(380, 200, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.idMemberTxt.setFont(font)
        self.idMemberTxt.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.idMemberTxt.setText("")
        self.idMemberTxt.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.idMemberTxt.setObjectName("idMemberTxt")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(80, 130, 200, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:  #86ACA5; padding:2px;")
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.returnDebit.setText(_translate("MainWindow", "razduzi knjigu"))
        
        self.makeDebit.setText(_translate("MainWindow", "novo zaduzenje"))
        self.updateDebitBtn.setText(_translate("MainWindow", "pretraga knjiga"))
        self.allDebitsBtn.setText(_translate("MainWindow", "pregled zaduzenja"))
        self.allReservationsBtn.setText(_translate("MainWindow", "pregled rezervacija"))
        self.isbnTxt.setPlaceholderText(_translate("MainWindow", "isbn:"))
        self.idMemberTxt.setPlaceholderText(_translate("MainWindow", "id clana:"))
        self.label_2.setText(_translate("MainWindow", "Razduzivanje"))
