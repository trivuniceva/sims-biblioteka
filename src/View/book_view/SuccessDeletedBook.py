from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):

    def go_to_meni(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window2)
        self.window2.show()

    def go_to_search_to_delete(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow("delete")
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        MainWindow.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(650, 0, 311, 551))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.sideMeni_2 = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni_2.setGeometry(QtCore.QRect(0, 0, 351, 551))
        self.sideMeni_2.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni_2.setObjectName("sideMeni_2")
        self.DeleteNew = QtWidgets.QPushButton(self.centralwidget)
        self.DeleteNew.setGeometry(QtCore.QRect(410, 330, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.DeleteNew.setFont(font)
        self.DeleteNew.setAutoFillBackground(False)
        self.DeleteNew.setStyleSheet("background-color: #86ACA5;\n"
                                     "color: #FFFFFF;\n"
                                     "border-radius: 15px;")
        self.DeleteNew.setObjectName("DeleteNew")

        self.DeleteNew.clicked.connect(self.go_to_search_to_delete)
        self.DeleteNew.clicked.connect(MainWindow.hide)

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(330, 190, 321, 41))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_3.setFont(font)
        self.label_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(350, 160, 301, 51))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label.setFont(font)
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(430, 230, 47, 13))
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.Meni = QtWidgets.QPushButton(self.centralwidget)
        self.Meni.setGeometry(QtCore.QRect(410, 280, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.Meni.setFont(font)
        self.Meni.setAutoFillBackground(False)
        self.Meni.setStyleSheet("background-color: #86ACA5;\n"
                                "color: #FFFFFF;\n"
                                "border-radius: 15px;")
        self.Meni.setObjectName("Meni")

        self.Meni.clicked.connect(self.go_to_meni)
        self.Meni.clicked.connect(MainWindow.hide)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.DeleteNew.setText(_translate("MainWindow", "Obrisi novu knjigu"))
        self.label_3.setText(_translate("MainWindow", "novu knjigu!"))
        self.label.setText(_translate("MainWindow", "Uspesno ste obrisali "))
        self.Meni.setText(_translate("MainWindow", "Meni"))
