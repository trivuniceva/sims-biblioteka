# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pretragaRezervacijaBibliotekar.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def __init__(self, logged_user):
        self.logged_user = logged_user

    def go_back(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def showResults(self, display):
        from src.View.book_view.reservationSearchResults import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(display, self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def search_reservations(self):
        from src.Controller.book_controller.reservationService import ReservationModel

        db = ReservationModel()

        isbn = self.isbnInput.text()
        member_id = self.member_id.text()
        result = db.search(isbn, member_id)

        self.showResults(result)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        MainWindow.setStyleSheet("background-color: #ffffff")

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 171))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(20, 20, 461, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:#ffffff;")
        self.label_2.setObjectName("label_2")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png)")
        self.goBackButton.setText("")
        self.goBackButton.setObjectName("goBackButton")
        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.restartButton_2 = QtWidgets.QPushButton(self.sideMeni)
        self.restartButton_2.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.restartButton_2.setAutoFillBackground(False)
        self.restartButton_2.setStyleSheet("background-color: #E7E3D5;\n"
"color: #527770;\n"
"border-radius: 20px;")
        self.restartButton_2.setObjectName("restartButton_2")
        self.isbnInput = QtWidgets.QLineEdit(self.centralwidget)
        self.isbnInput.setGeometry(QtCore.QRect(170, 300, 331, 31))
        self.isbnInput.setStyleSheet("background-color: #ffffff;\n"
"borde-radius: 2px;\n"
"color: #527770;\n"
"padding-left:8px;")

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(MainWindow.hide)

        self.isbnInput.setText("")
        self.isbnInput.setObjectName("isbnInput")
        self.showResultsButton = QtWidgets.QPushButton(self.centralwidget)
        self.showResultsButton.setGeometry(QtCore.QRect(250, 440, 161, 51))
        self.showResultsButton.setAutoFillBackground(False)
        self.showResultsButton.setStyleSheet("background-color: #86ACA5;\n"
"color: #FFFFFF;\n"
"border-radius: 15px;")
        self.showResultsButton.setObjectName("showResultsButton")
        self.showResultsButton.clicked.connect(self.search_reservations)

        self.showResultsButton.clicked.connect(MainWindow.hide)
        self.member_id = QtWidgets.QLineEdit(self.centralwidget)
        self.member_id.setGeometry(QtCore.QRect(170, 260, 331, 31))
        self.member_id.setStyleSheet("background-color: #ffffff;\n"
"borde-radius: 2px;\n"
"color: #527770;\n"
"padding-left:8px;")
        self.member_id.setText("")
        self.member_id.setObjectName("member_id")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_2.setText(_translate("MainWindow", "Pretraga rezervacija"))
        self.restartButton_2.setText(_translate("MainWindow", "restartuj filtere"))
        self.isbnInput.setPlaceholderText(_translate("MainWindow", "ISBN (direktna pretraga):"))
        self.showResultsButton.setText(_translate("MainWindow", "prikazi rezultate"))
        self.member_id.setPlaceholderText(_translate("MainWindow", "id clana:"))
