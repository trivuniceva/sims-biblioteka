import sys
from datetime import datetime

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_prikazZaduzenja(object):
    def __init__(self, debit, logged_user=None):
        self.logged_user = logged_user
        self.debit = debit
    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def return_debit(self):
        from src.Controller.book_controller.debitService import DebitControl
        dc = DebitControl()
        msg_box = QtWidgets.QMessageBox()
        ret = msg_box.warning(msg_box, "Upozorenje!", "Da li ste sigurni da zelite da vratite knjigu?", QtWidgets.
                              QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if ret == msg_box.Ok:
            if dc.return_debit(self.isbn.text(), self.member_id.text()):
                self.isbn.setText("")
                self.member_id.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Knjiga je uspesno vracena.")
                self.goBack()
            else:
                self.isbn.setText("")
                self.member_id.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Doslo je do greske. Knjiga ne moze biti vracena.")
                self.goBack()

    def goToAddDebit(self):
        from src.View.book_view.makeDebit import DebitView

        self.window2 = QtWidgets.QMainWindow()
        self.ui = DebitView(self.logged_user)
        self.ui.make_debit(self.window2, None)
        self.window2.show()

    def goToBookSearch(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToReservationSearch(self):
        from src.View.book_view.reservationSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToDebitSearch(self):
        from src.View.book_view.debitSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, prikazZaduzeenja):
        prikazZaduzeenja.setObjectName("prikazZaduzenja")
        prikazZaduzeenja.resize(960, 537)
        prikazZaduzeenja.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(prikazZaduzeenja)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 551))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.dodajZaduzenje = QtWidgets.QPushButton(self.widget_2)
        self.dodajZaduzenje.setGeometry(QtCore.QRect(20, 40, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(prikazZaduzeenja.hide)
        self.dodajZaduzenje.setFont(font)
        self.dodajZaduzenje.setAutoFillBackground(False)
        self.dodajZaduzenje.setStyleSheet("background-color: #E7E3D5;\n"
                                           "color: #527770;\n"
                                           "border-radius: 15px;")
        self.dodajZaduzenje.setObjectName("dodajZaduzenje")
        self.dodajZaduzenje.clicked.connect(self.goToAddDebit)
        self.dodajZaduzenje.clicked.connect(prikazZaduzeenja.hide)
        self.pretragaKnjiga = QtWidgets.QPushButton(self.widget_2)
        self.pretragaKnjiga.setGeometry(QtCore.QRect(20, 110, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pretragaKnjiga.setFont(font)
        self.pretragaKnjiga.setAutoFillBackground(False)
        self.pretragaKnjiga.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.pretragaKnjiga.setObjectName("pretragaKnjiga")
        self.pretragaKnjiga.clicked.connect(self.goToBookSearch)
        self.pretragaKnjiga.clicked.connect(prikazZaduzeenja.hide)
        self.allDebitsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allDebitsBtn.setGeometry(QtCore.QRect(20, 180, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allDebitsBtn.setFont(font)
        self.allDebitsBtn.setAutoFillBackground(False)
        self.allDebitsBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.allDebitsBtn.setObjectName("allDebitsBtn")
        self.allDebitsBtn.clicked.connect(self.goToDebitSearch)
        self.allDebitsBtn.clicked.connect(prikazZaduzeenja.hide)
        self.allReservationsBtn = QtWidgets.QPushButton(self.widget_2)
        self.allReservationsBtn.setGeometry(QtCore.QRect(20, 250, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allReservationsBtn.setFont(font)
        self.allReservationsBtn.setAutoFillBackground(False)
        self.allReservationsBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                              "color: #527770;\n"
                                              "border-radius: 15px;")
        self.allReservationsBtn.clicked.connect(self.goToReservationSearch)
        self.allReservationsBtn.clicked.connect(prikazZaduzeenja.hide)
        self.allReservationsBtn.setObjectName("allReservationsBtn")
        self.date = QtWidgets.QLineEdit(self.centralwidget)
        self.date.setGeometry(QtCore.QRect(350, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.date.setFont(font)
        self.date.setStyleSheet("border: none;\n"
                                        "border-bottom: 2px solid #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "\n"
                                        "")
        self.date.setText("")
        self.date.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.date.setObjectName("date")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(50, 110, 200, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:  #86ACA5; padding:2px;")
        self.label_2.setObjectName("label_2")
        self.isbn = QtWidgets.QLineEdit(self.centralwidget)
        self.isbn.setGeometry(QtCore.QRect(50, 180, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbn.setFont(font)
        self.isbn.setStyleSheet("border: none;\n"
                                "border-bottom: 2px solid #E7E3D5;\n"
                                "color: #527770;\n"
                                "\n"
                                "")
        self.isbn.setText("")
        self.isbn.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.isbn.setObjectName("isbn")
        self.book_name = QtWidgets.QLineEdit(self.centralwidget)
        self.book_name.setGeometry(QtCore.QRect(50, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.book_name.setFont(font)
        self.book_name.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.book_name.setText("")
        self.book_name.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.book_name.setObjectName("book_name")
        self.member_id = QtWidgets.QLineEdit(self.centralwidget)
        self.member_id.setGeometry(QtCore.QRect(350, 180, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.member_id.setFont(font)
        self.member_id.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.member_id.setText("")
        self.member_id.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.member_id.setObjectName("member_id")
        self.returnDebitBtn = QtWidgets.QPushButton(self.centralwidget)
        self.returnDebitBtn.setGeometry(QtCore.QRect(470, 360, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.returnDebitBtn.setFont(font)
        self.returnDebitBtn.setAutoFillBackground(False)
        self.returnDebitBtn.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.returnDebitBtn.setObjectName("returnDebitBtn")
        self.returnDebitBtn.clicked.connect(self.return_debit)
        self.returnDebitBtn.clicked.connect(prikazZaduzeenja.hide)
        prikazZaduzeenja.setCentralWidget(self.centralwidget)

        self.retranslateUi(prikazZaduzeenja)
        QtCore.QMetaObject.connectSlotsByName(prikazZaduzeenja)

    def retranslateUi(self, prikazZaduzenja):
        _translate = QtCore.QCoreApplication.translate
        prikazZaduzenja.setWindowTitle(_translate("prikazZaduzenja", "MainWindow"))
        self.dodajZaduzenje.setText(_translate("prikazZaduzenja", "dodaj zaduzenje"))
        self.pretragaKnjiga.setText(_translate("prikazZaduzenja", "pretraga knjiga"))
        self.allDebitsBtn.setText(_translate("prikazZaduzenja", "pregled zaduzenja"))
        self.allReservationsBtn.setText(_translate("prikazZaduzenja", "pregled rezervacija"))
        self.date.setPlaceholderText(_translate("prikazZaduzenja", "datum uzimanja:"))
        self.label_2.setText(_translate("prikazZaduzenja", "Zaduzenje"))
        self.isbn.setPlaceholderText(_translate("prikazZaduzenja", "isbn:"))
        self.book_name.setPlaceholderText(_translate("prikazZaduzenja", "naziv knjige:"))
        self.member_id.setPlaceholderText(_translate("prikazZaduzenja", "id clana:"))
        self.returnDebitBtn.setText(_translate("prikazZaduzenja", "vrati knjigu"))
        self.date.setText(datetime.strftime(self.debit.get_date(), '%d/%m/%Y'))
        self.member_id.setText(self.debit.get_member())
        self.isbn.setText(self.debit.get_book())
        from src.repositories.BookRepository import BookRepository
        br = BookRepository()
        book = br.get_book_by_isbn(self.debit.get_book())
        if book:
            self.book_name.setText(book.get_title())

