# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pregledRezervacijaBibliotekar.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets

from src.entities.Reservation import ReservationStatus


class Ui_MainWindow(object):
    def __init__(self, logged_user, reservation):
        self.logged_user = logged_user
        self.reservation = reservation

    def goToBookReturn(self):
        from src.View.book_view.makeDebit import DebitView

        self.window2 = QtWidgets.QMainWindow()
        self.ui = DebitView(self.logged_user)
        self.ui.make_debit(self.window2, None)
        self.window2.show()

    def goToBookSearch(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(None, self.logged_user, None)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToReservationSearch(self):
        from src.View.book_view.reservationSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToDebitSearch(self):
        from src.View.book_view.debitSearch import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goToAddDebit(self):
        from src.Controller.book_controller.debitService import DebitControl
        dc = DebitControl()
        msg_box = QtWidgets.QMessageBox()
        ret = msg_box.warning(msg_box, "Upozorenje!", "Da li ste sigurni da zelite da izdate knjigu?", QtWidgets.
                              QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if ret == msg_box.Ok:
            if dc.make_debit_by_reservation(self.reservation , self.logged_user.get_username()):
                self.isbn.setText("")
                self.member_id.setText("")
                self.isbn_2.setText("")
                self.status.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Knjiga je uspesno izdata.")
                self.goBack()
            else:
                self.isbn.setText("")
                self.member_id.setText("")
                self.isbn_2.setText("")
                self.status.setText("")
                msg = QtWidgets.QMessageBox()
                msg.information(msg, "Obavestenje", "Doslo je do greske. Knjiga ne moze biti izdata.")
                self.goBack()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(960, 537)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.kreirajZaduzenja = QtWidgets.QPushButton(self.centralwidget)
        self.kreirajZaduzenja.setGeometry(QtCore.QRect(470, 360, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.kreirajZaduzenja.setFont(font)
        self.kreirajZaduzenja.setAutoFillBackground(False)
        self.kreirajZaduzenja.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.kreirajZaduzenja.setObjectName("kreirajZaduzenja")
        self.kreirajZaduzenja.clicked.connect(self.goToAddDebit)
        self.kreirajZaduzenja.clicked.connect(MainWindow.close)
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(MainWindow.hide)
        self.isbn_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.isbn_2.setGeometry(QtCore.QRect(50, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbn_2.setFont(font)
        self.isbn_2.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.isbn_2.setText("")
        self.isbn_2.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.isbn_2.setObjectName("isbn_2")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(50, 110, 200, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet("color:  #86ACA5; padding:2px;")
        self.label_2.setObjectName("label_2")
        self.isbn = QtWidgets.QLineEdit(self.centralwidget)
        self.isbn.setGeometry(QtCore.QRect(50, 180, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.isbn.setFont(font)
        self.isbn.setStyleSheet("border: none;\n"
                                "border-bottom: 2px solid #E7E3D5;\n"
                                "color: #527770;\n"
                                "\n"
                                "")
        self.isbn.setText("")
        self.isbn.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.isbn.setObjectName("isbn")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 551))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.retrurnDebitBtn_2 = QtWidgets.QPushButton(self.widget_2)
        self.retrurnDebitBtn_2.setGeometry(QtCore.QRect(20, 40, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.retrurnDebitBtn_2.setFont(font)
        self.retrurnDebitBtn_2.setAutoFillBackground(False)
        self.retrurnDebitBtn_2.setStyleSheet("background-color: #E7E3D5;\n"
                                             "color: #527770;\n"
                                             "border-radius: 15px;")
        self.retrurnDebitBtn_2.setObjectName("retrurnDebitBtn_2")

        self.retrurnDebitBtn_2.clicked.connect(self.goToBookReturn)
        self.retrurnDebitBtn_2.clicked.connect(MainWindow.hide)
        self.pretragaKnjiga = QtWidgets.QPushButton(self.widget_2)
        self.pretragaKnjiga.setGeometry(QtCore.QRect(20, 110, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.pretragaKnjiga.setFont(font)
        self.pretragaKnjiga.setAutoFillBackground(False)
        self.pretragaKnjiga.setStyleSheet("background-color: #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "border-radius: 15px;")
        self.pretragaKnjiga.setObjectName("pretragaKnjiga")
        self.pretragaKnjiga.clicked.connect(self.goToBookSearch)
        self.pretragaKnjiga.clicked.connect(MainWindow.hide)
        self.allDebitsBtn_2 = QtWidgets.QPushButton(self.widget_2)
        self.allDebitsBtn_2.setGeometry(QtCore.QRect(20, 180, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allDebitsBtn_2.setFont(font)
        self.allDebitsBtn_2.setAutoFillBackground(False)
        self.allDebitsBtn_2.setStyleSheet("background-color: #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "border-radius: 15px;")
        self.allDebitsBtn_2.setObjectName("allDebitsBtn_2")
        self.allDebitsBtn_2.clicked.connect(self.goToDebitSearch)
        self.allDebitsBtn_2.clicked.connect(MainWindow.hide)
        self.allReservationsBtn_2 = QtWidgets.QPushButton(self.widget_2)
        self.allReservationsBtn_2.setGeometry(QtCore.QRect(20, 250, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.allReservationsBtn_2.setFont(font)
        self.allReservationsBtn_2.setAutoFillBackground(False)
        self.allReservationsBtn_2.setStyleSheet("background-color: #E7E3D5;\n"
                                                "color: #527770;\n"
                                                "border-radius: 15px;")
        self.allReservationsBtn_2.setObjectName("allReservationsBtn_2")
        self.allReservationsBtn_2.clicked.connect(self.goToReservationSearch)
        self.allReservationsBtn_2.clicked.connect(MainWindow.hide)
        self.status = QtWidgets.QLineEdit(self.centralwidget)
        self.status.setGeometry(QtCore.QRect(350, 250, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.status.setFont(font)
        self.status.setStyleSheet("border: none;\n"
                                  "border-bottom: 2px solid #E7E3D5;\n"
                                  "color: #527770;\n"
                                  "\n"
                                  "")
        self.status.setText("")
        self.status.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.status.setObjectName("status")
        self.member_id = QtWidgets.QLineEdit(self.centralwidget)
        self.member_id.setGeometry(QtCore.QRect(350, 180, 281, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.member_id.setFont(font)
        self.member_id.setStyleSheet("border: none;\n"
                                     "border-bottom: 2px solid #E7E3D5;\n"
                                     "color: #527770;\n"
                                     "\n"
                                     "")
        self.member_id.setText("")
        self.member_id.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.member_id.setObjectName("member_id")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.kreirajZaduzenja.setText(_translate("MainWindow", "kreiraj zaduzenje"))
        self.isbn_2.setPlaceholderText(_translate("MainWindow", "naziv knjige:"))
        self.label_2.setText(_translate("MainWindow", "Rezervacija"))
        self.isbn.setPlaceholderText(_translate("MainWindow", "isbn:"))
        self.retrurnDebitBtn_2.setText(_translate("MainWindow", "vracanje knjige"))
        self.pretragaKnjiga.setText(_translate("MainWindow", "pretraga knjiga"))
        self.allDebitsBtn_2.setText(_translate("MainWindow", "pregled zaduzenja"))
        self.allReservationsBtn_2.setText(_translate("MainWindow", "pregled rezervacija"))
        self.status.setPlaceholderText(_translate("MainWindow", "status:"))
        self.member_id.setPlaceholderText(_translate("MainWindow", "id clana:"))
        self.member_id.setText(self.reservation.get_member())
        self.isbn.setText(self.reservation.get_book())
        if self.reservation.get_reservation_status() == ReservationStatus.active:
            self.status.setText('aktivan')
        elif self.reservation.get_reservation_status() == ReservationStatus.expired:
            self.status.setText('istekla')
        elif self.reservation.get_reservation_status() == ReservationStatus.finished:
            self.status.setText('neaktivna')

        from src.repositories.BookRepository import BookRepository
        br = BookRepository()
        book = br.get_book_by_isbn(self.reservation.get_book())
        if book:
            self.isbn_2.setText(book.get_title())
