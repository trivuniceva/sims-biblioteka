from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QScrollBar, QListWidgetItem


class Ui_MainWindow(object):

    def __init__(self, display, logged_user, book=None):
        self.display = display
        self.reservation = book
        self.logged_user = logged_user

    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(logged_user=self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        MainWindow.setStyleSheet("background-color:#ffffff;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(MainWindow.hide)

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 101))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(40, 20, 461, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(95, 181, 531, 251))
        self.listWidget.setObjectName("listWidget")
        self.scroll_bar = QScrollBar()
        if self.display:
            for d in list(self.display):
                self.item = QListWidgetItem("isbn: " + d.get_book() + " member:" +
                                            d.get_member())
                self.listWidget.addItem(self.item)

        self.listWidget.setStyleSheet('background-color:#e7e3d5;'
                                      'color:#86ACA5;'
                                      'border-radius:8px;'
                                      'font-size:20px;'
                                      'scroll_bar-color:#86ACA5;'
                                      )

        self.scroll_bar.setStyleSheet('''
                                    QScrollBar:vertical {
                                        border: none;
                                        background: #ddd7c4;
                                        width: 20px;
                                        margin: 15px 0 15px 0;
                                        border-radius: 0px;
                                     }

                                    /*  HANDLE BAR VERTICAL */
                                    QScrollBar::handle:vertical {	
                                        background-color: #86ACA5;
                                        min-height: 76px;
                                        border-radius: 0px;
                                    }
                                    QScrollBar::handle:vertical:hover{	
                                        background-color: #577F77;
                                    }
                                    QScrollBar::handle:vertical:pressed {	
                                        background-color: #577F77;
                                    }

                                    /* BTN TOP - SCROLLBAR */
                                    QScrollBar::sub-line:vertical {
                                        border: none;
                                        background-color: #86ACA5;
                                        height: 15px;
                                        border-top-left-radius: 7px;
                                        border-top-right-radius: 7px;
                                        subcontrol-position: top;
                                        subcontrol-origin: margin;
                                    }
                                    QScrollBar::sub-line:vertical:hover {	
                                        background-color: #577F77;
                                    }
                                    QScrollBar::sub-line:vertical:pressed {	
                                        background-color: #577F77;
                                    }

                                    /* BTN BOTTOM - SCROLLBAR */
                                    QScrollBar::add-line:vertical {
                                        border: none;
                                        background-color: #86ACA5;
                                        height: 15px;
                                        border-bottom-left-radius: 7px;
                                        border-bottom-right-radius: 7px;
                                        subcontrol-position: bottom;
                                        subcontrol-origin: margin;
                                    }
                                    QScrollBar::add-line:vertical:hover {	
                                        background-color: #577F77;
                                    }
                                    QScrollBar::add-line:vertical:pressed {	
                                        background-color: #577F77;
                                    }

                                    /* RESET ARROW */
                                    QScrollBar::up-arrow:vertical, QScrollBar::down-arrow:vertical {
                                        background: none;
                                    }
                                    QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {
                                        background: none;
                                    }
                ''')

        self.listWidget.setVerticalScrollBar(self.scroll_bar)

        self.showResultsButton = QtWidgets.QPushButton(self.centralwidget)
        self.showResultsButton.setGeometry(QtCore.QRect(270, 460, 161, 51))
        self.showResultsButton.setAutoFillBackground(False)
        self.showResultsButton.setStyleSheet("background-color: #86ACA5;\n"
                                             "color: #FFFFFF;\n"
                                             "border-radius: 15px;")
        self.showResultsButton.setObjectName("showResultsButton")

        self.showResultsButton.clicked.connect(self.find_book)
        self.showResultsButton.clicked.connect(MainWindow.hide)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def find_book(self):
        index = self.listWidget.row(self.listWidget.currentItem())
        self.reservation = list(self.display)[index]

        from src.View.book_view.reservationDetails import Ui_MainWindow
        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user, self.reservation)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "REZULTATI PRETRAGE"))
        self.showResultsButton.setText(_translate("MainWindow", "prikazi"))
