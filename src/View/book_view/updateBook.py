from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):

    def __init__(self, book):
        self.book = book

    def show_message(self):
        from src.View.book_view.SuccessUpdatedBook import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def update_displayed_book(self):
        from src.Controller.book_controller.BookController import update_book

        update_book(self.titleUpdate.text(),
                    self.authorUpdate.text(),
                    self.categoryUpdate.text(),
                    self.genreUpdate.text(),
                    self.numOfPagesUpdate.text(),
                    self.yearUpdate.text(),
                    self.numOfItemsUpdate.text(),
                    self.comboBox.currentText(),
                    self.book)

        self.show_message()

    def goToAddBook(self):
        from src.View.book_view.addBook import UiMainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = UiMainWindow()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def goToDeleteBook(self):
        from src.View.book_view.searchBooks import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow("delete")
        self.ui.setupUi(self.window1)
        self.window1.show()

    def goBackToBookMeni(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBackToBookMeni)
        self.goBackButton.clicked.connect(MainWindow.close)

        self.isbndisplay = QtWidgets.QLabel(self.centralwidget)
        self.isbndisplay.setGeometry(QtCore.QRect(260, 60, 211, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.isbndisplay.setFont(font)
        self.isbndisplay.setStyleSheet("color:  #527777; padding:2px;")
        self.isbndisplay.setAlignment(QtCore.Qt.AlignCenter)
        self.isbndisplay.setObjectName("isbndisplay")
        self.isbndisplay.setText(self.book.get_isbn())
        self.titleUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.titleUpdate.setGeometry(QtCore.QRect(90, 130, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.titleUpdate.setFont(font)
        self.titleUpdate.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.titleUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.titleUpdate.setObjectName("titleUpdate")
        self.titleUpdate.setText(self.book.get_title())
        self.authorUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.authorUpdate.setGeometry(QtCore.QRect(90, 200, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.authorUpdate.setFont(font)
        self.authorUpdate.setStyleSheet("border: none;\n"
                                        "border-bottom: 2px solid #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "\n"
                                        "")
        self.authorUpdate.setInputMethodHints(QtCore.Qt.ImhUppercaseOnly)
        self.authorUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.authorUpdate.setObjectName("authorUpdate")
        self.authorUpdate.setText(self.book.get_author())
        self.categoryUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.categoryUpdate.setGeometry(QtCore.QRect(90, 270, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.categoryUpdate.setFont(font)
        self.categoryUpdate.setStyleSheet("border: none;\n"
                                          "border-bottom: 2px solid #E7E3D5;\n"
                                          "color: #527770;\n"
                                          "\n"
                                          "")
        self.categoryUpdate.setInputMethodHints(QtCore.Qt.ImhLowercaseOnly)
        self.categoryUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.categoryUpdate.setObjectName("categoryUpdate")
        self.categoryUpdate.setText(self.book.get_category())
        self.genreUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.genreUpdate.setGeometry(QtCore.QRect(90, 340, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.genreUpdate.setFont(font)
        self.genreUpdate.setStyleSheet("border: none;\n"
                                       "border-bottom: 2px solid #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "\n"
                                       "")
        self.genreUpdate.setInputMethodHints(QtCore.Qt.ImhLowercaseOnly)
        self.genreUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.genreUpdate.setObjectName("genreUpdate")
        self.genreUpdate.setText(self.book.get_genre())
        self.numOfPagesUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfPagesUpdate.setGeometry(QtCore.QRect(380, 130, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.numOfPagesUpdate.setFont(font)
        self.numOfPagesUpdate.setStyleSheet("border: none;\n"
                                            "border-bottom: 2px solid #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "\n"
                                            "")
        self.numOfPagesUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfPagesUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfPagesUpdate.setObjectName("numOfPagesUpdate")
        self.numOfPagesUpdate.setText(self.book.get_num_of_pages())
        self.yearUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.yearUpdate.setGeometry(QtCore.QRect(380, 200, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.yearUpdate.setFont(font)
        self.yearUpdate.setStyleSheet("border: none;\n"
                                      "border-bottom: 2px solid #E7E3D5;\n"
                                      "color: #527770;\n"
                                      "\n"
                                      "")
        self.yearUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.yearUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.yearUpdate.setObjectName("yearUpdate")
        self.yearUpdate.setText(self.book.get_year_of_publication())
        self.numOfItemsUpdate = QtWidgets.QLineEdit(self.centralwidget)
        self.numOfItemsUpdate.setGeometry(QtCore.QRect(380, 270, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.numOfItemsUpdate.setFont(font)
        self.numOfItemsUpdate.setStyleSheet("border: none;\n"
                                            "border-bottom: 2px solid #E7E3D5;\n"
                                            "color: #527770;\n"
                                            "\n"
                                            "")
        self.numOfItemsUpdate.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        self.numOfItemsUpdate.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.numOfItemsUpdate.setObjectName("numOfItemsUpdate")

        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setGeometry(QtCore.QRect(380, 340, 241, 41))

        self.numOfItemsUpdate.setText(self.book.get_num_of_available_books())
        self.deleteButton = QtWidgets.QPushButton(self.centralwidget)
        self.deleteButton.setGeometry(QtCore.QRect(270, 440, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.deleteButton.setFont(font)
        self.deleteButton.setAutoFillBackground(False)
        self.deleteButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.deleteButton.setObjectName("deleteButton")

        self.deleteButton.clicked.connect(self.update_displayed_book)
        self.deleteButton.clicked.connect(MainWindow.hide)

        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.createOption = QtWidgets.QPushButton(self.sideMeni)
        self.createOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.createOption.setAutoFillBackground(False)
        self.createOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.createOption.setObjectName("createOption")

        self.createOption.clicked.connect(self.goToAddBook)
        self.createOption.clicked.connect(MainWindow.hide)

        self.updateOption = QtWidgets.QPushButton(self.sideMeni)
        self.updateOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.updateOption.setAutoFillBackground(False)
        self.updateOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.updateOption.setObjectName("updateOption")

        self.deleteOption = QtWidgets.QPushButton(self.sideMeni)
        self.deleteOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.deleteOption.setAutoFillBackground(False)
        self.deleteOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.deleteOption.setObjectName("deleteOption")

        self.deleteOption.clicked.connect(self.goToDeleteBook)
        self.deleteOption.clicked.connect(MainWindow.hide)

        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(99, 119, 113))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.PlaceholderText, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(240, 240, 240))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 128))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.PlaceholderText, brush)
        self.comboBox.setPalette(palette)
        self.comboBox.setAutoFillBackground(True)
        self.comboBox.setEditable(True)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.setCurrentText(self.book.get_book_status())
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 40, 581, 371))
        self.label.setStyleSheet("background-color: #86ACA5;\n"
                                 "border-radius:20px;")
        self.label.setText("")
        self.label.setObjectName("label")
        self.label.raise_()
        self.goBackButton.raise_()
        self.isbndisplay.raise_()
        self.titleUpdate.raise_()
        self.authorUpdate.raise_()
        self.categoryUpdate.raise_()
        self.genreUpdate.raise_()
        self.numOfPagesUpdate.raise_()
        self.yearUpdate.raise_()
        self.numOfItemsUpdate.raise_()
        self.deleteButton.raise_()
        self.sideMeni.raise_()
        self.comboBox.raise_()
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.isbndisplay.setText(_translate("MainWindow", ""))
        # self.titleUpdate.setText(_translate("MainWindow", ""))
        self.titleUpdate.setPlaceholderText(_translate("MainWindow", "naslov:"))
        # self.authorUpdate.setText(_translate("MainWindow", ""))
        self.authorUpdate.setPlaceholderText(_translate("MainWindow", "autor:"))
        # self.categoryUpdate.setText(_translate("MainWindow", ""))
        self.categoryUpdate.setPlaceholderText(_translate("MainWindow", "kategorija:"))
        # self.genreUpdate.setText(_translate("MainWindow", ""))
        self.genreUpdate.setPlaceholderText(_translate("MainWindow", "zanr:"))
        # self.numOfPagesUpdate.setText(_translate("MainWindow", ""))
        self.numOfPagesUpdate.setPlaceholderText(_translate("MainWindow", "broj stranica:"))
        # self.yearUpdate.setText(_translate("MainWindow", ""))
        self.yearUpdate.setPlaceholderText(_translate("MainWindow", "godina izdavanja:"))
        # self.numOfItemsUpdate.setText(_translate("MainWindow", ""))
        self.numOfItemsUpdate.setPlaceholderText(_translate("MainWindow", "broj dostupnih komada:"))
        self.deleteButton.setText(_translate("MainWindow", "izmeni"))
        self.createOption.setText(_translate("MainWindow", "kreiraj"))
        self.updateOption.setText(_translate("MainWindow", "izmeni"))
        self.deleteOption.setText(_translate("MainWindow", "obrisi"))
        # self.comboBox.setCurrentText(_translate("MainWindow", ""))
        self.comboBox.setPlaceholderText(_translate("MainWindow", "broj dana trajanja zaduzenja:"))
        self.comboBox.setItemText(0, _translate("MainWindow", "28"))
        self.comboBox.setItemText(1, _translate("MainWindow", "14"))
        self.comboBox.setItemText(2, _translate("MainWindow", "0"))
