from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):

    def __init__(self, book, logged_user=None):
        self.book = book
        self.logged_user = logged_user

    def goToMakeDebit(self):
        from src.View.book_view.makeDebit import DebitView

        self.window2 = QtWidgets.QMainWindow()
        self.ui = DebitView(self.logged_user)
        self.ui.make_debit(self.window2, self.book.get_isbn())
        self.window2.show()

    def goToBookReturn(self):
        from src.View.book_view.returnDebit import Ui_MainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(self.logged_user, self.book.get_isbn())
        self.ui.setupUi(self.window2)
        self.window2.show()

    def goBack(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(self.logged_user)
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.kreirajOption = QtWidgets.QPushButton(self.widget_2)
        self.kreirajOption.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.kreirajOption.setAutoFillBackground(False)
        self.kreirajOption.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")

        self.kreirajOption.clicked.connect(self.goToMakeDebit)
        self.kreirajOption.clicked.connect(MainWindow.close)

        self.vratiOption = QtWidgets.QPushButton(self.widget_2)
        self.vratiOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.vratiOption.setAutoFillBackground(False)
        self.vratiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                       "color: #527770;\n"
                                       "border-radius: 20px;")
        self.vratiOption.clicked.connect(self.goToBookReturn)
        self.vratiOption.clicked.connect(MainWindow.close)

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(100, 70, 121, 131))
        self.label.setAutoFillBackground(True)
        self.label.setStyleSheet("border-image:url(:/newPrefix/icons8_customer_50px_1.png)")
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("icons8_literature_100px.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setStyleSheet("border-image:url(:/newPrefix/back_50px.png);\nbackground-color: #ffffff;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.goBack)
        self.goBackButton.clicked.connect(MainWindow.hide)

        self.textBrowser = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser.setGeometry(QtCore.QRect(40, 260, 261, 101))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.textBrowser.setFont(font)
        self.textBrowser.setAutoFillBackground(True)
        self.textBrowser.setStyleSheet("border: none;\n"
                                       " color: #86ACA5;")
        self.textBrowser.setObjectName("textBrowser")
        self.lineEdit_7 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_7.setGeometry(QtCore.QRect(350, 80, 331, 31))
        self.lineEdit_7.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.lineEdit_7.setObjectName("lineEdit_7")
        self.lineEdit_7.setText(self.book.get_title())
        self.lineEdit_10 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_10.setGeometry(QtCore.QRect(350, 200, 331, 31))
        self.lineEdit_10.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_10.setObjectName("lineEdit_10")
        self.lineEdit_10.setText(self.book.get_genre())
        self.lineEdit_11 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_11.setGeometry(QtCore.QRect(350, 240, 331, 31))
        self.lineEdit_11.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_11.setObjectName("lineEdit_11")
        self.lineEdit_11.setText("broj stranica: " + self.book.get_num_of_pages())
        self.lineEdit_13 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_13.setGeometry(QtCore.QRect(350, 320, 331, 31))
        self.lineEdit_13.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_13.setObjectName("lineEdit_13")
        self.lineEdit_13.setText("broj dostupnih komada: " + self.book.get_num_of_available_books())
        self.lineEdit_14 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_14.setGeometry(QtCore.QRect(420, 530, 331, 31))
        self.lineEdit_14.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_14.setObjectName("lineEdit_14")

        if self.book.get_num_of_available_books() == '0':
            self.lineEdit_14.setText("nedostupno")
        if self.book.get_num_of_available_books() != '0':
            self.lineEdit_14.setText("dostupno")

        self.lineEdit_12 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_12.setGeometry(QtCore.QRect(350, 280, 331, 31))
        self.lineEdit_12.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_12.setObjectName("lineEdit_12")

        self.lineEdit_12.setText("godina izdavanja: " + self.book.get_year_of_publication())

        self.lineEdit_6 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_6.setGeometry(QtCore.QRect(350, 40, 331, 31))
        self.lineEdit_6.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.lineEdit_6.setObjectName("lineEdit_6")
        self.lineEdit_6.setText(self.book.get_isbn())
        self.lineEdit_8 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_8.setGeometry(QtCore.QRect(350, 120, 331, 31))
        self.lineEdit_8.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.lineEdit_8.setObjectName("lineEdit_8")
        self.lineEdit_8.setText(self.book.get_author())
        self.lineEdit_9 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_9.setGeometry(QtCore.QRect(350, 160, 331, 31))
        self.lineEdit_9.setStyleSheet("background-color: #ffffff;\n"
                                      "borde-radius: 2px;\n"
                                      "color: #527770;\n"
                                      "padding-left:8px;")
        self.lineEdit_9.setObjectName("lineEdit_9")
        self.lineEdit_9.setText(self.book.get_category())
        self.lineEdit_15 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_15.setGeometry(QtCore.QRect(350, 360, 331, 31))
        self.lineEdit_15.setStyleSheet("background-color: #ffffff;\n"
                                       "borde-radius: 2px;\n"
                                       "color: #527770;\n"
                                       "padding-left:8px;")
        self.lineEdit_15.setObjectName("lineEdit_15")
        self.lineEdit_15.setText("broj dana zaduzenja: " + self.book.get_book_status())
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.kreirajOption.setText(_translate("MainWindow", "kreiraj zaduzenje"))
        self.vratiOption.setText(_translate("MainWindow", "razduzi"))
        self.textBrowser.setHtml(_translate("MainWindow",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:14pt; font-weight:400; font-style:normal;\">\n"
                                            "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:28pt;\">Naslov</span></p>\n"
                                            "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:28pt;\">Autor</span></p></body></html>"))
        self.textBrowser.setText(self.book.get_title() + " " + self.book.get_author())
        self.textBrowser.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_7.setPlaceholderText(_translate("MainWindow", "naslov"))
        self.lineEdit_7.setReadOnly(True)
        self.lineEdit_10.setPlaceholderText(_translate("MainWindow", "zanr"))
        self.lineEdit_10.setReadOnly(True)
        self.lineEdit_11.setPlaceholderText(_translate("MainWindow", "broj stanica"))
        self.lineEdit_11.setReadOnly(True)
        self.lineEdit_13.setPlaceholderText(_translate("MainWindow", "broj dostupnih komada"))
        self.lineEdit_13.setReadOnly(True)
        self.lineEdit_14.setPlaceholderText(_translate("MainWindow", "dostupnost"))
        self.lineEdit_14.setReadOnly(True)
        self.lineEdit_12.setPlaceholderText(_translate("MainWindow", "godina"))
        self.lineEdit_12.setReadOnly(True)
        self.lineEdit_6.setPlaceholderText(_translate("MainWindow", "isbn"))
        self.lineEdit_6.setReadOnly(True)
        self.lineEdit_8.setPlaceholderText(_translate("MainWindow", "autor"))
        self.lineEdit_8.setReadOnly(True)
        self.lineEdit_9.setPlaceholderText(_translate("MainWindow", "kategorija"))
        self.lineEdit_9.setReadOnly(True)
        self.lineEdit_15.setPlaceholderText(_translate("MainWindow", "dostupnost"))
        self.lineEdit_15.setReadOnly(True)

