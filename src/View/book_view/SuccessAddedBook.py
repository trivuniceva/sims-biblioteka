from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_SuccessfullyAddedBookMessage(object):

    def got_to_add_book_wind(self):
        from src.View.book_view.addBook import UiMainWindow

        self.window2 = QtWidgets.QMainWindow()
        self.ui = UiMainWindow()
        self.ui.setupUi(self.window2)
        self.window2.show()

    def go_to_meni(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window2 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window2)
        self.window2.show()

    def setupUi(self, MainWind):
        MainWind.setObjectName("SuccessfullyAddedBookMessage")
        MainWind.setFixedSize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(82, 119, 112))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWind.setPalette(palette)
        MainWind.setWindowTitle("")
        MainWind.setAutoFillBackground(True)
        MainWind.setDocumentMode(True)
        MainWind.setTabShape(QtWidgets.QTabWidget.Rounded)
        MainWind.setUnifiedTitleAndToolBarOnMac(True)
        self.centralwidget = QtWidgets.QWidget(MainWind)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(350, 170, 301, 41))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label.setFont(font)
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(450, 240, 47, 13))
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(350, 200, 291, 41))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_3.setFont(font)
        self.label_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.Meni = QtWidgets.QPushButton(self.centralwidget)
        self.Meni.setGeometry(QtCore.QRect(410, 290, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.Meni.setFont(font)
        self.Meni.setAutoFillBackground(False)
        self.Meni.setStyleSheet("background-color: #86ACA5;\n"
                                "color: #FFFFFF;\n"
                                "border-radius: 15px;")
        self.Meni.setObjectName("Meni")

        self.Meni.clicked.connect(self.go_to_meni)
        self.Meni.clicked.connect(MainWind.hide)

        self.AddNew = QtWidgets.QPushButton(self.centralwidget)
        self.AddNew.setGeometry(QtCore.QRect(410, 340, 181, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.AddNew.setFont(font)
        self.AddNew.setAutoFillBackground(False)
        self.AddNew.setStyleSheet("background-color: #86ACA5;\n"
                                  "color: #FFFFFF;\n"
                                  "border-radius: 15px;")
        self.AddNew.setObjectName("AddNew")

        self.AddNew.clicked.connect(self.got_to_add_book_wind)
        self.AddNew.clicked.connect(MainWind.hide)

        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(650, 0, 351, 551))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.sideMeni_2 = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni_2.setGeometry(QtCore.QRect(0, 0, 351, 551))
        self.sideMeni_2.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni_2.setObjectName("sideMeni_2")
        self.label_2.raise_()
        self.Meni.raise_()
        self.label_3.raise_()
        self.label.raise_()
        self.AddNew.raise_()
        self.sideMeni.raise_()
        self.sideMeni_2.raise_()

        MainWind.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWind)
        QtCore.QMetaObject.connectSlotsByName(MainWind)

    def retranslateUi(self, SuccessfullyAddedBookMessage):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("SuccessfullyAddedBookMessage", "Uspesno ste dodali "))
        self.label_3.setText(_translate("SuccessfullyAddedBookMessage", "novu knjigu!"))
        self.Meni.setText(_translate("SuccessfullyAddedBookMessage", "Meni"))
        self.AddNew.setText(_translate("SuccessfullyAddedBookMessage", "Dodaj novu knjigu"))
