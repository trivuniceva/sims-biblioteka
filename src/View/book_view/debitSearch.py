from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def __init__(self, logged_user):
        self.logged_user = logged_user

    def go_back(self):
        from src.View.users_view.librarian_view.bibliotekarMain import Ui_bibliotekarMain

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_bibliotekarMain(logged_user=self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def showResults(self, display):
        from src.View.book_view.debitSearchResults import Ui_MainWindow

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow(display, self.logged_user)
        self.ui.setupUi(self.window1)
        self.window1.show()

    def search_debits(self):
        from src.Controller.book_controller.debitService import DebitModel

        db = DebitModel()

        isbn = self.isbnInput.text()
        member_id = self.member_id.text()
        result = db.search(isbn, member_id)

        self.showResults(result)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(960, 537)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        MainWindow.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.sideMeni = QtWidgets.QWidget(self.centralwidget)
        self.sideMeni.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.sideMeni.setStyleSheet("background-color: #86ACA5;")
        self.sideMeni.setObjectName("sideMeni")
        self.restartButton = QtWidgets.QPushButton(self.sideMeni)
        self.restartButton.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.restartButton.setAutoFillBackground(False)
        self.restartButton.setStyleSheet("background-color: #E7E3D5;\n"
                                         "color: #527770;\n"
                                         "border-radius: 20px;")
        self.restartButton.setObjectName("restartButton")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(50, 60, 621, 171))
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("background-color: #86ACA5; border-radius: 20px;")
        self.widget.setObjectName("widget")
        self.label = QtWidgets.QLabel(self.widget)
        self.label.setGeometry(QtCore.QRect(20, 20, 461, 61))
        font = QtGui.QFont()
        font.setPointSize(30)
        self.label.setFont(font)
        self.label.setStyleSheet("color:#ffffff;")
        self.label.setObjectName("label")
        self.isbnInput = QtWidgets.QLineEdit(self.centralwidget)
        self.isbnInput.setGeometry(QtCore.QRect(170, 300, 331, 31))
        self.isbnInput.setStyleSheet("background-color: #ffffff;\n"
                                     "borde-radius: 2px;\n"
                                     "color: #527770;\n"
                                     "padding-left:8px;")
        self.isbnInput.setText("")
        self.isbnInput.setObjectName("isbnInput")
        self.member_id = QtWidgets.QLineEdit(self.centralwidget)
        self.member_id.setGeometry(QtCore.QRect(170, 260, 331, 31))
        self.member_id.setStyleSheet("background-color: #ffffff;\n"
                                     "borde-radius: 2px;\n"
                                     "color: #527770;\n"
                                     "padding-left:8px;")
        self.member_id.setText("")
        self.member_id.setObjectName("member_id")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(MainWindow.hide)

        self.showResultsButton = QtWidgets.QPushButton(self.centralwidget)
        self.showResultsButton.setGeometry(QtCore.QRect(250, 440, 161, 51))
        self.showResultsButton.setAutoFillBackground(False)
        self.showResultsButton.setStyleSheet("background-color: #86ACA5;\n"
                                             "color: #FFFFFF;\n"
                                             "border-radius: 15px;")
        self.showResultsButton.setObjectName("showResultsButton")

        self.showResultsButton.clicked.connect(self.search_debits)

        self.showResultsButton.clicked.connect(MainWindow.hide)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.restartButton.setText(_translate("MainWindow", "restartuj filtere"))
        self.label.setText(_translate("MainWindow", "Pretraga zaduzenja"))
        self.isbnInput.setPlaceholderText(_translate("MainWindow", "ISBN (direktna pretraga):"))
        self.member_id.setPlaceholderText(_translate("MainWindow", "id clana:"))
        self.showResultsButton.setText(_translate("MainWindow", "prikazi rezultate"))
