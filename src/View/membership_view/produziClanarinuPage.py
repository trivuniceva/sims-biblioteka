from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QListWidgetItem

from src.Controller.memberships_controller.ProduziClanarinuCRUD import ProduziClanarinuCRUD
from src.entities.MembershipClass import Membership


class Ui_ProduziClanarinu(object):

    mb = Membership()
    mb.make_membership_dict()

    pr = ProduziClanarinuCRUD()

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, ProduziClanarinu):
        ProduziClanarinu.setObjectName("ProduziClanarinu")
        ProduziClanarinu.setFixedSize(960, 537)
        ProduziClanarinu.setStyleSheet("background-color: #ffffff;")
        self.centralwidget = QtWidgets.QWidget(ProduziClanarinu)
        self.centralwidget.setObjectName("centralwidget")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(ProduziClanarinu.hide)

        self.goBackButton.setObjectName("goBackButton")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")

        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 285, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")

        self.obrisiOption.clicked.connect(self.obrisiCallFunc)

        self.resetOprion = QtWidgets.QPushButton(self.widget_2)
        self.resetOprion.setGeometry(QtCore.QRect(60, 405, 131, 89))
        self.resetOprion.setAutoFillBackground(False)
        self.resetOprion.setStyleSheet("background-color: #ffffff;\n"
                                       "color: #527770;\n"
                                       "border-radius: 20px;")
        self.resetOprion.setObjectName("resetOprion")

        self.resetOprion.clicked.connect(self.resetCallFunc)

        self.changeButton = QtWidgets.QPushButton(self.centralwidget)
        self.changeButton.setGeometry(QtCore.QRect(470, 440, 161, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.changeButton.setFont(font)
        self.changeButton.setAutoFillBackground(False)
        self.changeButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 15px;")
        self.changeButton.setText('')
        self.changeButton.setObjectName("changeBox")

        self.datumTrajanjaBox = QtWidgets.QLineEdit(self.centralwidget)
        self.datumTrajanjaBox.setGeometry(QtCore.QRect(440, 240, 241, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.datumTrajanjaBox.setFont(font)
        self.datumTrajanjaBox.setStyleSheet("border: none;\n"
                                            "border-bottom: 2px solid #E7E3D5;\n"
                                            "color: #527770;\n")
        self.datumTrajanjaBox.setAlignment(QtCore.Qt.AlignCenter)
        self.datumTrajanjaBox.setReadOnly(True)
        self.datumTrajanjaBox.setPlaceholderText("")
        self.datumTrajanjaBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.datumTrajanjaBox.setObjectName("datumTrajanjaBox")

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(30, 90, 371, 411))
        self.widget.setStyleSheet("background-color: #86ACA5;\n"
                                  "border-radius: 9px")
        self.widget.setObjectName("widget")
        self.linijaPretrage = QtWidgets.QLineEdit(self.widget)
        self.linijaPretrage.setGeometry(QtCore.QRect(20, 30, 331, 31))
        self.linijaPretrage.setStyleSheet("background-color: #ffffff;\n"
                                          "borde-radius: 2px;\n"
                                          "color: #527770;\n"
                                          "padding-left:8px;")
        self.linijaPretrage.setText("")
        self.linijaPretrage.setObjectName("linijaPretrage")

        self.listWidget = QtWidgets.QListWidget(self.widget)
        self.listWidget.setGeometry(QtCore.QRect(30, 80, 311, 311))
        self.listWidget.setStyleSheet("background-color: #86ACA5;\n"
                                      "color: #527770;\n"
                                      "")
        self.listWidget.setObjectName("listWidget")

        self.searchButton = QtWidgets.QPushButton(self.widget)
        self.searchButton.setGeometry(QtCore.QRect(320, 34, 21, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.searchButton.setFont(font)
        self.searchButton.setAutoFillBackground(False)
        self.searchButton.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 2px;")
        self.searchButton.setText("")
        self.searchButton.setObjectName("searchButton")

        self.searchButton.clicked.connect(self.find_user)

        self.imePrezimeBox = QtWidgets.QLineEdit(self.centralwidget)
        self.imePrezimeBox.setGeometry(QtCore.QRect(440, 160, 241, 51))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.imePrezimeBox.setFont(font)
        self.imePrezimeBox.setStyleSheet("border: none;\n"
                                         "border-bottom: 2px solid #E7E3D5;\n"
                                         "color: #527770;\n")
        self.imePrezimeBox.setAlignment(QtCore.Qt.AlignCenter)
        self.imePrezimeBox.setText("")
        self.imePrezimeBox.setReadOnly(True)
        self.imePrezimeBox.setPlaceholderText("")
        self.imePrezimeBox.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.imePrezimeBox.setObjectName("imePrezimeBox")
        self.ulogaClanarina = QtWidgets.QComboBox(self.centralwidget)
        self.ulogaClanarina.setGeometry(QtCore.QRect(440, 310, 241, 41))
        self.ulogaClanarina.setStyleSheet("color: #577F77;\n"
                                          "selection-color: #E7E3D5;\n"
                                          "selection-background-color: #577F77;\n"
                                          "border-radius: 10px;\n"
                                          "border-bottom: 2px solid #E7E3D5;\n"
                                          "color: #527770;\n")

        self.ulogaClanarina.setObjectName("ulogaClanarina")
        self.ulogaClanarina.addItem("")
        self.ulogaClanarina.addItem("")
        self.ulogaClanarina.addItem("")
        self.ulogaClanarina.addItem("")
        self.ulogaClanarina.addItem("")
        self.ulogaClanarina.addItem("")

        self.trajanjeClanarinaBox = QtWidgets.QComboBox(self.centralwidget)
        self.trajanjeClanarinaBox.setGeometry(QtCore.QRect(440, 370, 241, 41))
        self.trajanjeClanarinaBox.setStyleSheet("color: #577F77;\n"
                                                "selection-color: #E7E3D5;\n"
                                                "selection-background-color: #577F77;\n"
                                                "border-radius: 10px;\n"
                                                "border-bottom: 2px solid #E7E3D5;\n"
                                                "color: #527770;\n"
                                                "")
        self.trajanjeClanarinaBox.setObjectName("trajanjeClanarinaBox")
        self.trajanjeClanarinaBox.addItem("")
        self.trajanjeClanarinaBox.addItem("")
        self.trajanjeClanarinaBox.addItem("")
        self.trajanjeClanarinaBox.addItem("")
        ProduziClanarinu.setCentralWidget(self.centralwidget)

        self.retranslateUi(ProduziClanarinu)
        QtCore.QMetaObject.connectSlotsByName(ProduziClanarinu)

    def retranslateUi(self, ProduziClanarinu):
        _translate = QtCore.QCoreApplication.translate
        ProduziClanarinu.setWindowTitle(_translate("ProduziClanarinu", "MainWindow"))
        self.obrisiOption.setText(_translate("ProduziClanarinu", "obrisi"))
        self.resetOprion.setText(_translate("ProduziClanarinu", "reset"))

        self.linijaPretrage.setPlaceholderText(_translate("ProduziClanarinu", "broj clalnske karte:"))

        self.ulogaClanarina.setItemText(0, _translate("ProduziClanarinu", ""))
        self.ulogaClanarina.setItemText(1, _translate("ProduziClanarinu", "pocasni gost"))
        self.ulogaClanarina.setItemText(2, _translate("ProduziClanarinu", "penzioneri"))
        self.ulogaClanarina.setItemText(3, _translate("ProduziClanarinu", "odrasli"))
        self.ulogaClanarina.setItemText(4, _translate("ProduziClanarinu", "studenti"))
        self.ulogaClanarina.setItemText(5, _translate("ProduziClanarinu", "deca"))

        self.trajanjeClanarinaBox.setItemText(0, _translate("ProduziClanarinu", ""))
        self.trajanjeClanarinaBox.setItemText(1, _translate("ProduziClanarinu", "mesecna"))
        self.trajanjeClanarinaBox.setItemText(2, _translate("ProduziClanarinu", "polugodisnja"))
        self.trajanjeClanarinaBox.setItemText(3, _translate("ProduziClanarinu", "godisnja"))

    def find_user(self):
        from src.Controller.memberships_controller.membership_check import check_if_id_membership_used
        from src.Controller.users_controller.users_check import search_by_id

        entered_id_membership = self.linijaPretrage.text()
        print(self.mb.memberships_dict)

        if check_if_id_membership_used(entered_id_membership):
            self.display_user_obj = search_by_id(entered_id_membership)
            self.show_member()

    def show_member(self):
        name = self.display_user_obj.get_name()
        lastname = self.display_user_obj.get_lastname()
        self.item = QListWidgetItem(name + ' ' + lastname)
        self.listWidget.addItem(self.item)

        self.listWidget.setStyleSheet('background-color:#86ACA5;'
                                      'color:#e7e3d5;'
                                      'font-size:32px;')

        self.listWidget.clicked.connect(self.fill_space)

    def fill_space(self):
        self.imePrezimeBox.setText(self.item.text())

        self.id_membership = self.display_user_obj.get_id_membership()
        self.user_membership_info = self.mb.memberships_dict[self.id_membership]
        date = self.user_membership_info['membership_expiration_date']

        print('date: ', date)
        print(self.user_membership_info)

        if date == '':
            self.ulogaClanarina.setItemText(0, self.user_membership_info['membership_type'])
            self.changeButton.setText('kreiraj')
            self.changeButton.clicked.connect(self.kreirajCallFunc)

        else:
            self.datumTrajanjaBox.setText(date)
            self.ulogaClanarina.setItemText(0, self.user_membership_info['membership_type'])
            self.changeButton.setText('produzi')
            self.changeButton.clicked.connect(self.produziCallFunc)

    def kreirajCallFunc(self):
        print('<3')

        membership_type = self.ulogaClanarina.currentText()
        print(membership_type)
        vreme_trajanja_clanarina = self.trajanjeClanarinaBox.currentText()
        print(vreme_trajanja_clanarina)
        self.pr.create_membership(self.id_membership, membership_type, vreme_trajanja_clanarina)

        date = self.mb.memberships_dict[self.id_membership]['membership_expiration_date']
        print("date: ", date)
        self.datumTrajanjaBox.setText(date)

    def produziCallFunc(self):
        membership_type = self.ulogaClanarina.currentText()
        vreme_trajanja_clanarina = self.trajanjeClanarinaBox.currentText()
        old_date = self.datumTrajanjaBox.text()
        self.pr.update_membership(self.id_membership, membership_type, vreme_trajanja_clanarina, old_date)

        date = self.mb.memberships_dict[self.id_membership]['membership_expiration_date']
        self.datumTrajanjaBox.setText(date)

    def obrisiCallFunc(self):
        self.pr.delete_membership(self.id_membership)
        # kad se ovo odkomentarise nece da udje u kreirajCallFunc nzm zasto
        # self.datumTrajanjaBox.setText(' ')
        self.ulogaClanarina.setItemText(0, '')
        self.trajanjeClanarinaBox.setItemText(0, '')
        self.changeButton.setText('kreiraj')

        print('<3')
        self.changeButton.clicked.connect(self.kreirajCallFunc)

    def resetCallFunc(self):
        self.listWidget.clear()
        self.linijaPretrage.clear()
        self.imePrezimeBox.clear()
        self.datumTrajanjaBox.clear()
        self.changeButton.setText('')

        self.ulogaClanarina.setItemText(0, '')
        self.trajanjeClanarinaBox.setItemText(0, '')
