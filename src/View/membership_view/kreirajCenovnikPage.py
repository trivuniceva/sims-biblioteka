from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_KreirajCenovnik(object):

    def go_back(self):
        from src.View.users_view.admin_view.AdminMainPage import Ui_AdministratorMainPage

        self.window1 = QtWidgets.QMainWindow()
        self.ui = Ui_AdministratorMainPage()
        self.ui.setupUi(self.window1)
        self.window1.show()

    def setupUi(self, KreirajCenovnik):
        KreirajCenovnik.setObjectName("KreirajCenovnik")
        KreirajCenovnik.setFixedSize(960, 537)

        self.centralwidget = QtWidgets.QWidget(KreirajCenovnik)
        self.centralwidget.setObjectName("centralwidget")
        self.widget_2 = QtWidgets.QWidget(self.centralwidget)
        self.widget_2.setGeometry(QtCore.QRect(720, 0, 241, 541))
        self.widget_2.setStyleSheet("background-color: #86ACA5;")
        self.widget_2.setObjectName("widget_2")
        self.prikazi_cene = QtWidgets.QPushButton(self.widget_2)
        self.prikazi_cene.setGeometry(QtCore.QRect(60, 60, 131, 89))
        self.prikazi_cene.setAutoFillBackground(False)
        self.prikazi_cene.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.prikazi_cene.setObjectName("kreirajOption")

        self.prikazi_cene.clicked.connect(self.show_price)

        self.izmeniOption = QtWidgets.QPushButton(self.widget_2)
        self.izmeniOption.setGeometry(QtCore.QRect(60, 210, 131, 89))
        self.izmeniOption.setAutoFillBackground(False)
        self.izmeniOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.izmeniOption.setObjectName("izmeniOption")

        self.izmeniOption.clicked.connect(self.izmeniCallFunc)

        self.obrisiOption = QtWidgets.QPushButton(self.widget_2)
        self.obrisiOption.setGeometry(QtCore.QRect(60, 360, 131, 89))
        self.obrisiOption.setAutoFillBackground(False)
        self.obrisiOption.setStyleSheet("background-color: #E7E3D5;\n"
                                        "color: #527770;\n"
                                        "border-radius: 20px;")
        self.obrisiOption.setObjectName("obrisiOption")

        self.obrisiOption.clicked.connect(self.obrisiCallFunc)

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(268, 107, 187, 347))
        self.label.setStyleSheet("background-color: #86ACA5;\n"
                                 "border-radius: 15px;")
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(140, 130, 161, 300))
        self.label_2.setStyleSheet("background-color: #E7E3D5;\n"
                                   "border-radius: 15px;")
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(20, 150, 178, 268))
        self.label_4.setStyleSheet("background-color: #86ACA5;\n"
                                   "border-radius: 15px;")
        self.label_4.setText("")
        self.label_4.setObjectName("label_4")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(400, 130, 181, 300))
        self.label_3.setStyleSheet("background-color: #E7E3D5;\n"
                                   "border-radius: 15px;")
        self.label_3.setText("")
        self.label_3.setObjectName("label_3")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(520, 150, 178, 268))
        self.label_5.setStyleSheet("background-color: #86ACA5;\n"
                                   "border-radius: 15px;")
        self.label_5.setText("")
        self.label_5.setObjectName("label_5")
        self.goBackButton = QtWidgets.QPushButton(self.centralwidget)
        self.goBackButton.setGeometry(QtCore.QRect(10, 10, 24, 24))
        self.goBackButton.setText("<")
        self.goBackButton.setObjectName("goBackButton")

        self.goBackButton.clicked.connect(self.go_back)
        self.goBackButton.clicked.connect(KreirajCenovnik.hide)

        self.pocasni_cena = QtWidgets.QLineEdit(self.centralwidget)
        self.pocasni_cena.setGeometry(QtCore.QRect(288, 360, 151, 31))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.pocasni_cena.setFont(font)
        self.pocasni_cena.setStyleSheet("background-color:#E7E3D5;\n"
                                        "color: #577F77;\n"
                                        "border-radius: 10px;\n"
                                        "padding: 4px;\n"
                                        "")
        self.pocasni_cena.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.pocasni_cena.setObjectName("pocasni_cena")
        self.odrasli_cena = QtWidgets.QLineEdit(self.centralwidget)
        self.odrasli_cena.setGeometry(QtCore.QRect(160, 370, 101, 29))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.odrasli_cena.setFont(font)
        self.odrasli_cena.setStyleSheet("background-color:#86ACA5;\n"
                                        "color: #E7E3D5;\n"
                                        "border-radius: 8px;\n"
                                        "padding: 4px;\n"
                                        "")
        self.odrasli_cena.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.odrasli_cena.setObjectName("odrasli_cena")
        self.studenti_cena = QtWidgets.QLineEdit(self.centralwidget)
        self.studenti_cena.setGeometry(QtCore.QRect(40, 360, 84, 28))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.studenti_cena.setFont(font)
        self.studenti_cena.setStyleSheet("background-color:#E7E3D5;\n"
                                         "color: #577F77;\n"
                                         "border-radius: 7px;\n"
                                         "padding: 4px;\n"
                                         "")
        self.studenti_cena.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.studenti_cena.setObjectName("studenti_cena")
        self.comboBox_pocasni = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_pocasni.setGeometry(QtCore.QRect(288, 400, 151, 31))
        self.comboBox_pocasni.setStyleSheet("color: #577F77;\n"
                                            "background-color: #E7E3D5;\n"
                                            "selection-color: #E7E3D5;\n"
                                            "selection-background-color: #577F77;\n"
                                            "border-radius: 10px;\n"
                                            "")
        self.comboBox_pocasni.setObjectName("comboBox_pocasni")
        self.comboBox_pocasni.addItem("")
        self.comboBox_pocasni.addItem("")
        self.comboBox_pocasni.addItem("")
        self.deca_cena = QtWidgets.QLineEdit(self.centralwidget)
        self.deca_cena.setGeometry(QtCore.QRect(600, 370, 84, 28))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.deca_cena.setFont(font)
        self.deca_cena.setStyleSheet("background-color:#E7E3D5;\n"
                                     "color: #577F77;\n"
                                     "border-radius: 7px;\n"
                                     "padding: 4px;\n"
                                     "")
        self.deca_cena.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.deca_cena.setObjectName("deca_cena")
        self.penzioneri_cena = QtWidgets.QLineEdit(self.centralwidget)
        self.penzioneri_cena.setGeometry(QtCore.QRect(470, 380, 101, 29))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.penzioneri_cena.setFont(font)
        self.penzioneri_cena.setStyleSheet("background-color:#86ACA5;\n"
                                           "color: #E7E3D5;\n"
                                           "border-radius: 8px;\n"
                                           "padding: 4px;\n"
                                           "")
        self.penzioneri_cena.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.penzioneri_cena.setObjectName("penzioneri_cena")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(300, 140, 121, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_6.setFont(font)
        self.label_6.setStyleSheet("color: #ffffff;")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(180, 160, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_7.setFont(font)
        self.label_7.setStyleSheet("color:#577F77; ")
        self.label_7.setObjectName("label_7")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setGeometry(QtCore.QRect(50, 180, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_8.setFont(font)
        self.label_8.setStyleSheet("color: #ffffff;")
        self.label_8.setObjectName("label_8")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setGeometry(QtCore.QRect(610, 180, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_9.setFont(font)
        self.label_9.setStyleSheet("color: #ffffff;")
        self.label_9.setObjectName("label_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setGeometry(QtCore.QRect(470, 160, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_10.setFont(font)
        self.label_10.setStyleSheet("color:#577F77; ")
        self.label_10.setObjectName("label_10")
        self.widget_2.raise_()
        self.label_4.raise_()
        self.label_2.raise_()
        self.label_5.raise_()
        self.label_3.raise_()
        self.label.raise_()
        self.goBackButton.raise_()
        self.pocasni_cena.raise_()
        self.odrasli_cena.raise_()
        self.studenti_cena.raise_()
        self.comboBox_pocasni.raise_()
        self.deca_cena.raise_()
        self.penzioneri_cena.raise_()
        self.label_6.raise_()
        self.label_7.raise_()
        self.label_8.raise_()
        self.label_9.raise_()
        self.label_10.raise_()
        KreirajCenovnik.setCentralWidget(self.centralwidget)

        self.retranslateUi(KreirajCenovnik)
        QtCore.QMetaObject.connectSlotsByName(KreirajCenovnik)

    def retranslateUi(self, KreirajCenovnik):
        _translate = QtCore.QCoreApplication.translate
        KreirajCenovnik.setWindowTitle(_translate("KreirajCenovnik", "MainWindow"))
        self.prikazi_cene.setText(_translate("KreirajCenovnik", "prikazi cene"))
        self.izmeniOption.setText(_translate("KreirajCenovnik", "izmeni"))
        self.obrisiOption.setText(_translate("KreirajCenovnik", "obrisi"))
        self.pocasni_cena.setPlaceholderText(_translate("KreirajCenovnik", "cena clanarine:"))
        self.odrasli_cena.setPlaceholderText(_translate("KreirajCenovnik", "cena clanarine:"))
        self.studenti_cena.setPlaceholderText(_translate("KreirajCenovnik", "cena clanarine:"))
        self.comboBox_pocasni.setItemText(0, _translate("KreirajCenovnik", "mesecna"))
        self.comboBox_pocasni.setItemText(1, _translate("KreirajCenovnik", "polugodisnja"))
        self.comboBox_pocasni.setItemText(2, _translate("KreirajCenovnik", "godisnja"))
        self.deca_cena.setPlaceholderText(_translate("KreirajCenovnik", "cena clanarine:"))
        self.penzioneri_cena.setPlaceholderText(_translate("KreirajCenovnik", "cena clanarine:"))
        self.label_6.setText(_translate("KreirajCenovnik", "pocasni gosti"))
        self.label_7.setText(_translate("KreirajCenovnik", "odrasli"))
        self.label_8.setText(_translate("KreirajCenovnik", "odrasli"))
        self.label_9.setText(_translate("KreirajCenovnik", "deca"))
        self.label_10.setText(_translate("KreirajCenovnik", "penzioneri"))

    def show_price(self):
        from src.entities.MembershipClass import Membership

        mb = Membership()
        mb.make_membership_price_dict()

        price_dict = mb.price_membership_dict

        self.show_it(price_dict['studenti']['price'], price_dict['odrasli']['price'],
                     price_dict['pocasni_clanovi']['price'], price_dict['penzioneri']['price'],
                     price_dict['deca']['price'], price_dict['pocasni_clanovi']['type_membership'])

    def show_it(self, studenti, odrasli, pocasni, penzioneri, deca, duzina_trajanja):
        self.studenti_cena.setText(studenti)
        self.odrasli_cena.setText(odrasli)
        self.pocasni_cena.setText(pocasni)
        self.penzioneri_cena.setText(penzioneri)
        self.deca_cena.setText(deca)

        self.comboBox_pocasni.setItemText(0, duzina_trajanja)

    def izmeniCallFunc(self):
        from src.Controller.memberships_controller.PriceMembershipCRUD import PriceMembershipCRUD

        pr_mb = PriceMembershipCRUD()
        pr_mb.createUpdatePrice(self.pocasni_cena.text(), self.comboBox_pocasni.currentText())

        price_dict = pr_mb.price_dict

        self.show_it(price_dict['studenti']['price'], price_dict['odrasli']['price'],
                     price_dict['pocasni_clanovi']['price'], price_dict['penzioneri']['price'],
                     price_dict['deca']['price'], price_dict['pocasni_clanovi']['type_membership'])

    def obrisiCallFunc(self):
        from src.Controller.memberships_controller.PriceMembershipCRUD import PriceMembershipCRUD

        pr_mb = PriceMembershipCRUD()
        pr_mb.delete_price()

        price_dict = pr_mb.price_dict

        self.show_it(price_dict['studenti']['price'], price_dict['odrasli']['price'],
                     price_dict['pocasni_clanovi']['price'], price_dict['penzioneri']['price'],
                     price_dict['deca']['price'], price_dict['pocasni_clanovi']['type_membership'])
