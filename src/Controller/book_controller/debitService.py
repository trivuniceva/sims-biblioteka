from datetime import datetime, timedelta

from src.entities.Reservation import ReservationStatus


class DebitControl:
    def __init__(self):
        self.__debit_model = DebitModel()

    def make_debit(self, member_id, isbn, librarian_id):
        if not DebitControl.is_num_string(member_id) or not DebitControl.is_num_string(isbn):
            return False
        if self.__debit_model.is_book_free(isbn) and not self.__debit_model.is_already_taken(isbn,member_id):
            self.__debit_model.make_debit(member_id, isbn, librarian_id)
            return True
        return False

    def make_debit_by_reservation(self, reservation, librarian_id):
        if not DebitControl.is_num_string(reservation.get_member()) or not DebitControl.is_num_string(
                reservation.get_book()):
            return False
        if not self.__debit_model.is_reservation_expired(reservation) and \
               not self.__debit_model.is_already_taken(reservation.get_book(), reservation.get_member()):
            self.__debit_model.make_debit_from_reservation(reservation, librarian_id)
            return True
        return False

    def return_debit(self, isbn, member_id):
        self.__debit_model.return_debit(isbn, member_id)

    def extend_debit(self, isbn, member_id):
        debit = self.__debit_model.search(isbn, member_id)
        if debit:
            if len(debit) == 1:
                if self.__debit_model.can_debit_be_extended(debit[1]):
                    self.__debit_model.extend_debit(debit[1])
                    return True
        return False

    @staticmethod
    def is_num_string(string):
        if string.isnumeric():
            return True
        return False


class DebitModel:

    def __init__(self):
        from src.repositories.DebitRepository import DebitRepository
        from src.repositories.BookRepository import BookRepository
        from src.repositories.ReservationRepository import ReservationRepository

        self.__debit_repo = DebitRepository()
        self.__debit_repo.read_all()

        self.__res_repo = ReservationRepository()
        self.__res_repo.read_all()

        self.__books_repo = BookRepository()

    def write_all(self):
        self.__debit_repo.write_all()

    @staticmethod
    def find_by_isbn(books, isbn):
        if isbn != "":
            books = [res for res in books if res.get_book() == isbn]
        return books

    @staticmethod
    def find_by_member_id(books, member_id):
        if member_id != "":
            books = [res for res in books if res.get_member() == member_id]
        return books

    def search(self, isbn, member_id):

        books = list(self.__debit_repo.get_all().values())

        books = self.find_by_isbn(books, isbn)
        books = self.find_by_member_id(books, member_id)

        return books

    def make_debit(self, member_id, isbn, librarian_id):
        from src.entities.Debit import Debit

        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        book = self.__books_repo.get_book_by_isbn(isbn)
        status = int(book.get_book_status())
        return_deadline = today + timedelta(days=status)
        debit = Debit(self.__debit_repo.generate_debit_id(), today, member_id, isbn, librarian_id, return_deadline)
        self.__debit_repo.add(debit)
        self.__books_repo.remove_book_from_state(isbn)

    def make_debit_from_reservation(self, reservation, librarian_id):
        self.make_debit(reservation.get_member(), reservation.get_book(), librarian_id)
        reservation.set_reservation_status(ReservationStatus.finished)
        self.__res_repo.update(reservation)
        self.__books_repo.remove_book_from_reserved(reservation.get_book())

    def return_debit(self, isbn, member_id):
        debit = self.search(isbn, member_id)
        if debit: debit = debit[1]
        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        debit.return_book(today)
        self.__debit_repo.update(debit)
        self.__books_repo.add_book_to_state(debit.get_book())

    def update_debit(self, isbn, member_id, debit):
        debit.set_book(isbn)
        debit.set_member(member_id)
        self.__debit_repo.update(debit)

    def extend_debit(self, debit):
        debit.extend_debit()
        self.__debit_repo.update(debit)

    def get_debits(self):
        return self.__debit_repo.get_all()

    def is_book_free(self, isbn):
        book = self.__books_repo.get_book_by_isbn(isbn)
        if book:
            if int(book.get_num_of_available_books()) > book.get_num_of_reserved() and \
                    int(book.get_num_of_available_books()) >= 1:
                return True
        return False

    def is_already_taken(self, isbn, id):
        book = self.__books_repo.get_book_by_isbn(isbn)
        if book:
            debits = self.search(isbn,id)
            if not debits:
                return True
        return False

    @staticmethod
    def can_debit_be_extended(debit):
        if debit.extension_status:
            return False
        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        if debit.get_return_deadline() >= today:
            return True
        return False

    @staticmethod
    def is_reservation_expired(reservation):
        if reservation.get_reservation_status() == ReservationStatus.finished or \
                reservation.get_reservation_status() == ReservationStatus.expired:
            return True
        return False
