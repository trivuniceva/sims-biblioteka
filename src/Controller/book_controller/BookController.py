import random
from src.repositories.BookRepository import BookRepository


def find_book_by_title(result, title):
    if title != "":
        result = [res for res in result if res.get_title() == title]
    return result


def find_book_by_author(result, author):
    if author != "":
        result = [res for res in result if res.get_author() == author]
    return result


def find_book_by_category(result, category):
    if category != "":
        result = [res for res in result if res.get_category() == category]
    return result


def find_book_by_genre(result, genre):
    if genre != "":
        result = [res for res in result if res.get_genre() == genre]
    return result


def find_book_by_status(result, status):
    if status != "":
        result = [res for res in result if res.get_book_status() == status]
    return result


def find_book_by_available_status(result, available_status):
    if available_status == 'trenutno dostupne':
        result = [res for res in result if res.get_num_of_available_books() != "0"]
    return result


def find_book_by_year_of_publication(result, year):
    if year != "":
        result = [res for res in result if res.get_year_of_publication]

    return result


def find_book_by_isbn(isbn):
    return br.get_all_books()[isbn]


def filtered_search(title, author, category, genre, year_of_publication, num_of_items, status):

    books = list(BookRepository().get_all_books().values())

    books = find_book_by_status(books, status)
    books = find_book_by_available_status(books, num_of_items)
    books = find_book_by_year_of_publication(books, year_of_publication)
    books = find_book_by_author(books, author)
    books = find_book_by_category(books, category)
    books = find_book_by_title(books, title)
    books = find_book_by_genre(books, genre)

    return books


def direct_search_or_filtered(isbn, title, author, category, genre, year_of_publication, num_of_items,
                              status):
    res = []

    if isbn == "":

        res = filtered_search(title, author, category, genre, year_of_publication, num_of_items, status)

    if isbn != "":
        res.append(find_book_by_isbn(isbn))

    return res


def is_isbn_valid(isbn):

    if isbn not in br.get_all_books().keys():
        return True
    else:
        return False


def generate_isbn():
    isbn = ""

    for i in range(13):
        x = random.randint(0, 9)
        isbn += str(x)

    while not is_isbn_valid(isbn):
        generate_isbn()

    return isbn


def delete_book(isbn):
    br.all_books.pop(isbn)

    br.update_txt_file()


def update_book(title, author, category, genre, num_of_pages, year_of_publication, num_of_items, status, book):

    book.set_title(title)
    book.set_author(author)
    book.set_category(category)
    book.set_genre(genre)
    book.set_num_of_pages(num_of_pages)
    book.set_year_of_publication(year_of_publication)
    book.set_num_of_available_books(num_of_items)
    book.set_book_status(status+'\n') # zbog upisa u fajl nz ne pitajte, glupost....nisam sekla \n...

    br.all_books[book.get_isbn()] = book

    br.update_txt_file()


def add_book(title, author, category, genre, num_of_pages, year_of_publication, num_of_items, status):
    from src.entities.Book import Book
    isbn = generate_isbn()

    new_book = Book(isbn, title, author, category, genre, num_of_pages, year_of_publication, num_of_items, status)

    br.add_book_to_all_books(new_book)
    br.add_one_book_to_txt(new_book)


br = BookRepository()
