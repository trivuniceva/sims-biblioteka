from datetime import datetime, timedelta


class ReservationControl:
    def __init__(self):
        self.__reservation_model = ReservationModel()

    def make_reservation(self, isbn, member_id):
        from src.Controller.book_controller.debitService import DebitControl

        if not DebitControl.is_num_string(member_id) or not DebitControl.is_num_string(isbn):
            return False
        if self.__reservation_model.is_book_free_for_reservation(isbn, member_id):
            self.__reservation_model.make_reservation(isbn, member_id)
            return True
        return False

    def remove_reservation(self, reservation):
        self.__reservation_model.remove_reservation(reservation)


class ReservationModel:

    def __init__(self):
        from src.repositories.ReservationRepository import ReservationRepository
        from src.repositories.BookRepository import BookRepository

        self.__reservation_repo = ReservationRepository()
        self.__reservation_repo.read_all()

        self.__books_repo = BookRepository()

    def make_reservation(self, isbn, member_id):
        from src.entities.Reservation import Reservation

        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        new_reservation = Reservation(self.__reservation_repo.generate_reservation_id(), today, isbn, member_id)
        self.__reservation_repo.add(new_reservation)
        self.__books_repo.add_book_to_num_of_reserved(isbn)
        self.__books_repo.remove_book_from_state(isbn)

    def remove_reservation(self, reservation):
        self.__reservation_repo.remove(reservation)
        self.__books_repo.remove_book_from_reserved(reservation.get_book())

    def reservation_expired(self, reservation):
        if reservation.get_reservation_date() + timedelta(days=3) == datetime.now():
            self.__reservation_repo.remove(reservation)
            return True
        return False

    def is_book_free_for_reservation(self, isbn, member_id):
        book = self.__books_repo.get_book_by_isbn(isbn)
        if 1 <= int(book.get_num_of_available_books()) <= 3:
            reservations = self.search(isbn, member_id)
            if not reservations:
                return True
        return False

    @staticmethod
    def find_by_isbn(books, isbn):
        if isbn != "":
            books = [res for res in books if res.get_book() == isbn]
        return books

    @staticmethod
    def find_by_member_id(books, member_id):
        if member_id != "":
            books = [res for res in books if res.get_member() == member_id]
        return books

    def search(self, isbn, member_id):

        books = self.__reservation_repo.get_all().values()

        books = self.find_by_isbn(books, isbn)
        books = self.find_by_member_id(books, member_id)

        return books
