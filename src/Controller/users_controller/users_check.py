from src.repositories.user_repository.UserRepository import UserRepository

user = UserRepository()
user.make_registred_users_dict()


def check_if_username_used(entered_username):
    print(entered_username)

    for registred_username in user.registred_users.keys():
        if registred_username == entered_username:
            return True


def direct_search(entered_username):
    display_user_obj = user.registred_users[entered_username]
    print('display: -------------------->')
    print(display_user_obj)
    return display_user_obj


def search_by_id(entered_id):
    for display_user_obj in user.registred_users.values():
        if display_user_obj.get_role() == 'member':
            if display_user_obj.get_id_membership() == entered_id:
                return display_user_obj


def multicriteria_search():
    results = user.registred_users.values()
    return results


def password_search(result, password):
    result = [res for res in result if res.get_password() == password]
    return result


def name_search(result, password):
    result = [res for res in result if res.get_name() == password]
    return result


def lastname_search(result, lastname):
    result = [res for res in result if res.get_lastname() == lastname]
    return result


def email_search(result, email):
    result = [res for res in result if res.get_email() == email]
    return result


def jmbg_search(result, jmbg):
    result = [res for res in result if res.get_jmbg() == jmbg]
    return result


def telephone_search(result, telephone):
    result = [res for res in result if res.get_telephone_number() == telephone]
    return result


def address_search(result, address):
    result = [res for res in result if res.get_address() == address]
    return result


def membership_search(result, membership_type):
    result = [res for res in result if res.get_membership_type() == membership_type]
    return result


def birth_search(result, birth):
    result = [res for res in result if res.get_birth_date() == birth]
    return result


def other_case(result):
    result = [result]
    return result
