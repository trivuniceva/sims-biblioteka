from src.entities.MemberClass import MemberClass
from src.repositories.user_repository.UserRepository import UserRepository


class member_CRUD(MemberClass):
    user = UserRepository()
    user.make_registred_users_dict()

    def __init__(self, id_membership, telephone_number, address, membership_type, birth_date, username, password, name,
                 lastname, jmbg, email, role):
        super().__init__(id_membership, telephone_number, address, membership_type, birth_date, username, password,
                         name, lastname, jmbg, email, role)

    def create_member(self):
        print(self.user.registred_users)
        self.user.registred_users[self.username] = MemberClass(self.id_membership, self.telephone_number, self.address,
                                                               self.membership_type, self.birth_date, self.username,
                                                               self.password, self.name, self.lastname, self.jmbg,
                                                               self.email, self.role)
        print(self.user.registred_users)

    def update_member(self, username_new, password_new, name_new, lastname_new, jmbg_new, email_new, telephone_new,
                      address_new, date_new, membership_type_new):

        self.user.registred_users[username_new] = MemberClass(self.user.registred_users[username_new].get_id_membership(),
                                                              telephone_new, address_new, membership_type_new, date_new,
                                                              username_new, password_new, name_new, lastname_new,
                                                              jmbg_new, email_new, 'member')

    def delete_member(self, entered_username):
        print(self.user.registred_users)
        print('- - - - - - - - - - D E L E T E - - - - - - - - - - ')

        self.user.registred_users.pop(entered_username)

        print('- - - - - - - - - - - - - - - - - - - - - - - - - ')
        print(self.user.registred_users)
