from src.entities.MembershipClass import *


class PriceMembershipCRUD:

    mb = Membership()
    mb.make_membership_price_dict()
    price_dict = mb.price_membership_dict

    studenti_dict = price_dict['studenti']
    odrasli_dict = price_dict['odrasli']
    pocasni_clanovi_dict = price_dict['pocasni_clanovi']
    penzioneri = price_dict['penzioneri']
    deca = price_dict['deca']

    def createUpdatePrice(self, pocasni_new_price, duzina_trajanja):

        duzina = self.check_duzina_trajanja(duzina_trajanja)

        self.make_price(float(pocasni_new_price), duzina)

        self.studenti_dict['price'] = str(self.studenti_new_price)
        self.studenti_dict['type_membership'] = duzina_trajanja
        self.odrasli_dict['price'] = str(self.odrasli_new_price)
        self.odrasli_dict['type_membership'] = duzina_trajanja
        self.pocasni_clanovi_dict['price'] = str(pocasni_new_price)
        self.pocasni_clanovi_dict['type_membership'] = duzina_trajanja
        self.penzioneri['price'] = str(self.penzioneri_new_price)
        self.penzioneri['type_membership'] = duzina_trajanja
        self.deca['price'] = str(self.deca_new_price)
        self.deca['type_membership'] = duzina_trajanja

    def make_price(self, pocasni_new_price, duzina_trajanja):
        print('<3333')

        studenti_percentage = 10
        odrasli_percentage = 30
        penzioneri_percentage = 15
        deca_percentage = 5

        self.studenti_new_price = pocasni_new_price + (((studenti_percentage / 100) * pocasni_new_price) * duzina_trajanja)
        self.odrasli_new_price = pocasni_new_price + (((odrasli_percentage / 100) * pocasni_new_price) * duzina_trajanja)
        self.penzioneri_new_price = pocasni_new_price + (((penzioneri_percentage / 100) * pocasni_new_price) * duzina_trajanja)
        self.deca_new_price = pocasni_new_price + (((deca_percentage / 100) * pocasni_new_price) * duzina_trajanja)

    @staticmethod
    def check_duzina_trajanja(duzina_trajanja):
        if duzina_trajanja == 'mesecna':
            return 1
        elif duzina_trajanja == 'polugodisnja':
            return 6
        else:
            return 12

    def delete_price(self):
        self.studenti_dict['price'] = ''
        self.studenti_dict['type_membership'] = ''
        self.odrasli_dict['price'] = ''
        self.odrasli_dict['type_membership'] = ''
        self.pocasni_clanovi_dict['price'] = ''
        self.pocasni_clanovi_dict['type_membership'] = ''
        self.penzioneri['price'] = ''
        self.penzioneri['type_membership'] = ''
        self.deca['price'] = ''
        self.deca['type_membership'] = ''


