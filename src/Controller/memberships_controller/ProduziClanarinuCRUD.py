from src.entities.MembershipClass import Membership
from datetime import datetime
from dateutil.relativedelta import relativedelta


class ProduziClanarinuCRUD:
    mb = Membership()
    mb.make_membership_dict()

    def create_membership(self, id_membership, membership_type, vreme_trajanja_clanarina):
        today = self.current_date()
        new_date = self.make_date(today, vreme_trajanja_clanarina)

        self.mb.memberships_dict[id_membership]['membership_type'] = membership_type
        self.mb.memberships_dict[id_membership]['membership_expiration_date'] = new_date

        print('<33')

    def update_membership(self, id_membership, membership_type, vreme_trajanja_clanarina, old_date):
        old_date_obj = self.make_date_obj(old_date)
        new_date = self.make_date(old_date_obj, vreme_trajanja_clanarina)

        self.mb.memberships_dict[id_membership]['membership_type'] = membership_type
        self.mb.memberships_dict[id_membership]['membership_expiration_date'] = new_date

    def delete_membership(self, id_membership):
        print(self.mb.memberships_dict[id_membership])

        self.mb.memberships_dict[id_membership]['membership_type'] = ''
        self.mb.memberships_dict[id_membership]['membership_expiration_date'] = ''

        print(self.mb.memberships_dict[id_membership])

    @staticmethod
    def current_date():
        today = datetime.today()
        return today

    @staticmethod
    def make_date_obj(old_date):
        date1 = datetime.strptime(old_date, "%d.%m.%Y.")
        return date1

    def make_date(self, old_date, vreme_trajanja_clanarina):
        if vreme_trajanja_clanarina == 'mesecna':
            new_date = self.add_month(old_date, 1)
            return new_date

        elif vreme_trajanja_clanarina == 'polugodisnja':
            new_date = self.add_month(old_date, 6)
            return new_date

        else:
            new_date = self.add_month(old_date, 12)
            return new_date

    @staticmethod
    def add_month(old_date, month_number):
        next_month = old_date + relativedelta(months = month_number)
        return next_month.strftime('%d.%m.%Y.')
