from src.entities.LibraryClass import Library
from src.repositories.user_repository.UserRepository import UserRepository


def login(username, password):
    lib = Library()
    ur = UserRepository()
    ur.make_registred_users_dict()
    # ur.make_members__dict()
    lib.set_all_users(ur.registred_users)

    user = lib.get_user_by_username(username)
    if user.get_password() == password:
        print("Welcome ", user.get_name(), " ", user.get_lastname())
        return user
    else:
        return None
