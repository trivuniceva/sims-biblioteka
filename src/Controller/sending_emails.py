import smtplib
import random
from src.Controller.users_controller.librarian_CRUD import *


def autogenerate_username():
    username = 'simsBibl' + str(random.randint(1000, 9999))

    user = UserRepository()
    user.make_registred_users_dict()

    if user.registred_users.keys().__contains__(username):
        autogenerate_username()
    else:
        return username


def autogenerate_password():
    password = str(random.randint(10, 99)) + '_!' + str(random.randint(1000, 9999))
    return password


def autogenerate_id_membership():
    id_memb = str(random.randint(1000000, 9999999))

    from src.entities.MembershipClass import Membership

    mb = Membership()
    mb.make_membership_dict()

    if mb.memberships_dict.keys().__contains__(id_memb):
        autogenerate_id_membership()
    else:
        return id_memb


def send_username_password(generated_username, generated_password, email):
    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    server.login('simsBiblioteka@gmail.com', 'elita sa sita')

    message = '''
    Postovani,
    vase korisnicko ime je: ''' + generated_username + '''
    lozinka je: ''' + generated_password

    server.sendmail('simsBiblioteka@gmail.com', email, message)
    server.quit()

    print('email sent')


def send_username_password_member(generated_username, generated_password, generated_id_memb, email):
    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    server.login('simsBiblioteka@gmail.com', 'elita sa sita')

    message = '''
        Postovani,
        vase korisnicko ime je: ''' + generated_username + '''
        lozinka je: ''' + generated_password + '''
        i broj clanske karte: ''' + generated_id_memb

    server.sendmail('simsBiblioteka@gmail.com', email, message)
    server.quit()

    print('email sent')
