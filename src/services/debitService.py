from src.entities.Debit import Debit
from datetime import datetime, timedelta

from src.repositories.BookRepository import BookRepository
from src.repositories.DebitRepository import *


class DebitControl:

    def __init__(self, debit_model):
        self.__debit_model = debit_model

    def make_debit(self, member_id, isbn, librarian_id):
        if not DebitControl.is_num_string(member_id, 2) or not DebitControl.is_num_string(isbn, 2):
            return False
        if self.__debit_model.is_book_free(isbn):
            self.__debit_model.make_debit(member_id, isbn, librarian_id)
            return True
        return False

    def make_debit_by_reservation(self, reservation, librarian_id):
        if not DebitControl.is_num_string(reservation.get_member(), 2) or not DebitControl.is_num_string(
                reservation.get_book(), 2):
            return False
        self.__debit_model.make_debit_from_reserved(reservation, librarian_id)
        return True

    def return_debit(self, debit):
        self.__debit_model.return_debit(debit)

    def update_debit(self):
        pass

    def extend_debit(self, debit):
        if self.__debit_model.can_debit_be_extended(debit):
            self.__debit_model.extend_debit(debit)
            return True
        return False

    @staticmethod
    def is_num_string(string, num_of_char):
        if string.isnumeric():
            if len(string) == num_of_char:
                return True
        return False


class DebitModel:
    def __init__(self):
        self.__debit_repo = DebitRepository()
        self.__debit_repo.read_all()

        self.__books_repo = BookRepository()

    def write_all(self):
        self.__debit_repo.write_all()

    @staticmethod
    def find_by_isbn(books, isbn):
        if isbn != "":
            books = [res for res in books if res.get_isbn()]
        return books

    @staticmethod
    def find_by_member_id(books, id):
        if id != "":
            books = [res for res in books if res.get_member()]
        return books

    def search(self, isbn, member_id):

        books = list(self.__debit_repo.get_all().values())

        books = self.find_by_isbn(books, isbn)
        books = self.find_by_member_id(books, member_id)

        return books

    def make_debit(self, member_id, isbn, librarian_id):
        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        book = self.__books_repo.get_book_by_isbn(isbn)
        # dodati status knjige preovera na koliko se dana moze izdati
        return_deadline = today + timedelta(days=10)
        debit = Debit(self.__debit_repo.generate_debit_id(), today, member_id, isbn, librarian_id, return_deadline)
        self.__debit_repo.add(debit)
        self.__books_repo.remove_book_from_state(isbn)

    def make_debit_from_reservation(self, reservation, librarian_id):
        self.make_debit(reservation.get_member(), reservation.get_book(), librarian_id)
        self.__books_repo.remove_book_from_reserved(reservation.get_book())

    def return_debit(self, debit):
        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        debit.return_book(today)
        self.__debit_repo.update(debit)
        self.__books_repo.add_book_to_state(debit.get_book())

    def update_debit(self, isbn, member_id, debit):
        debit.set_book(isbn)
        debit.set_member(member_id)
        self.__debit_repo.update(debit)

    def extend_debit(self, debit):
        debit.extend_debit()
        self.__debit_repo.update(debit)

    def get_debits(self):
        return self.__debit_repo.get_all()

    def is_book_free(self, isbn):
        book = self.__books_repo.get_book_by_isbn(isbn)
        if book:
            if int(book.get_num_of_available_books()) > book.get_num_of_reserved() and int(book.get_num_of_available_books()) >= 1:
                return True
        return False

    @staticmethod
    def can_debit_be_extended(debit):
        if debit.extension_status:
            return False
        today = datetime.strftime(datetime.now(), '%d/%m/%Y')
        today = datetime.strptime(today, '%d/%m/%Y')
        if debit.get_return_deadline() >= today:
            return True
        return False
