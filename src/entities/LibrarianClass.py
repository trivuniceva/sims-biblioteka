from src.entities.UsersClass import *


class LibrarianClass(UsersClass, ABC):

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_name(self):
        return self.name

    def get_lastname(self):
        return self.lastname

    def get_jmbg(self):
        return self.jmbg

    def get_email(self):
        return self.email

    def get_role(self):
        return self.role
