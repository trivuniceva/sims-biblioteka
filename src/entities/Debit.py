import enum


class DebitStatus(enum.Enum):
    active = 1
    notActive = 2


class Debit:
    def __init__(self, debit_id, debit_date, member_id, isbn, librarian_id, return_deadline, return_date=None,
                 extension_status=False, debit_status=DebitStatus.active):
        self.__debit_id = debit_id
        self.__debit_date = debit_date
        self.__member_id = member_id
        self.__isbn = isbn
        self.__librarian_id = librarian_id
        self.__return_date = return_date
        self.__extension_status = extension_status
        self.__return_deadline = return_deadline
        self.__debit_status = debit_status

    def return_book(self, return_date):
        self.__return_date = return_date
        self.__debit_status = DebitStatus.notActive

    def extend_debit(self):
        self.__extension_status = True
        self.__return_deadline += self.__return_deadline

    def get_id(self):
        return self.__debit_id

    def get_date(self):
        return self.__debit_date

    def get_member(self):
        return self.__member_id

    def set_member(self, member):
        self.__member_id = member

    def get_book(self):
        return self.__isbn

    def set_book(self, isbn):
        self.__isbn = isbn

    def get_librarian(self):
        return self.__librarian_id

    def get_return_date(self):
        return self.__return_date

    def get_extension_status(self):
        return self.__extension_status

    def get_return_deadline(self):
        return self.__return_deadline

    def get_status(self):
        return self.__debit_status
