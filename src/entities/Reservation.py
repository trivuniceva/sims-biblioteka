import enum

class ReservationStatus(enum.Enum):
    active = 1
    expired = 2
    finished = 3


class Reservation:
    def __init__(self, reservation_id, reservation_date, book, member, reservation_status=ReservationStatus.active):
        self._reservation_id = reservation_id
        self._reservation_date = reservation_date
        self._book = book
        self._member = member
        self._reservation_status = reservation_status

    def set_reservation_status(self, status):
        self._reservation_status = status

    def get_reservation_id(self):
        return self._reservation_id

    def get_reservation_date(self):
        return self._reservation_date

    def get_book(self):
        return self._book

    def get_member(self):
        return self._member

    def get_reservation_status(self):
        return self._reservation_status
