class Book(object):
    def __init__(self, isbn=None, title=None, author=None, category=None, genre=None, num_of_pages=None,
                 year_of_publication=None, num_of_available_books=None, book_status=None, num_of_reserved=0):
        self.isbn = isbn
        self.title = title
        self.author = author
        self.category = category
        self.genre = genre
        self.num_of_pages = num_of_pages
        self.year_of_publication = year_of_publication
        self.num_of_available_books = num_of_available_books
        self.book_status = book_status
        self.num_of_reserved = num_of_reserved

    def get_isbn(self):
        return self.isbn

    def set_isbn(self, isbn):
        self.isbn = isbn

    def get_title(self):
        return self.title

    def set_title(self, title):
        self.title = title

    def get_author(self):
        return self.author

    def set_author(self, author):
        self.author = author

    def get_category(self):
        return self.category

    def set_category(self, category):
        self.category = category

    def get_genre(self):
        return self.genre

    def set_genre(self, genre):
        self.genre = genre

    def get_num_of_pages(self):
        return self.num_of_pages

    def set_num_of_pages(self, num_of_pages):
        self.num_of_pages = num_of_pages

    def get_num_of_reserved(self):
        return int(self.num_of_reserved)

    def set_num_of_reserved(self, num_of_reserved):
        self.num_of_reserved = num_of_reserved

    def get_year_of_publication(self):
        return self.year_of_publication

    def set_year_of_publication(self, year_of_publication):
        self.year_of_publication = year_of_publication

    def get_num_of_available_books(self):
        return self.num_of_available_books

    def set_num_of_available_books(self, num_of_available_books):
        self.num_of_available_books = num_of_available_books

    def get_book_status(self):
        return self.book_status

    def set_book_status(self, book_status):
        self.book_status = book_status
