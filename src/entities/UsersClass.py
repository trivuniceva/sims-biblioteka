from abc import ABC, abstractmethod


class UsersClass:

    @abstractmethod
    def __init__(self, username, password, name, lastname, jmbg, email, role):
        self.username = username
        self.password = password
        self.name = name
        self.lastname = lastname
        self.jmbg = jmbg
        self.email = email
        self.role = role

    def get_username(self):
        return self.username

    def get_password(self):
        return self.password

    def get_name(self):
        return self.name

    def get_lastname(self):
        return self.lastname

    def get_jmbg(self):
        return self.jmbg

    def get_email(self):
        return self.email

    def get_role(self):
        return self.role
