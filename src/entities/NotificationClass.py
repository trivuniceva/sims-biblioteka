

class Notification:
    def __init__(self, notificationID, userID, message):
        self.notificationID = notificationID
        self.userID = userID
        self.message = message

    def getNotificationID(self):
        return self.notificationID

    def getUserID(self):
        return self.userID

    def getMessage(self):
        return self.message

