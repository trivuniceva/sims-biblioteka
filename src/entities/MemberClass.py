from src.entities.UsersClass import *


class MemberClass(UsersClass, ABC):
    points = 0

    def __init__(self, id_membership, telephone_number, address, membership_type, birth_date, username, password,
                 name, lastname, jmbg, email, role):
        super().__init__(username, password, name, lastname, jmbg, email, role)
        self.id_membership = id_membership
        self.telephone_number = telephone_number
        self.address = address
        self.membership_type = membership_type
        self.birth_date = birth_date

    def get_id_membership(self):
        return self.id_membership

    def get_telephone_number(self):
        return self.telephone_number

    def get_address(self):
        return self.address

    def get_membership_type(self):
        return self.membership_type

    def get_birth_date(self):
        return self.birth_date
