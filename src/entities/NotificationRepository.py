from src.entities.NotificationClass import *
from time import sleep
notificationsFileName = "../testData/notifications.txt"
readNotificationsFileName = "../testData/readNotifications.txt"


class NotificationRepository:
    def __init__(self):
        self.notificationsOfUser = []
        self.allNotifications = {}  # allNotifications is a dictionary {key = notificationID, value = Notification
        #                             object}
        self.allReadNotifications = {}  # readNotifications is a dictionary {key = userID, value = list of
        #                                 Notification objects}
        self.LoadAllReadNotifications()

    def LoadNotificationsByUser(self, userID, ignoreRead=True):
        self.notificationsOfUser = []
        self.LoadAllReadNotifications()
        notificationsFile = open(notificationsFileName, "r")
        for line in notificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                if data[1] == userID:
                    if (ignoreRead == True) and not (data[0] in self.allReadNotifications[userID]):
                        self.notificationsOfUser.append(Notification(data[0], data[1], data[2]))
                    if (ignoreRead == False) and (data[0] in self.allReadNotifications[userID]):
                        self.notificationsOfUser.append(Notification(data[0], data[1], data[2]))

        notificationsFile.close()

    def LoadAllNotifications(self):
        self.allNotifications = {}
        self.LoadAllReadNotifications()
        notificationsFile = open(notificationsFileName, "r")
        for line in notificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                self.allNotifications[data[0]] = Notification(data[0], data[1], data[2])

        notificationsFile.close()

    def LoadAllReadNotifications(self):
        self.allReadNotifications = {}
        readNotificationsFile = open(readNotificationsFileName, "r")
        for line in readNotificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                if not data[0] in self.allReadNotifications.keys():
                    self.allReadNotifications[data[0]] = []
                self.allReadNotifications[data[0]].append(data[1])

        readNotificationsFile.close()

    def getNotificationsByUserID(self, userID, ignoreRead=True):
        self.LoadNotificationsByUser(userID, ignoreRead)
        return self.notificationsOfUser

    def addNewNotification(self, notification_to_add):
        notificationsFile = open(notificationsFileName, "a")
        notificationsFile.write(notification_to_add.getNotificationID() + '|' + notification_to_add.getUserID() +
                                '|' + notification_to_add.getMessage() + '\n')
        self.LoadAllNotifications()
        notificationsFile.close()

    def deleteNotification(self, notification_to_delete):
        self.allNotifications.pop(notification_to_delete.notificationID)
        notificationsFile = open(notificationsFileName, "w")
        for notification in self.allNotifications.values():
            notificationsFile.write(notification.getNotificationID() + '|' + notification.getUserID() +
                                    '|' + notification.getMessage() + '\n')
        notificationsFile.close()

        readNotificationsFile = open(readNotificationsFileName, "w")
        readNotificationsFile.write("")
        readNotificationsFile.close()

        readNotificationsFile = open(readNotificationsFileName, "a")
        read_notifications_of_user = []
        for notification in self.allReadNotifications[notification_to_delete.userID]:
            if notification_to_delete.notificationID != notification:
                read_notifications_of_user.append(notification)
        self.allReadNotifications[notification_to_delete.userID] = read_notifications_of_user

        for key in self.allReadNotifications.keys():
            for notification in self.allReadNotifications[key]:
                readNotificationsFile.write(key + '|' + notification + '\n')
        readNotificationsFile.close()

        self.LoadAllNotifications()
        self.LoadAllReadNotifications()

if __name__ == "__main__":
    nr = NotificationRepository()
    nr.LoadAllNotifications()
    nr.LoadNotificationsByUser('1', False)
    notificationsOFUser = nr.getNotificationsByUserID('1', False)
    for notific in notificationsOFUser:
        print(notific.getNotificationID() + " " + notific.getUserID() + " " + notific.getMessage())
    print("deleting notification " + notificationsOFUser[1].getNotificationID(), end="")
    sleep(0.5)
    print(".", end="")
    sleep(0.5)
    print(".", end="")
    sleep(0.5)
    print(".")

    nr.deleteNotification(notificationsOFUser[1])
    print("Notification deleted")
    nr.LoadNotificationsByUser('1', False)
    notificationsOFUser = nr.getNotificationsByUserID('1', False)
    for notific in notificationsOFUser:
        print(notific.getNotificationID() + " " + notific.getUserID() + " " + notific.getMessage())
