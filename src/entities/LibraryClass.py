class Library:
    def __init__(self):
        self._all_users = {}
        self._members = {}
        self._books = {}
        self._categories = {}
        self._take_overs = {}
        self._reservations = {}

    def get_all_users(self):
        return self._all_users

    def get_members(self):
        return self._members

    def get_books(self):
        return self._books

    def get_categories(self):
        return self._categories

    def get_take_overs(self):
        return self._take_overs

    def get_reservations(self):
        return self._reservations

    def set_all_users(self, all_users):
        self._all_users = all_users

    def set_members(self, members):
        self._members = members

    def set_books(self, books):
        self._books = books

    def set_categories(self, categories):
        self._categories = categories

    def set_take_overs(self, take_overs):
        self._take_overs = take_overs

    def set_reservations(self, reservations):
        self._reservations = reservations

    def get_user_by_username(self, username):
        for key in self._all_users.keys():
            if key == username:
                return self._all_users[username]
        return None

    def get_member_by_id(self, ID):
        for key in self._members.keys():
            if key == ID:
                return self._members[ID]
        return None

    def get_book_by_isbn(self, isbn):
        for key in self._books.keys():
            if key == isbn:
                return self._books[isbn]
        return None

    def get_reservation_by_id(self, ID):
        for key in self._reservations.keys():
            if key == ID:
                return self._reservations[ID]
        return None

    def get_take_over_by_id(self, ID):
        for key in self._take_overs.keys():
            if key == ID:
                return self._take_overs[ID]
        return None

    def add_user(self, user):
        if user.username not in self._all_users.keys():
            self._all_users[user.username] = user
            if user.role == 'member':
                self._members[user.id_membership] = user
            # pozvati metodu iz repozitorijuma za unos novog korisnika u txt fajlove
            return user  # if a user is returned everything is good
        else:
            return None  # if None is returned username already exists
