from src.repositories.user_repository.readMembershipFile import *
membership_file = 'C:\\Users\\Nikolina\\Desktop\\sims_projekat\\src\\testData\\memberships.txt'
membership_price_file = 'C:\\Users\\Nikolina\\Desktop\\sims_projekat\\src\\testData\\membership_price.txt'


class Membership:

    memberships_dict = {}
    price_membership_dict = {}

    memberships_lst = read_membership_file(membership_file)
    price_membership_lst = read_membership_file(membership_price_file)

    def make_membership_dict(self):
        for memberships in self.memberships_lst:
            id_membership = memberships[0]
            membership_type = memberships[1]
            membership_expiration_date = memberships[2]
            self.memberships_dict[id_membership] = {'membership_type': membership_type, 'membership_expiration_date': membership_expiration_date}

    def make_membership_price_dict(self):
        for memberships in self.price_membership_lst:
            id_membership = memberships[0]
            price = memberships[1]
            type_membership = memberships[2]
            self.price_membership_dict[id_membership] = {'price': price, 'type_membership': type_membership}
