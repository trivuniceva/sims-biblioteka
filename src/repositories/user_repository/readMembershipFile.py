
def read_membership_file(file_name):

    memberships_lst = []

    file = open(file_name, 'r')

    for line in file.readlines():
        # if someone accidentally enter '\n' in file
        if line != '\n':
            lst_split_info = line.strip().split('|')
            memberships_lst.append(lst_split_info)

    file.close()

    return memberships_lst
