

class UserRepository:
    from src.repositories.user_repository.readUsersFile import ReadUsersFile

    registred_users = {}

    readF = ReadUsersFile()
    readF.read_information()
    users_lst = readF.user_lst

    def make_registred_users_dict(self):
        from src.entities.MemberClass import MemberClass
        from src.entities.UsersClass import UsersClass

        for user in self.users_lst:
            username = user[0]
            role = user[6]
            if role == 'member':
                self.registred_users[username] = MemberClass(user[7], user[8], user[9], user[10], user[11],
                                                             user[0], user[1], user[2], user[3], user[4], user[5],
                                                             user[6])
            else:
                self.registred_users[username] = UsersClass(user[0], user[1], user[2], user[3], user[4], user[5],
                                                            user[6])
