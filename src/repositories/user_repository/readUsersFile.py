
class ReadUsersFile:
    file_name = '../../testData/usersFile.txt'
    user_lst = []

    def read_information(self):
        file = open(self.file_name, 'r')

        for line in file.readlines():
            # if someone accidentally enter '\n' in file
            if line != '\n':
                lst_split_info = line.strip().split('|')
                self.user_lst.append(lst_split_info)

        file.close()
