file_reservations = "../../testData/reservations.txt"


class ReservationFiles:

    @staticmethod
    def read_all():
        from src.entities.Reservation import Reservation
        from src.entities.Reservation import ReservationStatus
        from src.repositories.DebitRepository import DebitRepository

        reservations = {}
        try:
            f = open(file_reservations, "r")
            for line in f.readlines():
                words = line.split('|')
                reservations[words[0]] = Reservation(words[0], DebitRepository.convert_string_to_date(words[1]),
                                                     words[2], words[3], ReservationStatus(int(words[4])))
            return reservations
        except FileNotFoundError:
            open(file_reservations, "x")
            return reservations

    @staticmethod
    def write_all(reservations):
        from src.repositories.DebitRepository import DebitRepository

        f = open(file_reservations, "w")
        line = ""
        for r in list(reservations.values()):
            line += r.get_reservation_id() + "|" + DebitRepository.convert_date_to_string(
                r.get_reservation_date()) + "|" + r.get_book() + "|" \
                    + r.get_member() + "|" + str(r.get_reservation_status().value) + "\n"
        f.write(line)


class ReservationRepository:
    def __init__(self):
        self.__reservations = {}

    def read_all(self):
        self.__reservations = ReservationFiles.read_all()

    def write_all(self):
        ReservationFiles.write_all(self.__reservations)

    def get_all(self):
        return self.__reservations

    def remove(self, reservation):
        self.__reservations.pop(reservation.get_reservation_id(), None)
        self.write_all()

    def update(self, reservation):
        if reservation.get_reservation_id() in self.__reservations.keys():
            self.__reservations[reservation.get_reservation_id()] = reservation
            self.write_all()
        return None

    def add(self, reservation):
        if reservation.get_reservation_id() in self.__reservations.keys():
            self.update(reservation)
        else:
            self.__reservations[reservation.get_reservation_id()] = reservation
        self.write_all()

    def generate_reservation_id(self):
        from src.repositories.DebitRepository import DebitRepository

        if self.__reservations:
            while True:
                num = DebitRepository.generate_key()
                if num not in self.__reservations.keys():
                    return num
        return DebitRepository.generate_key()
