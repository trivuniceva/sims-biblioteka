from src.entities.Book import Book as BookClass
file_name = "../../testData/Books.txt"

class BookRepository(object):

    def __init__(self):
        self.all_books = {}
        self.load_books()

    def get_all_books(self):
        return self.all_books

    def set_all_books(self, all_books):
        self.all_books = all_books

    # kontroler
    def add_book_to_all_books(self, new_book):
        self.all_books[new_book.get_isbn()] = new_book

    def load_books(self):

        file = open("../../testData/Books.txt", "r")
        for lines in file:
            line = lines.split("|")
            book = BookClass(line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7], line[8],int(line[9].replace("\n","")))
            self.add_book_to_all_books(book)
        file.close()

    def update_txt_file(self):
        open(file_name, 'w').close()
        file = open(file_name, mode='w', newline='\n')

        for book in list(self.all_books.values()):
            file.writelines(
                book.get_isbn() + "|" + book.get_title() + "|" + book.get_author() + "|" + book.get_category() +
                "|" + book.get_genre() + "|" + book.get_num_of_pages() + "|" + book.get_year_of_publication() +
                "|" + str(book.get_num_of_available_books()) + "|" + book.get_book_status() +"|"+ str(book.get_num_of_reserved()) + "\n")

        file.close()

    @staticmethod
    def add_one_book_to_txt(book):
        file = open(file_name, "a")

        file.write(book.get_isbn() + "|" + book.get_title() + "|" + book.get_author() + "|" + book.get_category() + "|"
                   + book.get_genre() + "|" + book.get_num_of_pages() + "|" + book.get_year_of_publication() + "|" +
                   str(book.get_num_of_available_books()) + "|" + book.get_book_status()  +"|"+  str(book.get_num_of_reserved())+ "\n")

        file.close()

    def get_book_by_isbn(self, isbn):
        for book in self.all_books.values():
            if book.get_isbn() == isbn:
                return book

    def remove_book_from_state(self, isbn):
        for book in self.all_books.values():
            if book.get_isbn() == isbn:
                book.set_num_of_available_books(str(int(book.get_num_of_available_books()) - 1))
                break
        self.update_txt_file()

    def remove_book_from_reserved(self, isbn):
        for book in self.all_books.values():
            if book.get_isbn() == isbn:
                book.set_num_of_reserved(book.get_num_of_reserved() - 1)
                break
        self.update_txt_file()

    def add_book_to_state(self, isbn):
        for book in self.all_books.values():
            if book.get_isbn() == isbn:
                book.set_num_of_available_books(str(int(book.get_num_of_available_books()) + 1))
                break
        self.update_txt_file()

    def add_book_to_num_of_reserved(self, isbn):
        for book in self.all_books.values():
            if book.get_isbn() == isbn:
                book.set_num_of_reserved(book.get_num_of_reserved() + 1)
                break
        self.update_txt_file()
