from src.entities.NotificationClass import *

notificationsFileName = "notifications.txt"
readNotificationsFileName = "readNotifications.txt"

class NotificationRepository:
    def __init__(self):
        self.notificationsOfUser = []
        self.allNotifications = {}              #allNotifications is a dictionary {key = userID, value = list of Notification objects}
        self.allReadNotifications = {}             #readNotifications is a dictionary {key = userID, value = list of Notification objects}
        self.LoadAllReadNotifications()

    def LoadNotificationsByUser(self, userID, ignoreRead = True):
        self.notificationsOfUser = []
        self.LoadAllReadNotifications()
        notificationsFile = open(notificationsFileName, "r")
        for line in notificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                if (data[1] == userID):
                    if (ignoreRead == True) and not (data[0] in self.allReadNotifications[userID]):
                        self.notificationsOfUser.append(Notification(data[0], data[1], data[2]))
                    if (ignoreRead == False) and (data[0] in self.allReadNotifications[userID]):
                        self.notificationsOfUser.append(Notification(data[0], data[1], data[2]))

        notificationsFile.close()

    def LoadAllNotifications(self):
        self.allNotifications = {}
        self.LoadAllReadNotifications()
        notificationsFile = open(notificationsFileName, "r")
        for line in notificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                if not data[1] in self.allNotifications.keys():
                    self.allNotifications[data[1]] = []
                self.allNotifications[data[1]].append(Notification(data[0], data[1], data[2]))

        notificationsFile.close()

    def LoadAllReadNotifications(self):
        self.allReadNotifications = {}
        readNotificationsFile = open(readNotificationsFileName, "r")
        for line in readNotificationsFile.readlines():
            if line != "\n":
                data = line.strip().split('|')
                if not data[1] in self.allReadNotifications.keys():
                    self.allReadNotifications[data[0]] = []
                self.allReadNotifications[data[0]].append(data[1])

        readNotificationsFile.close()

    def getNotificationsByUserID(self, userID, ignoreRead = True):
        self.LoadNotificationsByUser(userID, ignoreRead)
        return self.notificationsOfUser

if __name__ == "__main__":
    nr = NotificationRepository()
    nr.LoadNotificationsByUser('1',True)
    print(nr.getNotificationsByUserID('1',True))

