from src.repositories.user_repository.readUsersFile import *
from src.entities.MemberClass import *


class UserRepository:
    users_login_dict = {}
    members_dict = {}

    readF = ReadUsersFile()
    readF.read_information()
    users_lst = readF.user_lst

    def make_users_login_dict(self):
        for user in self.users_lst:
            username = user[0]
            role = user[6]
            if username != '':
                if role == 'member':
                    self.users_login_dict[username] = MemberClass(user[7], user[8], user[9], user[10], user[11],
                                                                  user[0], user[1], user[2], user[3], user[4], user[5],
                                                                  user[6])
                else:
                    self.users_login_dict[username] = UsersClass(user[0], user[1], user[2], user[3], user[4], user[5],
                                                                 user[6])

    def make_members__dict(self):
        for user in self.users_lst:
            role = user[6]
            if role == 'member':
                id_membership = user[7]
                self.members_dict[id_membership] = MemberClass(user[7], user[8], user[9], user[10], user[11], user[0],
                                                               user[1], user[2], user[3], user[4], user[5], user[6])
