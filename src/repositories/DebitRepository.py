import random
from datetime import datetime

file_debits = "../../testData/debits.txt"


class DebitFiles:

    @staticmethod
    def read_all():
        from src.entities.Debit import Debit
        from src.entities.Debit import DebitStatus

        debits = {}
        try:
            f = open(file_debits, 'r')
            if f:
                for line in f.readlines():
                    words = line.split('|')
                    debits[words[0]] = Debit(words[0], DebitRepository.convert_string_to_date(words[1]),
                                             words[2], words[3], words[4], DebitRepository.convert_string_to_date(
                            words[7]), DebitRepository.convert_string_to_date
                                             (words[5]), bool(words[6]), DebitStatus(int(words[8])))
                return debits
        except FileNotFoundError:
            open(file_debits, 'x')
            return debits

    @staticmethod
    def write_all(debits):
        line = ""

        for d in debits.values():
            ret_date = ""
            ret_deadline = ""
            ex_status = ""
            if d.get_return_date():
                ret_date = DebitRepository.convert_date_to_string(d.get_return_date())
            if d.get_return_deadline():
                ret_deadline = DebitRepository.convert_date_to_string(d.get_return_deadline())
            if d.get_extension_status():
                ex_status = "1"
            line += d.get_id() + "|" + DebitRepository.convert_date_to_string(
                d.get_date()) + "|" + d.get_member() + "|" + d.get_book() + "|" + d.get_librarian() + "|" + \
                ret_date + "|" + ex_status + "|" + ret_deadline + "|" + str(d.get_status().value) + "\n"

        f = open(file_debits, 'w')
        f.write(line)


class DebitRepository:
    def __init__(self):
        self.__debits = {}

    def read_all(self):
        self.__debits = DebitFiles.read_all()

    def write_all(self):
        DebitFiles.write_all(self.__debits)

    def get_all(self):
        return self.__debits

    def remove(self, debit):
        self.__debits.pop(debit.get_id(), None)

    def update(self, debit):
        if debit.get_id() in self.__debits.keys():
            self.__debits[debit.get_id()] = debit
        self.write_all()
        return None

    def add(self, debit):
        if debit.get_id() in self.__debits.keys():
            self.update(debit)
        else:
            self.__debits[debit.get_id()] = debit
        self.write_all()

    def generate_debit_id(self):
        if self.__debits:
            while True:
                num = DebitRepository.generate_key()
                if num not in self.__debits.keys():
                    return num
        return DebitRepository.generate_key()

    @staticmethod
    def generate_key():
        return str(random.randrange(1000, 9999))

    @staticmethod
    def convert_date_to_string(date):
        return datetime.strftime(date, '%d/%m/%Y')

    @staticmethod
    def convert_string_to_date(date):
        try:
            return datetime.strptime(date, '%d/%m/%Y')
        except ValueError:
            return None
